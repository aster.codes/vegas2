# Vegas2, a Game Theory Discord Community Bot

<div align="center">

[![pipeline status](https://gitlab.com/aster.codes/vegas2/badges/main/pipeline.svg)](https://gitlab.com/aster.codes/vegas2/-/pipelines)
[![coverage report](https://img.shields.io/gitlab/coverage/aster.codes/vegas2/main?label=Test%20Coverage)](https://gitlab.com/aster.codes/vegas2/-/commits/main)
[![PyLint](https://gitlab.com/aster.codes/vegas2/-/jobs/artifacts/main/raw/pylint/pylint.svg?job=PyLint%20Lint)](https://gitlab.com/aster.codes/vegas2/-/jobs/artifacts/main/raw/pylint/pylint.svg?job=PyLint%20Lint)
[![Black Formatted](https://img.shields.io/badge/Formatting-Black-000?logo=python&logoColor=ffd343)](https://gitlab.com/aster.codes/vegas2/-/commits/main)
[![Python Support](https://img.shields.io/badge/Python%20Support-%5E3.9.5-orange?logo=python&logoColor=ffd343)](https://gitlab.com/aster.codes/vegas2/-/commits/main)
[![Discord Support Server](https://img.shields.io/discord/794422514935529493?color=5865F2&label=Discord&logo=discord&logoColor=white)](https://discord.gg/Cx65DkJZtM)
[![Docker Compose Support](https://img.shields.io/badge/Docker%20Compose-Build%20Ready!-blue?logo=docker&logoColor=white)](https://gitlab.com/aster.codes/vegas2/-/commits/main)

</div>

# Support Site

If you are using Patchwork's VEGAS2, we provide a [full online dashboard with documentation](http://vegas2.patchwork.systems/). All settings for the bot can be changed via this site. Some modules get extended features also usable through the web interface!

# Support Discord

Need more help? Join the PatchFork Support server and we will help figure it out!

[![Discord Support Server](https://img.shields.io/discord/794422514935529493?color=5865F2&label=Discord&logo=discord&logoColor=white)](https://discord.gg/Cx65DkJZtM)

# Servers using VEGAS2

- [![Official Game Theorists](https://img.shields.io/discord/353694095220408322?color=5865F2&label=Discord&logo=discord&logoColor=white)](https://discord.gg/GameTheorists). 

# Contributing

We accept all kinds of contributions! Have an idea but don't know how to code? Submit an Issue! Not sure how to code, but want to help explain how to use the bot? You can help write documentation! Join the Support Discord and we can help find something!

# Developer Support

Most of VEGAS2's functions are provided entirely for free. We try to only premium for things that are excessive, high storage, high network, or high processing. If you want to help contribute to VEGAS2, [Maybax's](http://maybax.patchwork.systems/) and our other projects consider donating below!

[![Patchwork/Developer Patreon](https://img.shields.io/badge/Developer%20Patreon-Patchwork%20Patreon-%23ff424d?logo=patreon)]
