FROM python:3.10.13

ENV PYTHONUNBUFFERED 1

WORKDIR .

RUN apt-get update -y && apt-get upgrade -y
RUN apt-get install build-essential git -y

RUN pip install --upgrade pip
RUN pip install pip-tools

RUN mkdir /app

COPY ./requirements/ /app/requirements/

RUN pip-compile /app/requirements/requirements.in /app/requirements/dev_requirements.in --output-file /app/requirements/requirements.txt
RUN pip-sync /app/requirements/requirements.txt
