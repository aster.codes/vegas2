import re
import subprocess

import nox

LOCATIONS = ["vegas2", "noxfile.py"]

nox.options.sessions = [
    "reformat_code",
    "spell_check",
    # "type_check_code",
    "flake8_code",
    "pylint_code",
    # "pytest_code",
]


def build_deps(session: nox.Session):
    session.install("pip-tools")
    session.run(
        "pip-compile",
        "requirements/requirements.in",
        "requirements/dev_requirements.in",
        "--output-file",
        "requirements/requirements.txt",
    )
    session.run("pip-sync", "requirements/requirements.txt")


@nox.session(reuse_venv=True)
def build(session: nox.Session) -> None:
    """Build this project using flit."""
    session.install("flit")
    session.log("Starting build")
    session.run("flit", "build")


@nox.session(reuse_venv=True)
def type_check_code(session: nox.Session):
    """Run PyRight to Type Check code."""
    build_deps(session)
    session.run("pyright", *LOCATIONS[:-1])


@nox.session(reuse_venv=True)
def reformat_code(session: nox.Session):
    """Run Black and isort to format code."""
    build_deps(session)
    session.run("black", *LOCATIONS)
    session.run("isort", *LOCATIONS)


@nox.session(reuse_venv=True)
def pylint_code(session: nox.Session):
    """Run PyLint  to lint code."""
    build_deps(session)
    session.install("pylint-exit")
    session.run("rm", "-rf", "./pylint", external=True)
    session.run("mkdir", "./pylint", external=True)
    pylint_result = subprocess.run(
        ["pylint vegas2 || pylint-exit $?"],
        shell=True,
        capture_output=True,
    )
    pylint_result = pylint_result.stdout.decode("utf-8")
    with open("pylint/report.log", "w") as f:
        f.write(pylint_result)
    pylint_score_re = r"^Your code has been rated at ([\d\.\/]+)"
    match = re.search(pylint_score_re, pylint_result, re.MULTILINE)
    session.run(
        "anybadge",
        "--label=Pylint",
        "--file=pylint/pylint.svg",
        f"--value={match.group(0)}",
        "2=red",
        "4=orange",
        "8=yellow",
        "10=green",
    )
    session.run("bash", "-c", f'echo "Pylint score is {match.group(0)}"', external=True)


@nox.session(reuse_venv=True)
def flake8_code(session: nox.Session):
    """Run Flake8  to lint code."""
    build_deps(session)
    session.run("flake8", *LOCATIONS[:-1])


@nox.session(reuse_venv=True)
def prune_code(session: nox.Session):
    """Run Vulture  to find duplicate code."""
    build_deps(session)
    session.run("vulture", *LOCATIONS[:-1])


@nox.session(reuse_venv=True)
def spell_check(session: nox.Session):
    """Run codespell to spell check code."""
    build_deps(session)
    session.install("codespell")
    session.run("codespell", "-I", ".ignore-words", *LOCATIONS, "README.md")


@nox.session(reuse_venv=True)
def pytest_code(session: nox.Session):
    """Run pytest to test code."""
    build_deps(session)
    session.run(
        "pytest",
        *LOCATIONS,
    )
    session.run("coverage", "xml")


@nox.session(reuse_venv=True, python="3.10.1")
def build_docs(session: nox.Session):
    """Build documentation for this project"""
    build_deps(session)
    session.run(
        "pdoc",
        "-d",
        "numpy",
        "--logo",
        "https://vegas2.patchwork.systems/static/img/logo.png",
        "--logo-link",
        "https://vegas2.patchwork.systems/",
        "./vegas2",
        "--output-dir",
        "docs",
        "-t",
        "templates",
    )


@nox.session(reuse_venv=True, python="3.10.1")
def run_docs(session: nox.Session):
    """Build documentation for this project"""
    build_deps(session)
    session.run(
        "pdoc",
        "-d",
        "numpy",
        "--logo",
        "https://vegas2.patchwork.systems/static/img/logo.png",
        "--logo-link",
        "https://vegas2.patchwork.systems/",
        "./vegas2",
        "-t",
        "templates",
    )


@nox.session(reuse_venv=True, python="3.10.1")
def deploy_docs(session: nox.Session):
    """Build documentation for this project"""
    build_deps(session)
    session.run(
        "pdoc",
        "-d",
        "numpy",
        "--logo",
        "https://vegas2.patchwork.systems/static/img/logo.png",
        "--logo-link",
        "https://vegas2.patchwork.systems/",
        "./vegas2",
        "--output-dir",
        "docs",
        "-t",
        "templates",
    )
    session.run("rsync", "-avzh", "--delete", "docs/.", "mercury:/srv/vegas2_docs")


@nox.session(reuse_venv=True)
def clean(session: nox.Session):
    """Cleanup build directories."""
    session.run(
        "rm",
        "-rf",
        "html_coverage_report/",
        "coverage.xml",
        "docs",
        "pylint",
        "__pycache__",
        external=True,
    )
    session.run("find", ".", "-regex", "^.*\(__pycache__\|\.py[co]\)$", "-delete", external=True)
