"""Runs the Vegas2 bot."""
import os

from vegas2.client import build_bot

if os.name != "nt":
    import uvloop

    uvloop.install()

if __name__ == "__main__":
    build_bot().run()
