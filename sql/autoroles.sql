--
-- Create model AutoRolesChannel
--
CREATE TABLE public.autoroles_autoroleschannel ("id" bigserial NOT NULL PRIMARY KEY, "channel_ids" bigint[] NULL, "guild_id" bigint NOT NULL UNIQUE);
--
-- Create model AutoRoles
--
CREATE TABLE public.autoroles_autoroles ("id" bigserial NOT NULL PRIMARY KEY, "emoji" varchar(255) NOT NULL, "custom_emoji_id" bigint NULL UNIQUE, "role_id" bigint NOT NULL UNIQUE, "guild_id" bigint NOT NULL);
--
-- Create constraint autoroles_emoji_role_idx on model autoroles
--
ALTER TABLE public.autoroles_autoroles ADD CONSTRAINT "autoroles_emoji_role_idx" UNIQUE ("custom_emoji_id", "role_id");
ALTER TABLE public.autoroles_autoroleschannel ADD CONSTRAINT "autoroles_autorolesc_guild_id_c68b4705_fk_guildowne" FOREIGN KEY ("guild_id") REFERENCES public.guildownersettings_guildownersettings ("id") DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE public.autoroles_autoroles ADD CONSTRAINT "autoroles_autoroles_guild_id_97f5f3aa_fk_guildowne" FOREIGN KEY ("guild_id") REFERENCES public.guildownersettings_guildownersettings ("id") DEFERRABLE INITIALLY DEFERRED;
CREATE INDEX "autoroles_autoroles_guild_id_97f5f3aa" ON public.autoroles_autoroles ("guild_id");
