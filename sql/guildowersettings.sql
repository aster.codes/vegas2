--
-- Create model GuildOwnerSettings
--
CREATE TABLE public.guildownersettings_guildownersettings ("id" bigserial NOT NULL PRIMARY KEY, "guild_id" bigint NOT NULL UNIQUE, "logging_channel" bigint NULL, "vc_logging_channel" bigint NULL, "mod_logging_channel" bigint NULL);

--
-- Add field auto_delete_channel_ids to guildownersettings
--
ALTER TABLE public.guildownersettings_guildownersettings ADD COLUMN "auto_delete_channel_ids" bigint[] NULL;
--
-- Add field auto_delete_enabled to guildownersettings
--
ALTER TABLE public.guildownersettings_guildownersettings ADD COLUMN "auto_delete_enabled" boolean DEFAULT false NOT NULL;
ALTER TABLE public.guildownersettings_guildownersettings ALTER COLUMN "auto_delete_enabled" DROP DEFAULT;
--
-- Add field logging_enabled to guildownersettings
--
ALTER TABLE public.guildownersettings_guildownersettings ADD COLUMN "logging_enabled" boolean DEFAULT false NOT NULL;
ALTER TABLE public.guildownersettings_guildownersettings ALTER COLUMN "logging_enabled" DROP DEFAULT;
