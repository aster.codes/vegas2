--
-- Create model QuestionOfTheDay
--
CREATE TABLE public.qotd_questionoftheday ("id" serial NOT NULL PRIMARY KEY, "question" text NOT NULL, "date_added" date NOT NULL, "date_posted" date NOT NULL, "archived" boolean NULL, "avatar_url" varchar(255) NOT NULL, "nickname" varchar(255) NOT NULL, "author_id" integer NOT NULL);
--
-- Create model AnswerOfTheDay
--
CREATE TABLE public.qotd_answeroftheday ("id" serial NOT NULL PRIMARY KEY, "content" text NOT NULL, "avatar_url" varchar(255) NOT NULL, "nickname" varchar(255) NOT NULL, "author_id" integer NOT NULL, "question_id" integer NOT NULL);
ALTER TABLE public.qotd_questionoftheday ADD CONSTRAINT "qotd_questionoftheda_author_id_e79c9d8e_fk_discord_u" FOREIGN KEY ("author_id") REFERENCES public.discord_user_discorduser ("id") DEFERRABLE INITIALLY DEFERRED;
CREATE INDEX "qotd_questionoftheday_archived_8d26f78c" ON public.qotd_questionoftheday ("archived");
CREATE INDEX "qotd_questionoftheday_author_id_e79c9d8e" ON public.qotd_questionoftheday ("author_id");
ALTER TABLE public.qotd_answeroftheday ADD CONSTRAINT "qotd_answeroftheday_author_id_1328fec8_fk_discord_u" FOREIGN KEY ("author_id") REFERENCES public.discord_user_discorduser ("id") DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE public.qotd_answeroftheday ADD CONSTRAINT "qotd_answeroftheday_question_id_b1520913_fk_qotd_ques" FOREIGN KEY ("question_id") REFERENCES public.qotd_questionoftheday ("id") DEFERRABLE INITIALLY DEFERRED;
CREATE INDEX "qotd_answeroftheday_author_id_1328fec8" ON public.qotd_answeroftheday ("author_id");
CREATE INDEX "qotd_answeroftheday_question_id_b1520913" ON public.qotd_answeroftheday ("question_id");
