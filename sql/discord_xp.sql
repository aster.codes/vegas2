--
-- Create model UserXP
--
CREATE TABLE public.discord_xp_userxp ("id" serial NOT NULL PRIMARY KEY, "score" bigint NOT NULL, "multiplier" integer NOT NULL, "multiplier_timeout" timestamp with time zone NULL, "user_id" integer NOT NULL UNIQUE);
ALTER TABLE public.discord_xp_userxp ADD CONSTRAINT "discord_xp_userxp_user_id_65d5e3a7_fk_discord_u" FOREIGN KEY ("user_id") REFERENCES public.discord_user_discorduser ("id") DEFERRABLE INITIALLY DEFERRED;
CREATE INDEX "discord_xp_userxp_score_0d97af93" ON public.discord_xp_userxp ("score");
