--
-- PostgreSQL database dump
--

-- Dumped from database version 12.5 (Ubuntu 12.5-0ubuntu0.20.10.1)
-- Dumped by pg_dump version 12.5 (Ubuntu 12.5-0ubuntu0.20.10.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: discord_user_discorduser; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.discord_user_discorduser (
    id integer NOT NULL,
    discord_guild_id bigint NOT NULL,
    discord_user_id bigint NOT NULL
);


ALTER TABLE public.discord_user_discorduser OWNER TO postgres;

--
-- Name: discord_user_discorduser_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.discord_user_discorduser_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.discord_user_discorduser_id_seq OWNER TO postgres;

--
-- Name: discord_user_discorduser_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.discord_user_discorduser_id_seq OWNED BY public.discord_user_discorduser.id;


--
-- Name: discord_user_discorduser id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.discord_user_discorduser ALTER COLUMN id SET DEFAULT nextval('public.discord_user_discorduser_id_seq'::regclass);


--
-- Name: discord_user_discorduser discord_user_discorduser_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.discord_user_discorduser
    ADD CONSTRAINT discord_user_discorduser_pkey PRIMARY KEY (id);


--
-- Name: discord_user_discorduser_discord_guild_id_5a1d7f2c; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX discord_user_discorduser_discord_guild_id_5a1d7f2c ON public.discord_user_discorduser USING btree (discord_guild_id);


--
-- Name: discord_user_discorduser_discord_user_id_7fdeeed8; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX discord_user_discorduser_discord_user_id_7fdeeed8 ON public.discord_user_discorduser USING btree (discord_user_id);


--
-- Name: TABLE discord_user_discorduser; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public.discord_user_discorduser TO postgres;


--
-- PostgreSQL database dump complete
--


--
-- PostgreSQL database dump
--

-- Dumped from database version 12.5 (Ubuntu 12.5-0ubuntu0.20.10.1)
-- Dumped by pg_dump version 12.5 (Ubuntu 12.5-0ubuntu0.20.10.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: discord_user_discordban; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.discord_user_discordban (
    id integer NOT NULL,
    date_recieved timestamp with time zone NOT NULL,
    reason text,
    revoke_date timestamp with time zone,
    rejoin_date timestamp with time zone,
    discord_user_id integer NOT NULL
);


ALTER TABLE public.discord_user_discordban OWNER TO postgres;

--
-- Name: discord_user_discordban_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.discord_user_discordban_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.discord_user_discordban_id_seq OWNER TO postgres;

--
-- Name: discord_user_discordban_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.discord_user_discordban_id_seq OWNED BY public.discord_user_discordban.id;


--
-- Name: discord_user_discordban id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.discord_user_discordban ALTER COLUMN id SET DEFAULT nextval('public.discord_user_discordban_id_seq'::regclass);


--
-- Name: discord_user_discordban discord_user_discordban_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.discord_user_discordban
    ADD CONSTRAINT discord_user_discordban_pkey PRIMARY KEY (id);


--
-- Name: discord_user_discordban_discord_user_id_0d2cd316; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX discord_user_discordban_discord_user_id_0d2cd316 ON public.discord_user_discordban USING btree (discord_user_id);


--
-- Name: discord_user_discordban discord_user_discord_discord_user_id_0d2cd316_fk_discord_u; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.discord_user_discordban
    ADD CONSTRAINT discord_user_discord_discord_user_id_0d2cd316_fk_discord_u FOREIGN KEY (discord_user_id) REFERENCES public.discord_user_discorduser(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: TABLE discord_user_discordban; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public.discord_user_discordban TO postgres;


--
-- PostgreSQL database dump complete
--

--
-- PostgreSQL database dump
--

-- Dumped from database version 12.5 (Ubuntu 12.5-0ubuntu0.20.10.1)
-- Dumped by pg_dump version 12.5 (Ubuntu 12.5-0ubuntu0.20.10.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: discord_user_discordkick; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.discord_user_discordkick (
    id integer NOT NULL,
    date_recieved timestamp with time zone NOT NULL,
    reason text,
    rejoin_date timestamp with time zone,
    discord_user_id integer NOT NULL
);


ALTER TABLE public.discord_user_discordkick OWNER TO postgres;

--
-- Name: discord_user_discordkick_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.discord_user_discordkick_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.discord_user_discordkick_id_seq OWNER TO postgres;

--
-- Name: discord_user_discordkick_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.discord_user_discordkick_id_seq OWNED BY public.discord_user_discordkick.id;


--
-- Name: discord_user_discordkick id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.discord_user_discordkick ALTER COLUMN id SET DEFAULT nextval('public.discord_user_discordkick_id_seq'::regclass);


--
-- Name: discord_user_discordkick discord_user_discordkick_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.discord_user_discordkick
    ADD CONSTRAINT discord_user_discordkick_pkey PRIMARY KEY (id);


--
-- Name: discord_user_discordkick_discord_user_id_0a63fd91; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX discord_user_discordkick_discord_user_id_0a63fd91 ON public.discord_user_discordkick USING btree (discord_user_id);


--
-- Name: discord_user_discordkick discord_user_discord_discord_user_id_0a63fd91_fk_discord_u; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.discord_user_discordkick
    ADD CONSTRAINT discord_user_discord_discord_user_id_0a63fd91_fk_discord_u FOREIGN KEY (discord_user_id) REFERENCES public.discord_user_discorduser(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: TABLE discord_user_discordkick; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public.discord_user_discordkick TO postgres;


--
-- PostgreSQL database dump complete
--

--
-- PostgreSQL database dump
--

-- Dumped from database version 12.5 (Ubuntu 12.5-0ubuntu0.20.10.1)
-- Dumped by pg_dump version 12.5 (Ubuntu 12.5-0ubuntu0.20.10.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: discord_user_discordmodcontact; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.discord_user_discordmodcontact (
    id integer NOT NULL,
    date_recieved timestamp with time zone NOT NULL,
    reason text,
    urgency integer NOT NULL,
    contact_back boolean NOT NULL,
    discord_user_id integer NOT NULL,
    closed boolean NOT NULL
);


ALTER TABLE public.discord_user_discordmodcontact OWNER TO postgres;

--
-- Name: discord_user_discordmodcontact_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.discord_user_discordmodcontact_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.discord_user_discordmodcontact_id_seq OWNER TO postgres;

--
-- Name: discord_user_discordmodcontact_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.discord_user_discordmodcontact_id_seq OWNED BY public.discord_user_discordmodcontact.id;


--
-- Name: discord_user_discordmodcontact id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.discord_user_discordmodcontact ALTER COLUMN id SET DEFAULT nextval('public.discord_user_discordmodcontact_id_seq'::regclass);


--
-- Name: discord_user_discordmodcontact discord_user_discordmodcontact_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.discord_user_discordmodcontact
    ADD CONSTRAINT discord_user_discordmodcontact_pkey PRIMARY KEY (id);


--
-- Name: discord_user_discordmodcontact_discord_user_id_1a3d93a0; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX discord_user_discordmodcontact_discord_user_id_1a3d93a0 ON public.discord_user_discordmodcontact USING btree (discord_user_id);


--
-- Name: discord_user_discordmodcontact discord_user_discord_discord_user_id_1a3d93a0_fk_discord_u; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.discord_user_discordmodcontact
    ADD CONSTRAINT discord_user_discord_discord_user_id_1a3d93a0_fk_discord_u FOREIGN KEY (discord_user_id) REFERENCES public.discord_user_discorduser(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: TABLE discord_user_discordmodcontact; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public.discord_user_discordmodcontact TO postgres;


--
-- PostgreSQL database dump complete
--

--
-- PostgreSQL database dump
--

-- Dumped from database version 12.5 (Ubuntu 12.5-0ubuntu0.20.10.1)
-- Dumped by pg_dump version 12.5 (Ubuntu 12.5-0ubuntu0.20.10.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: discord_user_discordmodcontactresponses; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.discord_user_discordmodcontactresponses (
    id integer NOT NULL,
    date timestamp with time zone NOT NULL,
    message text NOT NULL,
    mod_response boolean NOT NULL,
    contact_id integer NOT NULL
);


ALTER TABLE public.discord_user_discordmodcontactresponses OWNER TO postgres;

--
-- Name: discord_user_discordmodcontactresponses_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.discord_user_discordmodcontactresponses_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.discord_user_discordmodcontactresponses_id_seq OWNER TO postgres;

--
-- Name: discord_user_discordmodcontactresponses_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.discord_user_discordmodcontactresponses_id_seq OWNED BY public.discord_user_discordmodcontactresponses.id;


--
-- Name: discord_user_discordmodcontactresponses id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.discord_user_discordmodcontactresponses ALTER COLUMN id SET DEFAULT nextval('public.discord_user_discordmodcontactresponses_id_seq'::regclass);


--
-- Name: discord_user_discordmodcontactresponses discord_user_discordmodcontactresponses_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.discord_user_discordmodcontactresponses
    ADD CONSTRAINT discord_user_discordmodcontactresponses_pkey PRIMARY KEY (id);


--
-- Name: discord_user_discordmodcontactresponses_contact_id_8f786d54; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX discord_user_discordmodcontactresponses_contact_id_8f786d54 ON public.discord_user_discordmodcontactresponses USING btree (contact_id);


--
-- Name: discord_user_discordmodcontactresponses discord_user_discord_contact_id_8f786d54_fk_discord_u; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.discord_user_discordmodcontactresponses
    ADD CONSTRAINT discord_user_discord_contact_id_8f786d54_fk_discord_u FOREIGN KEY (contact_id) REFERENCES public.discord_user_discordmodcontact(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: TABLE discord_user_discordmodcontactresponses; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public.discord_user_discordmodcontactresponses TO postgres;


--
-- PostgreSQL database dump complete
--

--
-- PostgreSQL database dump
--

-- Dumped from database version 12.5 (Ubuntu 12.5-0ubuntu0.20.10.1)
-- Dumped by pg_dump version 12.5 (Ubuntu 12.5-0ubuntu0.20.10.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: discord_user_discordrole; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.discord_user_discordrole (
    id integer NOT NULL,
    date_recieved timestamp with time zone NOT NULL,
    reason text,
    date_expired timestamp with time zone,
    role_id character varying(255) NOT NULL,
    discord_user_id integer NOT NULL,
    removed boolean NOT NULL
);


ALTER TABLE public.discord_user_discordrole OWNER TO postgres;

--
-- Name: discord_user_discordrole_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.discord_user_discordrole_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.discord_user_discordrole_id_seq OWNER TO postgres;

--
-- Name: discord_user_discordrole_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.discord_user_discordrole_id_seq OWNED BY public.discord_user_discordrole.id;


--
-- Name: discord_user_discordrole id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.discord_user_discordrole ALTER COLUMN id SET DEFAULT nextval('public.discord_user_discordrole_id_seq'::regclass);


--
-- Name: discord_user_discordrole discord_user_discordrole_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.discord_user_discordrole
    ADD CONSTRAINT discord_user_discordrole_pkey PRIMARY KEY (id);


--
-- Name: discord_user_discordrole_discord_user_id_44dae02f; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX discord_user_discordrole_discord_user_id_44dae02f ON public.discord_user_discordrole USING btree (discord_user_id);


--
-- Name: discord_user_discordrole_role_id_11ae249f; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX discord_user_discordrole_role_id_11ae249f ON public.discord_user_discordrole USING btree (role_id);


--
-- Name: discord_user_discordrole_role_id_11ae249f_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX discord_user_discordrole_role_id_11ae249f_like ON public.discord_user_discordrole USING btree (role_id varchar_pattern_ops);


--
-- Name: discord_user_discordrole discord_user_discord_discord_user_id_44dae02f_fk_discord_u; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.discord_user_discordrole
    ADD CONSTRAINT discord_user_discord_discord_user_id_44dae02f_fk_discord_u FOREIGN KEY (discord_user_id) REFERENCES public.discord_user_discorduser(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: TABLE discord_user_discordrole; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public.discord_user_discordrole TO postgres;


--
-- PostgreSQL database dump complete
--

--
-- PostgreSQL database dump
--

-- Dumped from database version 12.5 (Ubuntu 12.5-0ubuntu0.20.10.1)
-- Dumped by pg_dump version 12.5 (Ubuntu 12.5-0ubuntu0.20.10.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: discord_user_discordstrike; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.discord_user_discordstrike (
    id integer NOT NULL,
    date_recieved timestamp with time zone NOT NULL,
    reason text,
    strike_type character varying(255) NOT NULL,
    discord_user_id integer NOT NULL
);


ALTER TABLE public.discord_user_discordstrike OWNER TO postgres;

--
-- Name: discord_user_discordstrike_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.discord_user_discordstrike_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.discord_user_discordstrike_id_seq OWNER TO postgres;

--
-- Name: discord_user_discordstrike_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.discord_user_discordstrike_id_seq OWNED BY public.discord_user_discordstrike.id;


--
-- Name: discord_user_discordstrike id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.discord_user_discordstrike ALTER COLUMN id SET DEFAULT nextval('public.discord_user_discordstrike_id_seq'::regclass);


--
-- Name: discord_user_discordstrike discord_user_discordstrike_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.discord_user_discordstrike
    ADD CONSTRAINT discord_user_discordstrike_pkey PRIMARY KEY (id);


--
-- Name: discord_user_discordstrike_discord_user_id_f8f59cb9; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX discord_user_discordstrike_discord_user_id_f8f59cb9 ON public.discord_user_discordstrike USING btree (discord_user_id);


--
-- Name: discord_user_discordstrike_strike_type_1bb1f254; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX discord_user_discordstrike_strike_type_1bb1f254 ON public.discord_user_discordstrike USING btree (strike_type);


--
-- Name: discord_user_discordstrike_strike_type_1bb1f254_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX discord_user_discordstrike_strike_type_1bb1f254_like ON public.discord_user_discordstrike USING btree (strike_type varchar_pattern_ops);


--
-- Name: discord_user_discordstrike discord_user_discord_discord_user_id_f8f59cb9_fk_discord_u; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.discord_user_discordstrike
    ADD CONSTRAINT discord_user_discord_discord_user_id_f8f59cb9_fk_discord_u FOREIGN KEY (discord_user_id) REFERENCES public.discord_user_discorduser(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: TABLE discord_user_discordstrike; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public.discord_user_discordstrike TO postgres;


--
-- PostgreSQL database dump complete
--

--
-- PostgreSQL database dump
--

-- Dumped from database version 12.5 (Ubuntu 12.5-0ubuntu0.20.10.1)
-- Dumped by pg_dump version 12.5 (Ubuntu 12.5-0ubuntu0.20.10.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: discord_user_discordwarn; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.discord_user_discordwarn (
    id integer NOT NULL,
    date_recieved timestamp with time zone NOT NULL,
    reason text,
    discord_user_id integer NOT NULL
);


ALTER TABLE public.discord_user_discordwarn OWNER TO postgres;

--
-- Name: discord_user_discordwarn_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.discord_user_discordwarn_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.discord_user_discordwarn_id_seq OWNER TO postgres;

--
-- Name: discord_user_discordwarn_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.discord_user_discordwarn_id_seq OWNED BY public.discord_user_discordwarn.id;


--
-- Name: discord_user_discordwarn id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.discord_user_discordwarn ALTER COLUMN id SET DEFAULT nextval('public.discord_user_discordwarn_id_seq'::regclass);


--
-- Name: discord_user_discordwarn discord_user_discordwarn_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.discord_user_discordwarn
    ADD CONSTRAINT discord_user_discordwarn_pkey PRIMARY KEY (id);


--
-- Name: discord_user_discordwarn_discord_user_id_46e0f774; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX discord_user_discordwarn_discord_user_id_46e0f774 ON public.discord_user_discordwarn USING btree (discord_user_id);


--
-- Name: discord_user_discordwarn discord_user_discord_discord_user_id_46e0f774_fk_discord_u; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.discord_user_discordwarn
    ADD CONSTRAINT discord_user_discord_discord_user_id_46e0f774_fk_discord_u FOREIGN KEY (discord_user_id) REFERENCES public.discord_user_discorduser(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: TABLE discord_user_discordwarn; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public.discord_user_discordwarn TO postgres;


--
-- PostgreSQL database dump complete
--
--
