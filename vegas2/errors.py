"""Exceptions raised during the handling of any events in VEGAS2."""
from .events.base_events import VEGAS2Event

__all__ = [
    "AutoRoleUpsertFailedException",
]


class AutoRoleUpsertFailedException(Exception):
    """Raised if an AutoroleEntry DB Insert/Upsert into Fails."""

    def __init__(self, *args, event: VEGAS2Event, **kwargs):
        super().__init__(*args, **kwargs)
        self.event = event
