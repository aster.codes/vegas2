"""Welcome to the V.E.G.A.S.2 documentation!

We hope to fill this documentation out as time goes on. If you'd like to help contribue
please submit any PR's through gitlab and use the NumPy docstring format.

Important

This is meant to act as documentation for the commands and functions of the V.E.G.A.S. bot,
not as technical documentation for the code. Due to this, any functions/methods listed under
vegas2.plugins are representations of slash commands/groups. Likewise, `Parameters` listed are
not what the functions expect, but what the Slash Commands will require."""
import typing

__all__ = [
    "__author__",
    "__ci__",
    "__copyright__",
    "__docs__",
    "__url__",
    "__repo__",
    "checks",
    "models",
    "plugins",
    "util",
    "client",
    "config",
    "events",
    "impl",
]

from . import checks, client, config, events, impl, models, plugins, util

__author__: typing.Final[str] = "Patchwork Collective"
__ci__: typing.Final[str] = "https://gitlab.com/aster.codes/vegas2/-/pipelines"
__copyright__: typing.Final[str] = "© 2021-2022 Patchwork Collective"
__docs__: typing.Final[str] = "https://patchwork.systems/docs"
__url__: typing.Final[str] = "https://vegas2.patchwork.systems/"
__repo__: typing.Final[str] = "https://gitlab.com/aster.codes/vegas2"
__version__: typing.Final[str] = "0.1.0"
