from __future__ import annotations

import logging

import aiopg
import hikari
from psycopg2.extras import RealDictCursor

__all__ = [
    "ensure_guild_settings_exist",
]

logger = logging.getLogger(__name__)


async def ensure_guild_settings_exist(guild_id: hikari.Snowflake | int, pg_pool: aiopg.Pool) -> int | None:
    guild_id_str = str(guild_id)
    with await pg_pool.cursor(cursor_factory=RealDictCursor) as cursor:
        try:
            await cursor.execute(
                """
                INSERT INTO guildownersettings_guildownersettings (guild_id, logging_enabled, auto_delete_enabled)
                VALUES (%s, %s, %s )
                ON CONFLICT (guild_id)
                DO NOTHING RETURNING id;
                """,
                (guild_id_str, False, False),
            )
            result = await cursor.fetchone()

            if not result:
                await cursor.execute(
                    "SELECT id FROM guildownersettings_guildownersettings WHERE guild_id=%s LIMIT 1;",
                    (guild_id,),
                )
                result = await cursor.fetchone()

            return result["id"]
        except Exception as e:
            logger.error(e)
            return None
