import re

__all__ = [
    "STAFF_ROLES",
    "EVERYONE_PATTERN",
    "HERE_PATTERN",
    "INVITE_PATTERN",
    "REDIS_SPAM_KEY",
]

STAFF_ROLES = [
    "Admin",
    "Mods",
    "Disaster Director",
    "Server Glue",
    "Approved Theorizer",
    "Approved Artist",
    "Approved Writer",
    "Approved Musician",
    "Expert Meme-er",
]

EVERYONE_PATTERN = re.compile(r"@everyone")
HERE_PATTERN = re.compile(r"@here")
INVITE_PATTERN = re.compile(
    r"(?:https?://)?discord(?:app\.com/invite|\.gg)/?[a-zA-Z0-9]+/?",
    re.DOTALL,
)

REDIS_SPAM_KEY = "{guild_id}:{member_id}:spam_check"
