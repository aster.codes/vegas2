"""Constants used in the Question of the Day plugin."""

__all__ = [
    "DEFAULT_QOTD_PFP",
    "COLOURS",
    "EMBED_DESC",
    "EMBED_ICON",
]
DEFAULT_QOTD_PFP = (
    "https://cdn.discordapp.com/attachments/733789542884048906/899164219499495474/89c588852f0911ecb0c6f23c91500735.png"
)
COLOURS = [
    0xDE392E,
    0xDF9326,
    0xE6E13D,
    0x37CB39,
    0x3D9DCE,
    0x5640D2,
]
EMBED_DESC = "Every day we post a question that is thought provoking, educational, or just fun! Answer in this channel!"
EMBED_ICON = "https://cdn.discordapp.com/attachments/733789542884048906/899751996624625716/d1e4c923304f11ecaca5f23c91500735.png"
