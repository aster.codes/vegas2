"""Constants used in the tickets module."""
from __future__ import annotations

from hikari import Permissions

__all__ = [
    "DEFAULT_TICKET_CHAN_NAME",
    "DEFAULT_TICKET_CATEGORY_NAME",
    "MEMBER_NEW_TICKET_PERMISSIONS",
    "TICKETS_LOGO",
    "MAIN_TICKET_EMBED_DESC",
    "USER_TICKET_EMBED_DESC",
]

DEFAULT_TICKET_CHAN_NAME = "tickets"
"""Default ticket channel name."""

DEFAULT_TICKET_CATEGORY_NAME = "Help"
"""Default ticket category name."""

MEMBER_NEW_TICKET_PERMISSIONS = (
    Permissions.ATTACH_FILES | Permissions.READ_MESSAGE_HISTORY | Permissions.SEND_MESSAGES | Permissions.VIEW_CHANNEL
)
"""Permissions added to tickets for calling member."""

TICKETS_LOGO = "https://cdn.discordapp.com/attachments/733789542884048906/900079323279663175/85d744c5310511ecb705f23c91500735.png"
"""URL to Logo used in Tickets Embed"""

MAIN_TICKET_EMBED_DESC = (
    "If you need help with something you can submit a ticket! A new channel will "
    "be created and I will walk you through what to do. In that channel you will be "
    "able to talk directly to a Mod to ask questions and have access to send images or "
    "documents that might have private or sensitive information."
)
"""Description used in the embed in the main #tickets channel."""

USER_TICKET_EMBED_DESC = (
    "Thanks for submitting a ticket! We take all tickets "
    "very seriously. Please provide a full explanation in this "
    "channel. You can include text, images, files, video, or "
    "documents. \n\nPlease do not ping the Mods or Staff unless "
    "there is a life or death situation. Someone will address it "
    "available."
)
"""Description used in embed in individual user tickets."""
