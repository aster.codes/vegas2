WARN_CHECK_SQL = (
    "SELECT * FROM discord_user_discordwarn WHERE discord_user_id=%(member_id)s ORDER BY date_recieved DESC LIMIT %(warn_limit)s;"
)
ROLE_CHECK_SQL = (
    "SELECT * FROM discord_user_discordrole WHERE discord_user_id=%(member_id)s ORDER BY date_recieved DESC LIMIT %(role_limit)s;"
)
STRIKE_CHECK_SQL = "SELECT * FROM discord_user_discordstrike WHERE discord_user_id=%(member_id)s ORDER BY date_recieved DESC LIMIT %(strike_limit)s;"
KICK_CHECK_SQL = (
    "SELECT * FROM discord_user_discordkick WHERE discord_user_id=%(member_id)s ORDER BY date_recieved DESC LIMIT %(kick_limit)s;"
)
BAN_CHECK_SQL = (
    "SELECT * FROM discord_user_discordban WHERE discord_user_id=%(member_id)s ORDER BY date_recieved DESC LIMIT %(ban_limit)s;"
)

WARN_MEMBER_SQL = "INSERT INTO discord_user_discordwarn(date_recieved, reason, discord_user_id) VALUES(%(date_received)s, %(reason)s, %(discord_user_id)s);"
WARN_REMOVE_SQL = """DELETE FROM discord_user_discordwarn WHERE id=%(warn_id)s"""
STRIKE_MEMBER_SQL = """INSERT INTO discord_user_discordstrike(date_recieved, reason, strike_type, discord_user_id)
VALUES(%(date_received)s, %(reason)s, %(strike_type)s, %(discord_user_id)s);"""
STRIKE_REMOVE_SQL = """DELETE FROM discord_user_discordstrike WHERE id=%(strike_id)s"""
KICK_MEMBER_SQL = "INSERT INTO discord_user_discordkick(date_recieved, reason, discord_user_id) VALUES(%(date_received)s, %(reason)s, %(discord_user_id)s);"
BAN_MEMBER_SQL = "INSERT INTO discord_user_discordban(date_recieved, reason, discord_user_id) VALUES(%(date_received)s, %(reason)s, %(discord_user_id)s);"
MEMBER_EXISTS_SQL = """WITH SELECTED AS
(SELECT id
    FROM discord_user_discorduser
    WHERE discord_guild_id = '%(guild_id)s' AND discord_user_id = '%(member_id)s'
),
INSERTED AS (
    INSERT INTO discord_user_discorduser ("discord_guild_id", "discord_user_id")
    SELECT '%(guild_id)s', '%(member_id)s'
    WHERE NOT EXISTS (SELECT 1 FROM SELECTED)
    RETURNING id
)
SELECT id
    FROM INSERTED
UNION ALL
SELECT id
    FROM SELECTED"""
MODERATION_USER_CHECK_SQL = """
(SELECT id, date_recieved, reason, 'warn' AS type
FROM discord_user_discordwarn
    WHERE discord_user_id=%(member_id)s ORDER BY date_recieved DESC LIMIT %(warn_limit)s)
UNION
(SELECT id, date_recieved, reason, 'roles' AS type
FROM discord_user_discordrole
    WHERE discord_user_id=%(member_id)s ORDER BY date_recieved DESC LIMIT %(role_limit)s)
UNION
(SELECT id, date_recieved, reason, 'strike' AS type
FROM discord_user_discordstrike
    WHERE discord_user_id=%(member_id)s ORDER BY date_recieved DESC LIMIT %(strike_limit)s)
UNION
(SELECT id, date_recieved, reason, 'kick' AS type
FROM discord_user_discordkick
    WHERE discord_user_id=%(member_id)s ORDER BY date_recieved DESC LIMIT %(kick_limit)s)
UNION
(SELECT id, date_recieved, reason, 'ban' AS type
FROM discord_user_discordban
    WHERE discord_user_id=%(member_id)s ORDER BY date_recieved DESC LIMIT %(ban_limit)s);"""
