QOTD_LIST = "SELECT * FROM qotd_questionoftheday WHERE archived = false ORDER BY date_added asc LIMIT %(limit)s"
QOTD_LIST_ALL = "SELECT * FROM qotd_questionoftheday ORDER BY date_added asc LIMIT %(limit)s"
QOTD_GET_BY_DATE = (
    "SELECT * FROM qotd_questionoftheday WHERE date_posted::date = %(date_posted)s ORDER BY date_added asc LIMIT 1;"
)
QOTD_POST_BY_ID = "SELECT * FROM qotd_questionoftheday WHERE id = %(question_id)s LIMIT 1;"
QOTD_POST_FALLBACK = "SELECT * FROM qotd_questionoftheday WHERE archived = false ORDER BY date_added asc LIMIT 1;"
QOTD_ARCHIVE_QUESION = "UPDATE qotd_questionoftheday SET archived=TRUE, date_posted=%(date_posted)s WHERE id=%(question_id)s"
QOTD_DELETE_QUESTION = "DELETE FROM qotd_questionoftheday WHERE id=%(question_id)s"
QOTD_ADD_QUESTION = (
    "INSERT INTO qotd_questionoftheday (question, date_added, nickname, avatar_url, date_posted, archived, author_id)"
    " VALUES (%(question)s, %(date_added)s, %(nickname)s, %(avatar_url)s, %(date_posted)s, false, %(author_id)s)"
)
AOTD_ADD_ANSWER = (
    "INSERT INTO qotd_answeroftheday (content, avatar_url, nickname, author_id, question_id)"
    " VALUES (%(content)s, %(avatar_url)s, %(nickname)s, %(author_id)s, %(question_id)s);"
)
QOTD_POST = """WITH qotd_results as (
    SELECT * FROM qotd_questionoftheday
    WHERE archived = %(archived)s
    ORDER BY date_added asc
), qotd_by_id as (
    SELECT * FROM qotd_results
    WHERE id = %(question_id)s
    LIMIT 1
), qotd_by_date as (
    SELECT * FROM qotd_results
    WHERE date_posted::date = %(date_posted)s
), qotd_fallback as (
    SELECT * FROM qotd_results
    LIMIT 1
)
SELECT * FROM qotd_by_id
UNION ALL
SELECT * FROM qotd_by_date
WHERE (
    SELECT COUNT(*) FROM qotd_by_id
)=0
UNION ALL
SELECT * FROM qotd_fallback
WHERE (
    SELECT COUNT(*) FROM qotd_by_date
)=0"""
