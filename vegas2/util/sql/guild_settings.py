__all__ = [
    "GUILD_SETTINGS_SAVE",
    "GUILD_SETTINGS_DELETE",
]

GUILD_SETTINGS_SAVE = """UPDATE guildownersettings_guildownersettings
    SET logging_channel = %(logging_channel)s,
    vc_logging_channel = %(vc_logging_channel)s,
    mod_logging_channel = %(mod_logging_channel)s,
    auto_delete_channel_ids = %(auto_delete_channels_ids)s,
    logging_enabled = %(logging_enabled)s,
    auto_delete_enabled = %(auto_delete_enabled)s
WHERE guild_id = %(guild_id)s;"""
GUILD_SETTINGS_DELETE = """DELETE FROM guildownersettings_guildownersettings WHERE guild_id=%(guild_id)s;"""
