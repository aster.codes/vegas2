"""SQL statements used by VEGAS2 to save AutoRoleEntry data."""

__all__ = ["AUTOROLE_ENTRY_UPSERT", "AUTOROLE_ENTRY_DELETE"]
AUTOROLE_ENTRY_UPSERT = """INSERT INTO autoroles_autoroles
    (emoji, custom_emoji_id, role_id, guild_id)
VALUES
    (%(emoji)s, %(custom_emoji_id)s, %(role_id)s,
    (SELECT id from guildownersettings_guildownersettings where guild_id = %(guild_id)s))
ON CONFLICT (role_id)
DO
    UPDATE SET custom_emoji_id=EXCLUDED.custom_emoji_id, role_id=EXCLUDED.role_id
    WHERE autoroles_autoroles.custom_emoji_id=%(custom_emoji_id)s or autoroles_autoroles.role_id = %(role_id)s;"""
"""Used to add/update AutoRoleEntrys. If a conflict occurs on role_id, the UPDATE will execute"""
AUTOROLE_ENTRY_DELETE = """DELETE FROM autoroles_autoroles
WHERE guild_id=(SELECT id from guildownersettings_guildownersettings where guild_id=%(guild_id)s)
    AND (custom_emoji_id=%(custom_emoji_id)s OR emoji=%(emoji)s)"""
"""Used to delete AutoRoleEntrys"""
