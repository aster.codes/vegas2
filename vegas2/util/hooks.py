"""Provides error handling hooks for VEGAS2."""
from __future__ import annotations

import hikari
from tanjun import ConversionError, ParserError
from tanjun.abc import Context

__all__ = [
    "on_error",
    "on_parse_error",
]


async def on_error(ctx: Context, exception: BaseException) -> None:
    embed = hikari.Embed(
        title=f"An unexpected {type(exception).__name__} occurred",
        colour=16711680,
        description=f"```python\n{str(exception)[:1950]}```",
    )
    await ctx.respond(embed=embed)


async def on_parse_error(ctx: Context, exception: ParserError):
    message = str(exception)

    if isinstance(exception, ConversionError) and exception.errors:
        if len(exception.errors) > 1:
            message += ":\n* " + "\n* ".join(map("`{}`".format, exception.errors))

        else:
            message = f"{message}: `{exception.errors[0]}`"

    await ctx.respond(content=message)
