"""Dependency Injectors for Vegas2"""
from __future__ import annotations

import datetime
import io
import logging
import os
import re
import time

import aiopg
import emoji
import hikari
import httpx
import jinja2
import markdown2
import tanjun
from apscheduler.schedulers.asyncio import AsyncIOScheduler

from ..config import FullConfig

__all__ = [
    "HttpxInjector",
    "Jinja2Dep",
    "PostgresPoolDependency",
]

logger = logging.getLogger()


class Jinja2Dep:
    """A dependency handler for Jinja2 Templates."""

    __slots__ = ["_file_loader", "_env"]

    def __init__(self, templates_directory: str = "jinja2_templates"):
        """Setup the` `jinja2.FileSystemLoader` and `jinja2.Environment`

        Parameters
        ----------
        templates_directory: str
            A path to a directory containing Jinja2 templates.
        """
        self._file_loader = jinja2.FileSystemLoader(
            os.path.join(os.path.dirname(os.path.realpath(__file__)), templates_directory)
        )
        self._env = jinja2.Environment(loader=self._file_loader, autoescape=False)  # noqa: S701

    def get_template(self, template_name: str) -> jinja2.Template:
        """Get the `template_name` from the preconfigured Jinja2 Environment.

        Parameters
        ----------
        template_name: str
            The name of the template to retrieve.

        Returns
        -------
        jinja2.Template
            The template object."""
        return self._env.get_template(template_name)

    def sanitize_name(self, name: str):
        """Helper function to remove disallowed characters.

        Parameters
        ----------
        name: str
            A name to strip characters from.

        Returns
        -------
        str
            The string sanitized."""
        return (
            name.lower()
            .replace(" ", "-")
            .replace("'", "-")
            .replace("/", "-")
            .replace(".", "-")
            .replace("!", "-")
            .replace("=", "-")
            .replace("(", "-")
            .replace(")", "-")
        )

    async def message_content_chain(self, message: hikari.Message, client: tanjun.Client) -> tuple[str, list, list | None]:
        """Helper method to sanatize and convert a hikari.Message into safe content for Jinja.

        Parameters
        ----------
        message: hikari.Message
            The message to sanatize.
        client: tanjun.Client
            The client resource to use for API calls.

        Returns
        -------
        tuple[str, list, list | None]
            A tuple containing a string of Markdown text, a list of pings, and a list of attachments or None if no attachments.
        """
        attachments = []
        if len(message.attachments) > 0:
            for attachment in message.attachments:
                attachments.append((await client.metadata["image-saver"].asend((attachment, message))).attachments[0])
        if not message.content:
            return ("", None, attachments)
        message_content, pinged_mentions = await self.render_user_and_role_mentions(message, message.content, client)
        message_content = await self.render_custom_emoji(message, message_content, client)
        message_content = self.render_unicode_emoji(message, message_content)
        return (
            markdown2.markdown(f"{message_content}\n\n"),
            pinged_mentions,
            attachments,
        )

    async def archive_channel_asset(self, asset_channel: hikari.GuildTextChannel, client: tanjun.Client):
        """Helper method to send attachments, emoji, and other files to a preserving `asset_channel`.

        Parameters
        ----------
        asset_channel: hikari.GuildTextChannel
            A channel to save all file like assets for a channel.
        client: tanjun.Client
            The client resource to use."""
        parent_channel = await client.rest.fetch_channel(client.metadata["archive-channel"].parent_id)
        await asset_channel.send(
            f"{'=+'*15}\n"
            f"Starting new channel archival.\n"
            f"**Channel:** {client.metadata['archive-channel'].name}\n"
            f"**Channel ID:** {client.metadata['archive-channel'].id}\n"
            f"**Parent Channel:** {parent_channel.name}\n"
            f"**Parent ID:** {parent_channel.id}"
        )
        asset, message = yield
        while True:
            try:
                new_asset = await asset_channel.send(
                    content=f"*Message ID:* {message.id}\n*Author:* {message.author.username} [ID: {message.author.id}]",
                    attachment=asset,
                )
            except (
                hikari.NotFoundError,
                hikari.ForbiddenError,
                AttributeError,
                AttributeError,
            ):
                new_asset = await asset_channel.send(
                    content=(
                        f"*Message ID:* {message.id}\n*Author:* {message.author.username} "
                        "[ID: {message.author.id}]\n```Failed```"
                    ),
                    attachment="https://discord.com/assets/f9bb9c4af2b9c32a2c5ee0014661546d.png",
                )
            asset, message = yield new_asset

    def render_guild_roles_css(self, client: tanjun.Client) -> str:
        """Converts all hikari.Role's in a provided `guild` to a jinja2.Template of CSS classes matching the Roles color."""
        entry_output = ""
        for role_id in client.metadata["cached-roles"]:
            role = client.metadata["cached-roles"].get(role_id)
            if not role:
                continue
            role_name = self.sanitize_name(role.name)
            entry_output += self._env.get_template("channel-archive-styles-entry.html").render(
                role_name=role_name, color_code=role.color.hex_code
            )

        return self._env.get_template("channel-archive-styles-base.html").render(styles=entry_output)

    async def render_user_and_role_mentions(
        self, message: hikari.Message, message_content: str, client: tanjun.Client
    ) -> tuple[str, None] | tuple[str, list[hikari.Member | hikari.Role]]:
        """Helpder method to check for and color user and role mentions"""
        rest = message.app.rest
        pinged_mentions = None
        if mentions := message.mentions:
            pinged_mentions = []
            for user_id in mentions.user_ids:
                if client.metadata["cached-members"].get(user_id):
                    pinged_mentions.append(client.metadata["cached-members"].get(user_id))
                else:
                    try:
                        result = await rest.fetch_member(client.metadata["cached-guild"].id, user_id)
                        pinged_mentions.append(result)
                    except hikari.NotFoundError:
                        if user := mentions.users.get(user_id):
                            pinged_mentions.append(user)

            for role_id in mentions.role_ids:
                if client.metadata["cached-roles"].get(role_id):
                    pinged_mentions.append(client.metadata["cached-roles"].get(role_id))
                else:
                    results = await rest.fetch_roles(client.metadata["cached-guild"].id)
                    result = None
                    for role in results:
                        if role_id == role.id:
                            result = role
                    if result:
                        pinged_mentions.append(result)

            for mention in pinged_mentions:
                if not hasattr(mention, "id"):
                    continue
                classes = mention_name = search_str = ""
                if isinstance(mention, hikari.Member):
                    classes = f"class='{self.sanitize_name(mention.get_top_role().name)}'"
                    mention_name = f"{mention.username}#{mention.discriminator}"
                    search_str = f"<@!{mention.id}"
                elif isinstance(mention, hikari.Role):
                    classes = f"class='{self.sanitize_name(mention.name)}'"
                    mention_name = mention.name
                    search_str = f"<@&{mention.id}"
                mention_start = message_content.find(search_str)
                mention_end = mention_start + len(str(mention.id)) + 4
                pre_mention = message_content[:mention_start]
                mention = f"<span {classes}>@{mention_name}</span>"
                post_mention = message_content[mention_end:]

                message_content = f"{pre_mention} {mention} {post_mention}"
        return (message_content, None) if not pinged_mentions else (message_content, pinged_mentions)

    async def render_reactions(self, message: hikari.Message, client: tanjun.Client) -> None:
        reactions = {}
        for reaction in message.reactions or []:
            emoji_id = reaction.emoji.id if hasattr(reaction.emoji, "id") else reaction.emoji
            reactions[emoji_id] = {}
            reactions[emoji_id]["members"] = []
            reactions[emoji_id]["count"] = reaction.count
            reactions[emoji_id]["url"] = reaction.emoji.url
            reactions[emoji_id]["users"] = await client.rest.fetch_reactions_for_emoji(
                client.metadata["archive-channel"], message.id, reaction.emoji
            ).collect(collector=lambda user: user)
            if isinstance(reaction.emoji, hikari.UnicodeEmoji):
                reactions[emoji_id]["custom_emoji"] = False
                reactions[emoji_id]["name"] = emoji.demojize(reaction.emoji)
            elif isinstance(reaction.emoji, hikari.CustomEmoji):
                reactions[emoji_id]["custom_emoji"] = True
                reactions[emoji_id]["name"] = reaction.emoji.name
        return self.get_template("reactions.html").render(reactions=reactions)

    async def finalize_html_file(
        self,
        ctx: tanjun.abc.SlashContext,
        message_output: io.BytesIO,
        client: tanjun.Client,
        total_message_count,
    ):
        """Finalze the html channel archival.
        - Embeds the channel content into a larger template.
        - Customizes template with channel/guild names.
        - Deletes channel if not dry_run.
        - Sends html archive to proper channel.
        """
        html_file = io.BytesIO()

        message_output.seek(0)
        html_file.write(
            str.encode(
                self.get_template("channel-archive-base.html").render(
                    channel=client.metadata["archive-channel"],
                    guild=client.metadata["cached-guild"],
                    timestamp=datetime.datetime.now(),
                    styles=self.render_guild_roles_css(client),
                    bot_name="VEGAS2",
                    message_count=total_message_count,
                    body=message_output.getvalue().decode(),
                )
            )
        )

        if client.metadata["output-channel"]:
            await client.metadata["output-channel"].send(
                content=f"Channel archive for {client.metadata['archive-channel'].name} ready for download!",
                attachment=hikari.Bytes(
                    html_file.getvalue(),
                    f"{client.metadata['archive-channel'].name}-archive.html",
                ),
            )
        else:
            await ctx.edit_initial_response(
                content=f"Channel archive for {client.metadata['archive-channel'].name} ready for download!",
                attachment=hikari.Bytes(
                    html_file.getvalue(),
                    f"{client.metadata['archive-channel'].name}-archive.html",
                ),
            )

        client.metadata["ended"] = time.time()
        await ctx.respond(f"Archival ran for {client.metadata['ended'] - client.metadata['started']}")

        if not client.metadata["dry-run"]:
            await client.metadata["archive-channel"].delete()

    async def render_embeds(
        self,
        message: hikari.Message,
        bot: hikari.GatewayBotAware,
        client: tanjun.Client,
    ) -> str | None:
        """Converts a provided hikari.Embed to a jinja2.Template."""
        if not message.embeds:
            return None
        embeds_data = []
        for embed in message.embeds:
            embed_data = bot.entity_factory.serialize_embed(embed)[0]
            if embed_data.get("image"):
                if embed.image:
                    embed_data["image"]["url"] = (
                        (await client.metadata["image-saver"].asend((embed.image, message))).attachments[0].proxy_url
                    )
            if embed_data.get("thumbnail"):
                if embed.thumbnail:
                    embed_data["thumbnail"]["url"] = (
                        (await client.metadata["image-saver"].asend((embed.thumbnail, message))).attachments[0].proxy_url
                    )
            embeds_data.append(embed_data)
        return self.get_template("embeds.html").render(embeds=embeds_data)

    async def render_class_names(self, message: hikari.Message, client) -> str:
        """Returns a CSS class name for single `hikari.Message` during a channel archive.

        Parameters
        ----------
        message: hikari.Message
            The message to generate a CSS class name for.
        client: tanjun.Client
            The client resource to use.

        Returns
        -------
        str
            The CSS class name for the message."""
        cached_member = client.metadata["cached-members"].get(message.author.id)
        if not cached_member:
            cached_member = client.metadata["local-cached-members"].get(message.author.id)
            if not cached_member:
                try:
                    cached_member = await client.rest.fetch_member(client.metadata["cached-guild"].id, message.author.id)
                    client.metadata["local-cached-members"][message.author.id] = cached_member
                except hikari.NotFoundError:
                    return None
        return self.sanitize_name(cached_member.get_top_role().name)

    def render_video_embed(self, message: hikari.Message) -> str:
        """Helpder function to parse message.clean_content into a <video> element"""
        ...

    async def render_custom_emoji(self, message: hikari.Message, message_content: str, client: tanjun.Client) -> str:
        """Helper function to check for and replace custom discord emojis with an image that will render in html.

        Parameters
        ----------
        message: hikari.Message
            The message to attempt a render from.
        message_content: str
            The message content for the transform.
        client: tanjun.Client
            The client resource to use.

        Returns
        -------
        str
            A string html img element."""
        if not message_content:
            return ""
        custom_emojis = re.findall(r"<:\w*:\d*>", message_content)
        if custom_emojis:
            for custom_emoji in custom_emojis:
                start = message_content.find(custom_emoji)
                end = start + len(custom_emoji) + 1
                pre_emoji = message_content[:start]
                emoji_id = int(custom_emoji.split(":")[-1].split(">")[0])
                try:
                    demoji = client.metadata["cached-emoji"].get(emoji_id) or client.cache.get_emoji(emoji_id)
                    if not demoji:
                        continue
                    custom_emoji = f"<img class='lozad inline emoji' title=':{demoji.name}:' data-src='{demoji.url}' />"
                    post_emoji = message_content[end:]
                    message_content = f"{pre_emoji}{custom_emoji}{post_emoji}"
                except (AttributeError, IndexError) as err:
                    logger.error(err)
        return message_content

    def render_unicode_emoji(self, message: hikari.Message, message_content: str) -> str:
        """Helper class to wrap emoji in a span and class to style

        Parameters
        ----------
        message: hikari.Message
            The message to attempt a render from.
        message_content: str
            The message content for the transform.

        Returns
        -------
        str
            A string Unicode Emoji."""
        if not message_content:
            return ""
        emoji_list = emoji.emoji_lis(message_content)
        for unicode_emoji in emoji_list:
            start = message_content.find(unicode_emoji["emoji"])
            end = start + len(unicode_emoji["emoji"]) + 1
            pre_emoji = message_content[:start]
            emoji_name = emoji.demojize(unicode_emoji["emoji"])
            unicode_emoji = f"<span title='{emoji_name}' class='emoji'>{unicode_emoji['emoji']}</span>"
            post_emoji = message_content[end:]
            message_content = f"{pre_emoji}{unicode_emoji}{post_emoji}"
        return message_content


class APSchedulerInjector:
    """Simple dependency injector for tanjun to inject APScheduler."""

    __slots__ = [
        "_scheduler",
    ]

    def __init__(self):
        self._scheduler = AsyncIOScheduler()

    def __call__(self):
        return self._scheduler


class HttpxInjector:
    """Dependency injector for httpx with tanjun.

    Register dependency:
        client.set_type_dependency(httpx.AsyncClient, HttpxInjector())

    Then get in plugin:

    async def func_name(redis: httpx.AsyncClient = tanjun.injected(type=httpx.AsyncClient))
    """

    def __call__(self) -> httpx.AsyncClient:
        logger.info("New httpx Client Spawned")
        return httpx.AsyncClient()


class PostgresPoolDependency:
    """
    Dependency injector for aiopg with tanjun.

    Register dependency:
        client.set_type_dependency(aiopg.Pool, PostgresPoolDependency())

    Then get in plugin:

    async def func_name(redis: aiopg.Pool = tanjun.injected(type=aiopg.Pool))

    TODO:
        - Setup set_type_dep to bind self.
        - Method to yield a connection.
    """

    __slot__ = ("_postgres_pool",)

    def __init__(self) -> None:
        self._postgres_pool = None

    def __call__(self):
        return self._postgres_pool

    def load_into_client(self, client: tanjun.Client) -> None:
        """Load this dependency into a `tanjun.Client`."""
        client.add_client_callback(tanjun.ClientCallbackNames.STARTING, self.open).add_client_callback(
            tanjun.ClientCallbackNames.CLOSED, self.close
        )

    async def open(
        self,
        client: tanjun.Client = tanjun.injected(type=tanjun.Client),
        config: FullConfig = tanjun.injected(type=FullConfig),
    ) -> None:
        """Helper method to ensure `aiopg.Pool` is acquired."""
        if self._postgres_pool:
            raise RuntimeError("Session already running")

        self._postgres_pool = await aiopg.create_pool(
            f"dbname={config.database.name} user={config.database.username} password={config.database.password} port=5432 host={config.database.host}"  # noqa: E501
        )

        client.set_type_dependency(aiopg.Pool, self._postgres_pool)

    async def close(self, client: tanjun.Client = tanjun.injected(type=tanjun.Client)) -> None:
        """Helper method to ensure the acquired `aiopg.Pool` is closed properly."""
        if not self._postgres_pool:
            raise RuntimeError("Session not running")

        pool: aiopg.Pool = self._postgres_pool
        await pool.close()
        self._postgres_pool = None
        client.remove_type_dependency(aiopg.Pool)
