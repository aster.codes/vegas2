"""Provides multiple helper functions for working with users."""
from __future__ import annotations

import asyncio
import logging
from typing import Callable

import tanjun
import yuyo
from colorlog import ColoredFormatter
from hikari import GuildMessageCreateEvent
from hikari.api.event_manager import EventManager
from tanjun.abc import SlashContext

__all__ = [
    "collect_response",
    "ensure_guild_channel_validator",
    "is_int_validator",
    "yes_no_answer_validator",
    "get_vegas_logger",
]

TRUE_RESP = ["yes", "y", "ye", "t", "true"]
FALSE_RESP = ["no", "n", "false", "fa", "fal"]


def get_vegas_logger(log_file: str) -> logging.Logger:
    """Generate a custom Logger in the VEGAS2 format"""
    logger = logging.Logger(name=log_file)
    formatter = ColoredFormatter(
        "%(log_color)s%(levelname)s -- %(name)s -- %(reset)s %(blue)s%(message)s",
        datefmt=None,
        reset=True,
        log_colors={
            "DEBUG": "cyan",
            "INFO": "green",
            "WARNING": "yellow",
            "ERROR": "red",
            "CRITICAL": "red",
        },
    )

    handler = logging.StreamHandler()
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    logger.setLevel(logging.DEBUG)
    return logger


async def collect_response(  # pylint: disable=too-many-branches
    event_manager: EventManager,
    ctx: SlashContext | yuyo.ComponentContext,
    validator: list[str] | Callable | None = None,
    timeout: int = 60,
    timeout_msg: str = "Waited for 60 seconds... Timeout.",
) -> GuildMessageCreateEvent | None:
    """
    Helper function to collect a user response.

    Parameters
    ==========
    ctx: SlashContext
        The context to use.
    validator: list[str] | Callable | None = None
        A validator to check against. Validators can be:
            - list - A list of strings to match against.
            - Callable/Function - A function accepting (ctx, event) and returning bool.
            - None - Skips validation and returns True always.
    timeout int = 60
        The default wait_for timeout to use.
    timeout_msg: str = Waited for 60 seconds ... Timeout.
        The message to display if a timeout occurs
    """
    author = None
    if isinstance(ctx, tanjun.abc.SlashContext):
        author = ctx.member
    else:
        author = ctx.interaction.member

    def is_author(event: GuildMessageCreateEvent):
        if author.id == event.message.author.id:
            return True
        return False

    while True:
        try:
            event = await event_manager.wait_for(GuildMessageCreateEvent, predicate=is_author, timeout=timeout)
        except asyncio.TimeoutError:
            await ctx.edit_initial_response(timeout_msg)
            return None

        if event.content == "❌":
            await event.message.delete()
            return None

        if not validator:
            return event

        if not event.content:
            validation_message = await ctx.respond("Err: No content detected")
            await asyncio.sleep(3)
            await validation_message.delete()

        elif isinstance(validator, list):
            if any(valid_resp.lower() == event.content.lower() for valid_resp in validator):
                return event
            validation_message = await ctx.respond(f"That wasn't a valid response... Expected one these: {' - '.join(validator)}")
            await asyncio.sleep(3)
            await validation_message.delete()

        elif asyncio.iscoroutinefunction(validator):
            valid = await validator(ctx, event)
            if valid:
                return event
            validation_message = await ctx.respond("That doesn't look like a valid response... Try again?")
            await asyncio.sleep(3)
            await validation_message.delete()

        elif callable(validator):
            try:
                if validator(ctx, event):
                    return event
            except TypeError:  # So tanjun.conversions will work.
                if validator(event.content):
                    return event
            validation_message = await ctx.respond("Something about that doesn't look right... Try again?")
            await asyncio.sleep(3)
            await validation_message.delete()


def is_int_validator(_, event: GuildMessageCreateEvent) -> bool:
    """
    Used as a validator for `collect_response` to ensure the message content is an integer.
    """
    try:
        if event.content:
            int(event.content)
            return True
    except ValueError:
        pass
    return False


async def ensure_guild_channel_validator(ctx: tanjun.abc.Context, event) -> bool:
    """
    Used as a validator for `collect_response` to ensure a text channel in a guild exists.
    """
    guild = ctx.get_guild()
    if not guild:
        return False
    channels = guild.get_channels() if guild else []
    found_channel = None

    for channel_id in channels:
        channel = guild.get_channel(channel_id)
        if str(channel.id) in event.content or channel.name == event.content:
            found_channel = channel
            break

    if found_channel:
        return True

    await ctx.edit_initial_response(content=f"Channel `{event.content}` not found! Try again?")
    await event.message.delete()
    await asyncio.sleep(5)
    return False


def yes_no_answer_validator(_: tanjun.abc.SlashContext, event: GuildMessageCreateEvent):
    """Validator for collect_response that checks for yes/no answers."""
    if event.content:
        if event.content.lower() in TRUE_RESP:
            return True
        if event.content.lower() in FALSE_RESP:
            return True
    return False
