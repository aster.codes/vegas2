"""Basic implementation for Configs"""
from __future__ import annotations

import abc
import collections.abc as collections_abc
import logging
import os
import typing
from dataclasses import dataclass

import hikari
from hikari.api.config import CacheComponents

__all__: list[str] = ["FullConfig", "DatabaseConfig", "Tokens"]


logger = logging.getLogger()

ConfigT = typing.TypeVar("ConfigT", bound="Config")
DefaultT = typing.TypeVar("DefaultT")
ValueT = typing.TypeVar("ValueT")


class Config(abc.ABC):
    """Provides an implementation for the Basic Configuration."""

    __slots__ = ()

    @classmethod
    @abc.abstractmethod
    def from_environ(cls: type[ConfigT]) -> ConfigT:
        """Checks local environment variables and attempts to build the Config."""
        raise NotImplementedError


@dataclass(repr=False)
class DatabaseConfig(Config):
    """Provides an implementation for the Database Configuration."""

    host: str = "localhost"
    username: str = "postgres"
    password: str = "postgres"
    name: str = "postgres"
    port: int = 5432

    @classmethod
    def from_environ(cls) -> DatabaseConfig:
        return cls(
            host=os.environ.get("PG_HOST", "localhost"),
            username=os.environ.get("PG_USER", "postgres"),
            password=os.environ.get("PG_PASS", "postgres"),
            name=os.environ.get("PG_NAME", "postgres"),
            port=int(os.environ.get("PG_PORT", 5432)),
        )


@dataclass(repr=False)
class Tokens(Config):
    """Provides an implementation for the Token Configuration."""

    discord_bot: str
    marvel_public: str | None = None
    marvel_private: str | None = None
    merriam_webster: str | None = None
    sentry: str | None = None

    @classmethod
    def from_environ(cls) -> Tokens:
        token = os.environ.get("BOT_TOKEN")
        if not token:
            raise RuntimeError("Must provide discord bot token in environment variable BOT_TOKEN.")

        return cls(
            discord_bot=token,
            marvel_public=os.environ.get("MARVEL_PUB", None),
            marvel_private=os.environ.get("MARVEL_PRIV", None),
            merriam_webster=os.environ.get("MERWEB_KEY", None),
            sentry=os.environ.get("SENTRY_KEY", None),
        )


DEFAULT_CACHE = (
    CacheComponents.GUILDS
    | CacheComponents.GUILD_CHANNELS
    | CacheComponents.ROLES
    | CacheComponents.MEMBERS
    | CacheComponents.EMOJIS
    | CacheComponents.MESSAGES
    | CacheComponents.VOICE_STATES
)


@dataclass(repr=False)
class FullConfig(Config):  # pylint: disable=R0902
    """Provides a class and methods to handle Vegas2's Configuration."""

    database: DatabaseConfig
    tokens: Tokens
    cache: hikari.CacheComponents = DEFAULT_CACHE
    intents: hikari.Intents = hikari.Intents.ALL_UNPRIVILEGED
    log_level: int | str | dict[str, typing.Any] | None = logging.INFO
    mention_prefix: bool = True
    owner_only: bool = False
    prefixes: collections_abc.Set[str] = frozenset("m;")
    declare_global_commands: hikari.Snowflake | list[hikari.Snowflake] | bool = False

    @classmethod
    def from_environ(cls) -> FullConfig:
        log_level = os.environ.get("LOG_LEVEL", "INFO")
        if not isinstance(log_level, (str, int)):
            logger.error("Invalid Log Level provided from Environ")
            raise ValueError("Invalid log level found in config")

        log_level = log_level.upper()
        declare_global_commands = False

        if temp_declare_global_commands := os.environ.get("declare_global_commands", None):
            declare_global_commands = _cast_declarables(temp_declare_global_commands)
            logger.info(declare_global_commands)
        if not declare_global_commands:
            if temp_declare_global_commands := os.environ.get("set_global_commands", None):
                declare_global_commands = _cast_declarables(temp_declare_global_commands)
        if not declare_global_commands:
            logger.warning("As a fallback, setting `declare_global_commands` to True. Commands will be sent to all Guilds.")
            declare_global_commands = True
        return cls(
            database=DatabaseConfig.from_environ(),
            tokens=Tokens.from_environ(),
            cache=DEFAULT_CACHE,
            intents=typing.cast(
                hikari.Intents,
                os.environ.get("DISCORD_INTENTS", hikari.Intents.ALL),
            ),
            log_level=log_level,
            mention_prefix=bool(os.environ.get("MENTION_PREFIX", True)),
            owner_only=bool(os.environ.get("OWNER_ONLY", False)),
            prefixes=set(os.environ.get("PREFIXES", "m;").split(",")),
            declare_global_commands=declare_global_commands,
        )


def _cast_declarables(
    global_declares: str,
) -> hikari.Snowflake | list[hikari.Snowflake] | bool:
    declare_global_commands = False
    if global_declares in [
        "true",
        "True",
        "false",
        "False",
    ]:
        declare_global_commands = bool(global_declares.title())
    elif isinstance(global_declares, str) and "," in global_declares:
        declare_global_commands = list()
        for guild_id in global_declares.split(","):
            try:
                declare_global_commands.append(hikari.Snowflake(guild_id))
            except Exception:
                logger.warning(f"Failed to convert part of declare global commands Guild List {guild_id}")
    else:
        try:
            declare_global_commands = hikari.Snowflake(global_declares)
        except Exception:
            logger.warning(f"Failed to convert declare global commands Guild {global_declares}")
    return declare_global_commands
