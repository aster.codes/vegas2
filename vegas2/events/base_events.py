from __future__ import annotations

import attr
import hikari
from hikari.events.base_events import Event
from hikari.traits import RESTAware

__all__ = [
    "VEGAS2Event",
]


@attr.define()
class VEGAS2Event(Event):
    """Base event for Vegas2 events."""

    app: RESTAware = attr.field()
    """The application to attach the event to."""

    author_id: hikari.Snowflake = attr.field()
    """The id of the Member calling the command which caused this exception."""
    guild_id: hikari.Snowflake = attr.field()
    """The Guild ID that this exception was raised from."""
    channel_id: hikari.Snowflake = attr.field()
    """The Channel ID that this exception was raised from."""
