"""AutoRoleEvent's are used to signify changes in the AutoRole Model to provide decoupling.
These custom events are dispatched and consumed through the `hikari.api.EventManager` and represent
some form of state change for the AutoRoles."""
from __future__ import annotations

import typing

import attr
import hikari
from hikari.snowflakes import Snowflake

from .base_events import VEGAS2Event

if typing.TYPE_CHECKING:
    from hikari.traits import RESTAware

__all__ = [
    "AutoRoleEvent",
    "AutoRoleEntryUpdated",
    "AutoRoleEntryRemoved",
    "AutoRoleEntryDatabaseRemoved",
    "AutoRoleEntryDatabaseUpdated",
]


@attr.define()
class AutoRoleEvent(VEGAS2Event):
    """Base for an AutoRole being updated as an Event."""

    app: RESTAware = attr.ib()

    emoji: str | None = attr.field(default=None)
    """Either hikari.UnicodeEmoji.emoji or hikari.KnownCustomEmoji.name for this event."""
    custom_emoji_id: hikari.Snowflake | None = attr.field(default=None)
    """The hikari.KnownCustomEmoji.id for this event."""
    role_id: Snowflake | None = attr.field(default=None)
    """The hikari.Role.id for this event."""


@attr.define()
class AutoRoleEntryDatabaseUpdated(AutoRoleEvent):
    """Triggered when any AutoRoleEntry is added/updated and saved to persistent storage.

    This event signifies that the data needs to be added/updated in the local cache storage."""

    ...


@attr.define()
class AutoRoleEntryDatabaseRemoved(AutoRoleEvent):
    """Triggered when any AutoRoleEntry is removed from persistent storage.

    This event signifies that the data needs to be removed from the local cache storage."""

    ...


@attr.define()
class AutoRoleEntryUpdated(AutoRoleEvent):
    """Triggered when any AutoRoleEntry is updated/added in the VEAGS2 bot.

    This event signifies that the data needs to be saved to persistent storage."""

    ...


@attr.define()
class AutoRoleEntryRemoved(AutoRoleEvent):
    """Triggered when an AutoRole Model has an entry Removed."""

    ...
