"""The Events module contains all custom events that the VEGAS2 bot launches and consumes."""
__all__ = [
    "base_events",
    "VEGAS2Event",
    "autoroles",
    "AutoRoleEntryDatabaseRemoved",
    "AutoRoleEntryDatabaseUpdated",
    "AutoRoleEntryUpdated",
    "AutoRoleEntryRemoved",
    "guild_settings",
    "GuildSettingDatabaseUpdated",
    "GuildSettingDatabaseRemoved",
    "GuildSettingUpdated",
    "GuildSettingRemoved",
]

from . import autoroles, base_events, guild_settings
from .autoroles import (
    AutoRoleEntryDatabaseRemoved,
    AutoRoleEntryDatabaseUpdated,
    AutoRoleEntryRemoved,
    AutoRoleEntryUpdated,
)
from .base_events import VEGAS2Event
from .guild_settings import (
    GuildSettingDatabaseRemoved,
    GuildSettingDatabaseUpdated,
    GuildSettingRemoved,
    GuildSettingUpdated,
)
