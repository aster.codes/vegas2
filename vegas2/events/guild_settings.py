from __future__ import annotations

import typing

import attr
import hikari
from hikari.snowflakes import Snowflake

from .base_events import VEGAS2Event

if typing.TYPE_CHECKING:
    from hikari.traits import RESTAware  # pylint: disable=F401

__all__ = [
    "GuildSetting",
    "GuildSettingDatabaseUpdated",
    "GuildSettingDatabaseRemoved",
    "GuildSettingUpdated",
    "GuildSettingRemoved",
]


@attr.define()
class GuildSetting(VEGAS2Event):
    guild_id: Snowflake = attr.ib()
    logging_enabled: bool | None = attr.ib()
    logging_channel: int | None = attr.ib()
    vc_logging_channel: int | None = attr.ib()
    mod_logging_channel: int | None = attr.ib()
    auto_delete_enabled: bool | None = attr.ib()
    auto_delete_channels_ids: set[hikari.Snowflake] | None = attr.ib()


@attr.define()
class GuildSettingDatabaseUpdated(GuildSetting):
    ...


@attr.define()
class GuildSettingDatabaseRemoved(GuildSetting):
    ...


@attr.define()
class GuildSettingUpdated(GuildSetting):
    ...


@attr.define()
class GuildSettingRemoved(GuildSetting):
    ...
