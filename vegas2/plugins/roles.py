"""Provides 3 Slash Commands.

/autoroles list
    Provides an embed of all autoroles
/autoroles add
    Adds a new hikari.Role/hikari.Emoji combo to the AutoRolesList.

Provides 1 Listener

add_role_from_role_name_or_emoji
    Listens in #roles for user messages.
    Gives roles from AutoRoleList
"""
from __future__ import annotations

import re

import hikari
import tanjun
from emoji import emoji_count
from tanjun.abc import SlashContext
from tanjun.checks import with_owner_check

from ..checks.role_checks import with_any_role_check
from ..events import AutoRoleEntryRemoved, AutoRoleEntryUpdated
from ..models.autoroles import AutoRoleList
from ..util.helpers import get_vegas_logger

__all__ = [
    "add_role_from_role_name_or_emoji",
    "watch_channel_embed_for_emoji",
    "autoroles_list",
    "autoroles_add",
    "autoroles_remove",
    "loader",
]

CUSTOM_EMOJI_RE = r"<:\w+:(\d+)>"

logger = get_vegas_logger(__file__)

component = tanjun.Component()

autoroles = component.with_slash_command(tanjun.slash_command_group("autoroles", "Work with Autoroles"))  # pylint: disable=C0103


@autoroles.with_command
@with_any_role_check(
    [
        "Mods",
    ]
)
@tanjun.as_slash_command("list", "List all the current set of autoroles")
async def list_command(
    ctx: SlashContext,
    auto_roles: AutoRoleList = tanjun.injected(type=AutoRoleList),
    bot: hikari.GatewayBot = tanjun.injected(type=hikari.GatewayBot),
):
    await autoroles_list(ctx, auto_roles, bot)


async def autoroles_list(
    ctx: SlashContext,
    auto_roles: AutoRoleList = tanjun.injected(type=AutoRoleList),
    bot: hikari.GatewayBot = tanjun.injected(type=hikari.GatewayBot),
):
    """Responds with an embed of all AutoRoles for the calling Guild.

    Command: ```/autoroles list```
    """
    if not ctx.guild_id:
        return
    await auto_roles.load_for_guild(ctx.guild_id)
    guild = ctx.get_guild()
    if not guild:
        guild = await ctx.rest.fetch_guild(ctx.guild_id)
    desc = (
        f"Below is a list of Roles you can assign to yourself for {guild.name}!"
        " **Just find a role below that you want, you can:**\n\n"
        "- Send the emote next to the role\n"
        "- React to this message with the emote next to the role\n"
        "- Send the role name (case-insensitive)\n\n"
        "If you have any questions feel free to ask in #server-help!"
    )
    auto_roles_embeds = auto_roles.generate_embeds(ctx)
    if not auto_roles_embeds:
        await ctx.respond(f"No autoroles for {guild.name}")
        return

    send_embeds = []

    for embed in auto_roles_embeds:
        embed.set_thumbnail(guild.icon_url)
        embed_dict, *_ = bot.entity_factory.serialize_embed(embed)
        embed_dict["description"] = desc
        send_embeds.append(bot.entity_factory.deserialize_embed(embed_dict))
    await ctx.respond(embeds=send_embeds)


@autoroles.with_command
@with_owner_check
@tanjun.with_role_slash_option("add_role", "Role to add.")
@tanjun.with_str_slash_option("emoji", "The emote to associate with the role.")
@tanjun.as_slash_command("add", "Add to the current set of autoroles.")
async def add_command(
    ctx: SlashContext,
    emoji: str,
    add_role: hikari.Role,
    auto_roles: AutoRoleList = tanjun.injected(type=AutoRoleList),
    bot: hikari.GatewayBot = tanjun.injected(type=hikari.GatewayBot),
):
    await autoroles_add(ctx, emoji, add_role, auto_roles, bot)


async def autoroles_add(
    ctx: SlashContext,
    emoji: str,
    add_role: hikari.Role,
    auto_roles: AutoRoleList,
    bot: hikari.GatewayBot,
):
    """Add a role/emoji to the AutoRolesList for the calling Guild.

    Parameters
    ----------
    emoji: str
        Either a Unicode Emoji, or Custom Emoji to add.
    add_role: hikari.Role
        The role to add."""
    if not ctx.guild_id:
        return
    await auto_roles.load_for_guild(ctx.guild_id)
    guild = ctx.get_guild()
    if not guild:
        guild = await ctx.rest.fetch_guild(ctx.guild_id)

    try:
        discord_emoji: hikari.CustomEmoji | hikari.UnicodeEmoji | hikari.Emoji = hikari.Emoji.parse(emoji)
    except (ValueError, AttributeError):
        logger.info(f"{guild.name} passed bad emoji {emoji}.")
        await ctx.respond("That does not appear to be an emoji...")
        return

    added = auto_roles.append(discord_emoji, add_role)
    if added:
        bot.dispatch(
            AutoRoleEntryUpdated(
                app=bot,
                author_id=ctx.author.id,
                guild_id=ctx.guild_id,
                channel_id=ctx.channel_id,
                emoji=discord_emoji.name if isinstance(discord_emoji, hikari.CustomEmoji) else str(discord_emoji),
                custom_emoji_id=discord_emoji.id if isinstance(discord_emoji, hikari.CustomEmoji) else None,
                role_id=add_role.id,
            )
        )
    # await auto_roles.save_for_guild()
    await ctx.respond(content=f"Role {add_role.name} Added as {emoji}!")


@autoroles.with_command
@with_owner_check
@tanjun.with_str_slash_option("emoji", "The emote to associate with the role.", default=None)
@tanjun.with_role_slash_option("remove_role", "Role to remove.", default=None)
@tanjun.as_slash_command("remove", "Remove from the current set of autoroles")
async def remove_command(
    ctx: SlashContext,
    emoji: str | None = None,
    remove_role: hikari.Role | None = None,
    auto_roles: AutoRoleList = tanjun.injected(type=AutoRoleList),
    bot: hikari.GatewayBot = tanjun.inject(type=hikari.GatewayBot),
):
    await autoroles_remove(bot=bot, ctx=ctx, emoji=emoji, remove_role=remove_role, auto_roles=auto_roles)


async def autoroles_remove(
    auto_roles: AutoRoleList,
    bot: hikari.GatewayBot,
    ctx: SlashContext,
    emoji: str | None = None,
    remove_role: hikari.Role | None = None,
):
    """Remove a role/emoji from the AutoRolesList.

    Parameters
    ----------
    emoji: str
        Either a Unicode Emoji, or Custom Emoji to remove.
    remove_role: hikari.Role
        The role to remove."""
    if not ctx.guild_id:
        return
    await auto_roles.load_for_guild(ctx.guild_id)
    try:
        discord_emoji = hikari.Emoji.parse(emoji)
    except ValueError:
        logger.debug(f"{ctx.guild_id} passed bad emoji {emoji}.")
        return
    except AttributeError:
        if emoji:
            await ctx.respond("That does not appear to be an emoji...")
        discord_emoji = None

    provided_arg = remove_role.mention if remove_role else emoji
    output = f"No entry found for {provided_arg}"
    removed = auto_roles.remove(emoji=emoji, role=remove_role)
    if removed:
        bot.dispatch(
            AutoRoleEntryRemoved(
                app=bot,
                guild_id=ctx.guild_id,
                channel_id=ctx.channel_id,
                author_id=ctx.author.id,
                emoji=discord_emoji.name if isinstance(discord_emoji, hikari.CustomEmoji) else str(discord_emoji),
                custom_emoji_id=discord_emoji.id if isinstance(discord_emoji, hikari.CustomEmoji) else None,
                role_id=remove_role.id if remove_role else None,
            )
        )
        output = f"Entry {provided_arg} removed"
    await ctx.respond(content=output)


@component.with_listener(hikari.GuildReactionAddEvent)
async def watch_channel_embed_for_emoji(
    event: hikari.GuildReactionAddEvent,
    auto_roles: AutoRoleList = tanjun.injected(type=AutoRoleList),
    cache: hikari.api.Cache = tanjun.inject(type=hikari.api.Cache),
):
    """Automatically gives AutoRoles for any Guild based on Reactions.

    Any channel beginning with #roles will be watched. If members React to a pinned embed
    in that channel, the bot will give the associated AutoRole."""
    logger.info("*" * 80)
    channel = cache.get_guild_channel(event.channel_id)
    if not channel:
        logger.debug("#roles channel lookup for reaction")
        channel = await event.app.rest.fetch_channel(event.channel_id)
    if not channel.name or not channel.name.startswith("roles"):
        return

    message = cache.get_message(event.message_id)
    if not message:
        logger.debug("#roles message lookup for reaction")
        message = await event.app.rest.fetch_message(channel=event.channel_id, message=event.message_id)

    await auto_roles.load_for_guild(event.guild_id)
    if (
        not message.is_pinned
        or not message.embeds[0].title
        or not message.embeds[0].title.lower().startswith("notification roles")
        or len(message.embeds) == 0
        or not auto_roles.entries
    ):
        return

    action = None
    member_roles = event.member.get_roles()

    try:
        for entry in auto_roles.entries:
            if event.is_for_emoji(entry.emoji):
                action = "added"
                if entry.role in member_roles:
                    action = "removed"
                    await event.member.remove_role(entry.role, reason=f"{entry.role.name} removed via Autoroles.")
                else:
                    await event.member.add_role(entry.role, reason=f"{entry.role.name} changed via Autoroles.")
                confirm = await message.respond(
                    content=f"I {action} the role {entry.role.name} to {event.member.username}!",
                    reply=True,
                    role_mentions=False,
                    mentions_everyone=False,
                )
                await confirm.edit(content=f"I {action} the role {entry.role.mention} to {event.member.mention}!")
                break
        if not action:
            await message.respond(f"Sorry, {event.member.mention} I couldn't find a role for that emote!")
        await message.remove_all_reactions()
    except hikari.ForbiddenError:
        await channel.send("Sorry! I don't have permissions to add roles/that role!")


@component.with_listener(hikari.GuildMessageCreateEvent)
async def add_role_from_role_name_or_emoji(
    event: hikari.GuildMessageCreateEvent,
    auto_roles: AutoRoleList = tanjun.injected(type=AutoRoleList),
    cache: hikari.api.Cache = tanjun.inject(type=hikari.api.Cache),
):
    """Automatically gives AutoRoles for any Guild based on sent Emoji/Role name.

    Any channel beginning with #roles will be watched. If members sends a message
    in that channel that contains an associated Emoji or AutoRole Role name,
    the bot will give the associated AutoRole."""
    if not event.content or not event.member or not event.guild_id or event.is_bot:
        return
    channel = event.get_channel()
    if not channel:
        logger.info("#roles channel lookup for reaction")
        channel = await event.app.rest.fetch_channel(event.channel_id)
    if not channel.name or "roles" not in channel.name:
        return
    await auto_roles.load_for_guild(event.guild_id)
    if not auto_roles.entries:
        return

    custom_emoji_search = re.search(CUSTOM_EMOJI_RE, event.content)
    if custom_emoji_search:
        custom_emoji_id = custom_emoji_search.groups(0)[0]
        content = cache.get_emoji(int(custom_emoji_id))
        if not content:
            guild = event.get_guild()
            if not guild:
                guild = await event.app.rest.fetch_guild(event.guild_id)
            content = await guild.fetch_emoji(int(custom_emoji_id))
    else:
        content = event.content.lower()

    try:
        for entry in auto_roles.entries:
            if content == entry.role.name.lower() or str(content) == str(entry.emoji):
                action = "added"
                if entry.role in event.member.get_roles():
                    action = "removed"
                    await event.member.remove_role(
                        entry.role,
                        reason=f"{event.member.mention}{entry.role.name} removed via Autoroles.",
                    )
                else:
                    await event.member.add_role(entry.role, reason="{entry.role.name} changed via Autoroles.")
                confirm = await event.message.respond(
                    content=f"{event.member.mention} I {action} the role {entry.role.name}!",
                    reply=True,
                    user_mentions=True,
                )
                await confirm.edit(
                    content=f"{event.member.mention} I {action} the role {entry.role.mention}!",
                    user_mentions=True,
                )
                break
    except hikari.ForbiddenError:
        await event.message.respond("Sorry! I don't have permissions to add roles/that role!")


async def _is_unicode_or_custom_emoji(ctx: tanjun.abc.Context, to_check: str) -> bool:
    """Helper function to check if a string is an unicode emoji or a custom emoji."""
    if not emoji_count(to_check):
        try:
            custom_emoji_id = to_check.split(":")[-1][:-1]
            custom_emote = await tanjun.conversion.EmojiConverter().convert(ctx, custom_emoji_id)
            if not custom_emote:
                return False
        except ValueError:
            return False
    return True


loader = component.make_loader()
