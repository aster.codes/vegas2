"""Provides automatic ticketing functions."""
from __future__ import annotations

import asyncio
import logging
from functools import partial

import hikari
import tanjun
import yuyo
from hikari import PermissionOverwrite, PermissionOverwriteType, Permissions
from hikari.api import Cache
from hikari.channels import ChannelType, GuildChannel
from hikari.events.lifetime_events import StartedEvent
from hikari.interactions.component_interactions import ComponentInteraction
from tanjun.abc import SlashContext
from tanjun.checks import with_owner_check
from tanjun.conversion import parse_user_id
from yuyo.components import ComponentContext, as_single_executor

from ..checks.role_checks import with_any_role_check
from ..models.tickets import Ticket
from ..util.constants.tickets import (
    DEFAULT_TICKET_CATEGORY_NAME,
    DEFAULT_TICKET_CHAN_NAME,
)
from ..util.helpers import collect_response, yes_no_answer_validator

__all__ = [
    "load",
    "unload",
    "startup_tickets",
    "create_new_ticket_button",
    "add_member_to_ticket",
    "add_member_ticket_button",
    "remove_member_from_ticket",
    "remove_member_ticket_button",
    "close_ticket_button",
    "delete_ticket",
    "timeout_ticket",
    "loaders",
]

DEFAULT_TIMEOUT = 60

logger = logging.getLogger(__name__)

component = tanjun.Component()


as_single_executor("vegas2_remove_member_ticket")
async def remove_member_ticket_button(
    ctx: ComponentContext,
    cache: Cache = tanjun.injected(type=Cache),
    event_manager: hikari.api.EventManager = tanjun.inject(type=hikari.api.EventManager),
):
    """A button handler to remove a member from a ticket.

    This button appears in every ticket and will only work if the Member has @Mods or an Admin permission.

    The bot will prompt the clicker to provide a Member ID. If that member has permissions in the channel, they will be removed."""
    if not is_mod(ctx):
        return
    await ctx.respond("Please provide a Member (via ID) to remove.")
    try:
        response = await collect_response(event_manager, ctx, parse_user_id)
    except ValueError:
        return
    member_id = response.content
    member = cache.get_member(ctx.interaction.guild_id, member_id)
    if not member:
        member = await ctx.interaction.app.rest.fetch_member(ctx.interaction.guild_id, member_id)
    if not member:
        await ctx.respond("Can't find member {member_id}")
        return
    ticket = Ticket(ctx)
    await ticket.remove_member_from_ticket(ctx, member)


@as_single_executor("vegas2_add_member_ticket")
async def add_member_ticket_button(
    ctx: ComponentContext,
    cache: Cache = tanjun.injected(type=Cache),
    event_manager: hikari.api.EventManager = tanjun.inject(type=hikari.api.EventManager)
):
    """A button handler to add a member from a ticket.

    This button appears in every ticket and will only work if the Member has @Mods or an Admin permission.

    The bot will prompt the clicker to provide a Member ID. That member will be give permissions for the channel."""
    if not is_mod(ctx):
        return
    await ctx.respond("Please provide a Member (via ID) to add.")
    try:
        response = await collect_response(event_manager, ctx, parse_user_id)
    except ValueError:
        return
    member_id = response.content
    member = cache.get_member(ctx.interaction.guild_id, member_id)
    if not member:
        member = await ctx.interaction.app.rest.fetch_member(ctx.interaction.guild_id, member_id)
    if not member:
        await ctx.respond("Can't find member {member_id}")
        return
    ticket = Ticket(ctx)
    await ticket.add_member(ctx, member)


@as_single_executor("vegas2_timeout_mode_ticket")
async def timeout_ticket(
    ctx: ComponentContext,
    cache: Cache = tanjun.injected(type=Cache)
):
    """A button handler to Timing out/Locking a Ticket.

    This button appears in every ticket and will only work if the Member has @Mods or an Admin permission.

    Once clicked, all members with permissions in this ticket will be given a @Muted role. This works best
    if the @Muted role has no/restricted server permissions, locking the member to the ticket channel only."""
    if not ctx.interaction.member or not ctx.interaction.guild_id:
        return
    if not is_mod(ctx):
        return
    ctx._ephemeral_default = True
    await ctx.respond("🔐 Locking user(s) to this channel... 🔐")

    channel = ctx.interaction.get_channel()
    if not channel:
        channel = await ctx.interaction.fetch_channel()
    overwrites = channel.permission_overwrites
    embed = (
        hikari.Embed()
        .set_author(name=f"Locking Members to {channel.name}", icon=hikari.Emoji.parse("🔐"))
        .set_thumbnail(
            "https://cdn.discordapp.com/attachments/733789542884048906/900079323279663175/85d744c5310511ecb705f23c91500735.png"
        )
    )
    for overwrite_id in overwrites:
        if overwrites[overwrite_id].type == PermissionOverwriteType.MEMBER:
            member = cache.get_member(ctx.interaction.guild_id, overwrite_id)
            if not member:
                member = await ctx.interaction.app.rest.fetch_member(ctx.interaction.guild_id, overwrite_id)
            if not member:
                await ctx.respond(f"Can't find member {overwrite_id}")
            cached_roles = cache.get_roles_view_for_guild(ctx.interaction.guild_id)
            muted_role: hikari.Role | None = None
            for role_id in cached_roles:
                if cached_roles[role_id].name == "Muted":
                    muted_role = cached_roles[role_id]
                    break
            if not muted_role:
                guild_roles = ctx.interaction.app.rest.fetch_roles(ctx.interaction.guild_id)
                for role in guild_roles:
                    if role.name == "Muted":
                        muted_role = role
                        break
            if not muted_role:
                await ctx.respond(
                    "Please create a `Muted` role and remove Send Message " "permissions for all channels for this role."
                )
                return
            await member.add_role(muted_role)
            embed.add_field(
                name=f"{member.username} - {member.id}",
                value=f"*Member Mention:* {member.mention}",
            )
    await ctx.respond(embed)


@as_single_executor("vegas2_delete_ticket")
async def delete_ticket(
    ctx: ComponentContext,
):
    """A button handler to delete unneeded tickes.

    This button appears in every ticket and will only work if the Member has @Mods or an Admin permission."""
    if not is_mod(ctx):
        return
    await ctx.respond("Bye bye! 👋")
    channel = ctx.interaction.get_channel()
    if not channel:
        channel = await ctx.interaction.fetch_channel()
    await channel.delete()


@as_single_executor("vegas2_open_old_ticket")
async def open_old_ticket(
    ctx: ComponentContext,
):
    """A button handler to reopen old tickes.

    This button appears in every ticket and will only work if the Member has @Mods or an Admin permission."""
    if not is_mod(ctx):
        return
    ticket = Ticket(ctx)
    await ticket.open_old_ticket(ctx)


@as_single_executor("vegas2_close_ticket")
async def close_ticket_button(
    ctx: ComponentContext,
):
    """Event listener to check for closing ticket requests."""
    ticket = Ticket(ctx)
    await ticket.close_user_ticket(ctx)


@as_single_executor("vegas2_new_ticket")
async def create_new_ticket_button(
    ctx: ComponentContext,
    bot: hikari.GatewayBotAware = tanjun.injected(type=hikari.GatewayBotAware),
    cache: Cache = tanjun.injected(type=Cache)
):
    """Automatically creates new Ticket when a tickets button is clicked.

    This button will create a new hikari.GuildTextChannel in the same Category as #tickets.

    Permissions on this channel will be sync'd with the Category, but the Member creating the ticket
    will be given additional permissions to view/message/attach images."""
    if not isinstance(ctx.interaction, ComponentInteraction) or not ctx.interaction.member or not ctx.interaction.guild_id:
        return

    ticket = Ticket(ctx)
    await ticket.open_user_ticket(ctx, ctx.interaction.member, cache, bot)


tickets = component.with_slash_command(tanjun.slash_command_group("tickets", "Work with tickets [Staff Only]"))


@tickets.with_command
@with_any_role_check("Mods")
@tanjun.with_member_slash_option("member", "The member to add.")
@tanjun.as_slash_command("remove_member", "Adds a provided member tot the current ticket channel")
async def remove_member_command(ctx: tanjun.abc.SlashContext, member: hikari.Member):
    await remove_member_from_ticket(ctx, member)


async def remove_member_from_ticket(ctx: tanjun.abc.SlashContext, member: hikari.Member):
    """Removes the provided Member to the Ticket you are viewing.

    Only works in channels created by #tickets. Only works for members with @Mods or Administrator permission.

    Parameters
    ----------
    member: hikari.Member
        The member to remove from the Ticket."""
    ticket = Ticket(ctx)
    await ticket.remove_member(ctx, member)


@tickets.with_command
@with_any_role_check("Mods")
@tanjun.with_member_slash_option("member", "The member to add.")
@tanjun.as_slash_command("add_member", "Adds a provided member tot the current ticket channel")
async def add_member_command(ctx: tanjun.abc.SlashContext, member: hikari.Member):
    await add_member_to_ticket(ctx, member)


async def add_member_to_ticket(ctx: tanjun.abc.SlashContext, member: hikari.Member):
    """Adds the provided Member to the Ticket you are viewing.

    Only works in channels created by #tickets. Only works for members with @Mods or Administrator permission.

    Parameters
    ----------
    member: hikari.Member
        The member to add to the Ticket."""
    ticket = Ticket(ctx)
    await ticket.add_member(ctx, member)


@tickets.with_command
@with_owner_check
@tanjun.as_slash_command("startup", "Ensures your guild is configured for tickets!")
async def startup_tickets_command(ctx: SlashContext, bot: hikari.GatewayBotAware = tanjun.injected(type=hikari.GatewayBotAware)):
    await startup_tickets(ctx, bot)


async def startup_tickets(
    ctx: SlashContext,
    event_manager: hikari.api.EventManager = tanjun.inject(type=hikari.api.EventManager),
    bot: hikari.GatewayBotAware = tanjun.injected(type=hikari.GatewayBotAware),
    cache: hikari.api.Cache = tanjun.injected(type=hikari.api.Cache)
):
    """The bot will scan your Guild and ask questions to setup/customize Tickets.

    1) Prompts to create a `Ask Help!` category if none is found. This is optional, the category and name are not required.

    2) Prompts to create a #tickets channel if none is found. This channel will be created with Read Only permissions.
    The bot will then send an Embed explaining how the channel works and attach a Button to open new Tickets.

    Notes
    -----
    - If a Ticket Category is not created, the #tickets channel will be created in the first position (above all other channels).
    - New Member created Tickets will be created in the same category as #tickets. If you move #tickets, new Members Tickets
    will be created in the new category.
    - If no category is provided, new Member Tickets will be created in position 1 (above all other channels).
    - The Category is primarily used to enforce Ticket permissions. If you would like non-Administrator members to see
    Member Tickets, create a Category where those members have View/Send permissions and place #tickets here. All new
    Member Tickets will inherit this permission.
    """
    if not ctx.guild_id or not ctx.member:
        return
    ticket_channel: hikari.GuildTextChannel | hikari.GuildChannel | None = None
    tickets_category = None

    await ctx.respond("Let met see if your guild is setup!")

    async with ctx.rest.trigger_typing(ctx.channel_id):
        guild_channels = None
        if ctx.cache:
            guild_channels = ctx.cache.get_guild_channels_view_for_guild(ctx.guild_id)
        if not guild_channels:
            guild_channels = await ctx.rest.fetch_guild_channels(ctx.guild_id)

        for channel_id in guild_channels:
            if isinstance(channel_id, GuildChannel):
                channel = channel_id
            else:
                channel = guild_channels[channel_id]
            if (
                channel.name
                and channel.name.lower() == DEFAULT_TICKET_CATEGORY_NAME.lower()
                and channel.type == ChannelType.GUILD_CATEGORY
            ):
                tickets_category = channel
            if (
                channel.name
                and channel.name.lower() == DEFAULT_TICKET_CHAN_NAME.lower()
                and channel.type == ChannelType.GUILD_TEXT
            ):
                ticket_channel = channel

    if not tickets_category:
        await ctx.edit_initial_response(
            f"Tickets Category Not Found [Name: {DEFAULT_TICKET_CHAN_NAME}"
            "\nWould you like to setup now? (Optional, just helps with Perms management)"
        )
        message_event = await collect_response(event_manager, ctx, ["yes", "no",], timeout=DEFAULT_TIMEOUT)
        if message_event:
            async with ctx.rest.trigger_typing(ctx.channel_id):
                await ctx.edit_initial_response("Setting up Tickets Category and locking!  🔒")
                tickets_category = await ctx.rest.create_guild_category(
                    ctx.guild_id,
                    DEFAULT_TICKET_CATEGORY_NAME,
                    permission_overwrites=[
                        PermissionOverwrite(id=ctx.guild_id, type=0, deny=Permissions.VIEW_CHANNEL),
                    ],
                )
        else:
            await ctx.edit_initial_response("Okay! Let's see if #tickets is setup...")
            await asyncio.sleep(3)

    if not ticket_channel:
        await ctx.edit_initial_response("#ticket Channel not found... Setup now?")

        message_event = await collect_response(event_manager, ctx, ["yes", "no",], timeout=DEFAULT_TIMEOUT)

        if message_event:
            async with ctx.rest.trigger_typing(ctx.channel_id):
                await ctx.edit_initial_response("Okay let's get started!")
                ticket_channel = await ctx.rest.create_guild_text_channel(
                    ctx.guild_id,
                    name=DEFAULT_TICKET_CHAN_NAME,
                    category=tickets_category or hikari.UNDEFINED,
                    permission_overwrites=[
                        PermissionOverwrite(id=ctx.guild_id, type=0, deny=Permissions.SEND_MESSAGES),
                    ],
                )
    if ticket_channel:
        last_message = await ticket_channel.fetch_history().limit(1)
        if not last_message:
            await ctx.edit_initial_response("Setting up embed and button.  🔘")
            embed, row = await Ticket.start_ticket_embed_components(ctx, bot, cache)
            await ticket_channel.send(embed=embed, component=row)

    await ctx.edit_initial_response("Setup Complete!")


def is_mod(ctx: tanjun.abc.SlashContext | yuyo.ComponentContext):
    if isinstance(ctx, tanjun.abc.SlashContext):
        member = ctx.member
    else:
        member = ctx.interaction.member

    if not member:
        return False

    member_roles = member.get_roles()
    if not member_roles:
        return False

    is_mod = any("mods" == role.name.lower() for role in member_roles)
    is_admin = Permissions.ADMINISTRATOR in member.permissions
    return is_mod or is_admin


# loaders = component.make_loader()

@tanjun.as_loader
def load(client: tanjun.abc.Client) -> None:
    """Load this module's components into a bot."""
    client.add_component(component.copy())
    component_client = client.get_type_dependency(yuyo.ComponentClient)

    try:
        component_client.register_executor(create_new_ticket_button, timeout=None)

    except ValueError:
        pass  # They have their own implementation set.


@tanjun.as_unloader
def unload(client: tanjun.abc.Client) -> None:
    """Unload this module's components from a bot."""
    client.remove_component(component.copy())

    component_client = client.injector.get_type_dependency(yuyo.ComponentClient)
    assert component_client
    # TODO: only remove if get_executor_fur_id(DELETE_CUSTOM_ID) is on_delete_button
    component_client.deregister_executor(create_new_ticket_button)
