"""Provides commands that allow VEGAS2 to manage messages in your server.

This can be useful for things like Server Rules, Channel Pins or any sort
of constant/static information.

Notes
-----

**Website Integration**

This plugin offers integration with the [VEGAS2 website]
(https://vegas2.patchwork.systems/admin). Due to this you can also
add ManagedMessages through the website then publish those messages
to Discord with these commands. Likewise the scan commands can load
ManagedMessages into the website for easy GUI editing without bot commands."""
from __future__ import annotations

import json
import logging

import hikari
import tanjun
from hikari import traits
from tanjun.abc import SlashContext

from ..checks.role_checks import with_any_role_check

__all__ = [
    "messages_grp",
    "send_message",
    "loaders",
]

logging.getLogger()

component = tanjun.Component()

messages_grp = component.with_slash_command(tanjun.slash_command_group("messages", "Work with Bot Messages."))
"""The CommandGroup for Messages."""


@messages_grp.with_command
@with_any_role_check(
    [
        "Admin",
    ]
)
@tanjun.with_str_slash_option("content", "The content to send to the channel.", default=hikari.UNDEFINED)
@tanjun.with_str_slash_option("raw_embed", "A json embed to send.", default=hikari.UNDEFINED)
@tanjun.with_str_slash_option("reply_id", "The Message ID to reply to.", default=hikari.UNDEFINED)
@tanjun.with_bool_slash_option("pin", "Should this message be pinned?", default=False)
@tanjun.with_channel_slash_option("send_channel", "The channel to send the message to.")
@tanjun.as_slash_command("send-message", "Send a message with the Bot.")
async def send_message_command(
    ctx: SlashContext,
    send_channel: hikari.InteractionChannel,
    content: hikari.UndefinedOr[str],
    raw_embed: hikari.UndefinedOr[str],
    reply_id: hikari.UndefinedOr[str],
    pin: bool = False,
    entity_factory: traits.EntityFactoryAware = tanjun.injected(type=traits.EntityFactoryAware),
):
    await send_message(
        entity_factory=entity_factory,
        ctx=ctx,
        send_channel=send_channel,
        content=content,
        raw_embed=raw_embed,
        reply_id=reply_id,
        pin=pin,
    )


async def send_message(
    entity_factory: traits.EntityFactoryAware,
    ctx: SlashContext,
    send_channel: hikari.InteractionChannel,
    content: hikari.UndefinedOr[str],
    raw_embed: hikari.UndefinedOr[str],
    reply_id: hikari.UndefinedOr[str],
    pin: bool = False,
):
    embed = hikari.UNDEFINED
    replyish = hikari.UNDEFINED
    if raw_embed:
        try:
            embed = entity_factory.entity_factory.deserialize_embed(json.loads(raw_embed))

        except (TypeError, ValueError, SyntaxError) as exc:
            await ctx.respond(content=f"Invalid embed passed: {str(exc)[:1970]}")
            return

    if isinstance(reply_id, str):
        try:
            replyish = int(reply_id)
        except ValueError as exec:
            await ctx.respond(content=f"Invalid Reply ID passed: {str(exec)[:1970]}")

    if content or embed:
        message = await ctx.rest.create_message(
            send_channel.id,
            content=content,
            embed=embed,
            reply=replyish,
            user_mentions=True,
            role_mentions=True,
        )
        if pin:
            await ctx.rest.pin_message(send_channel.id, message.id)
        await ctx.respond("✅")

    else:
        await ctx.respond(content="Please provide `content` or `raw_embed`.")


loaders = component.make_loader()
