"""Provides a simple Welcome message when a new member joins the Guild.

TODO
----
- Implement command to change channel.
- Implement command to change welcome message."""
from __future__ import annotations

import hikari
import tanjun

from ..models.cache.redis import RedisCache

__all__ = [
    "welcome_member_message",
    "welcome_member_dm",
    "loaders",
]

component = tanjun.Component()

DEFAULT_WELCOME_MESSAGE = """Hello {member_mention}! Welcome to the {guild_name}!\n\n
We’re so happy to have you here! Make sure to read {{server_rules}} and {{server_info}} and enjoy the server!\n
If you have any questions feel free to ask in {{server_help}}!"""


@component.with_listener(hikari.MemberCreateEvent)
async def welcome_member_message(
    event: hikari.MemberCreateEvent,
    redis_client: RedisCache = tanjun.injected(type=RedisCache),
):
    """Automatically sends a message to a preset channel (or to #welcome, default) welcoming new members on join.

    No Command."""
    guild = event.get_guild()
    if not guild:
        guild = await event.app.rest.fetch_guild(event.guild_id)

    welcome_message = await _get_welcome_message(guild, event.member, redis_client)
    welcome_message = welcome_message.format(
        server_rules="<#483967686540394506>",
        server_info="<#735635802960429087>",
        server_help="<#516027780908056578>",
    )
    welcome_channel = await _get_welcome_channel(event.app.rest, guild, redis_client._redis_client)

    await welcome_channel.send(welcome_message, user_mentions=True)


@component.with_listener(hikari.MemberCreateEvent)
async def welcome_member_dm(
    event: hikari.MemberCreateEvent,
    redis_client: RedisCache = tanjun.injected(type=RedisCache),
):
    """Automatically sends a Direct message to a welcoming new members on join.

    No Command."""
    guild = event.get_guild()
    if not guild:
        guild = await event.app.rest.fetch_guild(event.guild_id)

    welcome_message = await _get_welcome_message(guild, event.member, redis_client._redis_client)
    welcome_message = welcome_message.format(
        server_rules="#server-rules",
        server_info="#server-info",
        server_help="#server-help",
    )

    await event.member.send(welcome_message)


async def _get_welcome_message(
    guild: hikari.GatewayGuild,
    member: hikari.MemberCreateEvent,
    redis_client: RedisCache,
) -> str:
    welcome_message = await redis_client._redis_client.get(f"{guild.id}:welcome_message")
    if not welcome_message:
        welcome_message = DEFAULT_WELCOME_MESSAGE.format(member_mention=member.mention, guild_name=guild.name)

    return welcome_message


async def _get_welcome_channel(
    rest: hikari.RESTAware, guild: hikari.GatewayGuild, redis_client: RedisCache
) -> hikari.TextableChannel | hikari.GuildChannel | None:
    if not guild:
        return
    welcome_chan_id = await redis_client._redis_client.get(f"{guild.id}:welcome_message_channel")
    if welcome_chan_id:
        if welcome_chan := guild.get_channel(welcome_chan_id):
            return welcome_chan
        if welcome_chan := await rest.fetch_channel(welcome_chan_id):
            return welcome_chan

    # Search for default #welcome channel
    guild_channels = guild.get_channels()
    for channel in guild_channels:
        if (welcome_chan := guild_channels.get(channel)).name == "welcome":
            return welcome_chan


loaders = component.make_loader()
"""Default tanjun loader."""
