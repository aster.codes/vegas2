"""Debugger module only usable by bot owner."""
from __future__ import annotations

import datetime
import os
import platform
import time
from typing import Sequence

import hikari
import psutil
import tanjun
import yuyo
from aioredis.exceptions import ResponseError
from tanjun.abc import SlashContext
from tanjun.checks import with_owner_check

from ..models.cache.redis import RedisCache
from ..models.database.postgres import PostgresDatabase
from ..util.helpers import get_vegas_logger

__all__ = [
    "debug",
    "clear",
    "about_bot",
    "redis",
    "execute_redis_command",
    "load_module",
    "unload_module",
    "reload_module",
    "loaders",
]

logger = get_vegas_logger(__name__)
component = tanjun.Component()

debug = component.with_slash_command(
    tanjun.slash_command_group("debug", "A bunch of debugger commands.")
)  # pylint: disable=C0103 


@debug.add_command
@tanjun.as_slash_command("shutdown", "Gracefully stop the bot.")
async def shutdown(
    ctx: tanjun.abc.SlashContext,
    client: tanjun.Client = tanjun.injected(type=tanjun.Client),
    bot: hikari.GatewayBot = tanjun.injected(type=hikari.GatewayBot),
):
    await ctx.respond("Bye!")
    logger.info("Stopping Client.")
    await client.close()
    logger.info("Stopped Client.")
    logger.info("Stopping GatewayBot.")
    await bot.close()
    logger.info("Stopped GatewayBot.")


@debug.add_command
@tanjun.with_str_slash_option(
    "clear_type",
    "The type of clear to use",
    choices=("terminal", "commands"),
    default="terminal",
)
@tanjun.with_bool_slash_option("clear_global", "Whether the Global Commands should bel cleared", default=False)
@tanjun.as_slash_command("clear", "Clear terminal or commands")
async def clear(
    ctx: SlashContext,
    clear_type: str = "terminal",
    clear_global: bool = False,
    client: tanjun.Client = tanjun.injected(type=tanjun.Client),
):
    if clear_type == "terminal":
        if os.name == "nt":
            os.system("cls")  # noqa: S605, S607
        else:
            os.system("clear")  # noqa: S605, S607
        await ctx.respond("✅")
    elif clear_type == "commands":
        if ctx.guild_id:
            await client.clear_application_commands(guild=ctx.guild_id)
            if clear_global:
                await client.clear_application_commands()
    else:
        await ctx.respond("Wow something went really wrong I'm so sorry.")
        return
    await ctx.respond("Cleared!")


@debug.with_command
@tanjun.as_slash_command("about", "About this bot.")
async def about_bot(ctx: SlashContext, process: psutil.Process = tanjun.cached_inject(psutil.Process)):
    """Get the bot's current delay."""
    bot_user = await ctx.rest.fetch_my_user()
    start_time = time.perf_counter()
    rest_latency = (time.perf_counter() - start_time) * 1_000
    gateway_latency = ctx.shards.heartbeat_latency * 1_000 if ctx.shards else float("NAN")
    start_date = datetime.datetime.fromtimestamp(process.create_time())
    uptime = datetime.datetime.now() - start_date
    memory_usage: float = process.memory_full_info().uss / 1024**2
    cpu_usage: float = process.cpu_percent() / psutil.cpu_count()
    memory_percent: float = process.memory_percent()
    help_links = """
    [Vegas Commands Documentation & Help](vegas2.patchwork.systems/docs/)\n
    [Support Discord](https://discord.gg/SZcPc5WkSS)\n
    [The Patchwork Collective Patreon (Vegas2 Developers)](https://www.patreon.com/patchworkcollective)\n
    [Gitlab Home](https://gitlab.com/aster.codes/vegas2)\n
    [Report an Issue](https://gitlab.com/aster.codes/vegas2/-/issues)\n
    """
    patreon_supporters = """@Raekoam#2765"""
    embed = (
        hikari.Embed()
        .set_thumbnail(bot_user.make_avatar_url())
        .set_author(name=f"About {bot_user.username}", url="https://gitlab.com/aster.codes/maybax2/")
        .add_field(name="Uptime", value=f"{uptime}", inline=True)
        .add_field(
            name="Memory Usage",
            value=f"{memory_usage:.2f} MB ({memory_percent:.0f}%)",
            inline=True,
        )
        .add_field(name="CPU Usage", value=f"{cpu_usage:.2f}% CPU", inline=True)
        .add_field(name="REST Client Ping:", value=f"{rest_latency}", inline=True)
        .add_field(name="Gateway Client Ping:", value=f"{gateway_latency}", inline=True)
        .add_field("Help & Support", value=help_links, inline=False)
        .add_field("Thanks to our Patreon Supporters!", value=patreon_supporters, inline=False)
        .set_footer(
            text=f"Made with Tanjun and Python {platform.python_version()}",
        )
    )

    await ctx.respond(embed=embed)


postgres = debug.with_command(tanjun.slash_command_group("postgres", "Postgres debugging commands."))  # pylint: disable=C0103

#  pg_methods = [method for method in dir(PostgresDatabase) if method.startswith('_') is False]


def dict_to_embed(query: str, index: int, idict: dict) -> hikari.Embed:
    keys = idict.keys()
    embed = hikari.Embed(description=f"```sql\n{query}\n```")
    for key in keys:
        embed.add_field(
            name=f"{key}: type({type(idict[key])})", value=idict[key] if idict[key] else "```[ None ]```", inline=False
        )

    return embed


@postgres.with_command
@with_owner_check
@tanjun.with_str_slash_option("query", "The Postgres Query to run on the pool.")
@tanjun.as_slash_command("execute-query", "Run a query on Postgres.")
async def execute_postgres_command(
    ctx: SlashContext,
    query: str,
    postgres: PostgresDatabase = tanjun.inject(type=PostgresDatabase),
    component_client: yuyo.ComponentClient = tanjun.inject(type=yuyo.ComponentClient),
):
    from psycopg2.extras import RealDictCursor

    guild = ctx.get_guild() or await ctx.fetch_guild()
    #  TODO: This makes me sad  :(
    with (await postgres._PostgresDatabase__pool.cursor(cursor_factory=RealDictCursor)) as cursor:
        await cursor.execute(query)
        results = await cursor.fetchall()

    iterator = (
        (
            f"Entry {idx+1}/{len(results)}",
            dict_to_embed(query, idx, result)
            .set_thumbnail(guild.icon_url)
            .set_author(name=ctx.author.username, icon=ctx.author.avatar_url),
        )
        for idx, result in enumerate(results)
    )
    paginator = yuyo.ComponentPaginator(iterator, authors=(ctx.author,), timeout=datetime.timedelta(minutes=3))

    if first_response := await paginator.get_next_entry():
        content, embed = first_response
        message = await ctx.edit_initial_response(content=content, component=paginator, embed=embed)
        component_client.set_executor(message, paginator)
        return
    else:
        await ctx.respond(f"No results for the query: \n```sql\n{query}\n```")


redis = debug.with_command(tanjun.slash_command_group("redis", "Redis debugging commands."))  # pylint: disable=C0103


@redis.with_command
@with_owner_check
@tanjun.with_str_slash_option("value", "The value to pass to the command.", default=None)
@tanjun.with_str_slash_option("key", "The Redis key to run in the command.", default=None)
@tanjun.with_str_slash_option("command", "The Redis command to run on the pool.")
@tanjun.as_slash_command("execute-command", "Run a command on Redis.")
async def execute_redis_command(
    ctx: SlashContext,
    command: str,
    key: str | None = None,
    value: str | None = None,
    redis: RedisCache = tanjun.injected(type=RedisCache),
):
    """Debugger command for redis.

    Parameters
    ==========
    command: str
        The Redis command to execute.
    key: str | None = None
        They key to provide the given Redis command.
    value: str | None = None
        They value to provide the given Redis command."""

    if key and value:
        if "," in value:
            value_list = value.split(",")
            try:
                result = await redis._redis_client.execute_command(command, key, *value_list)
            except ResponseError:
                value_list = [int(val) for val in value_list]
                result = await redis._redis_client.execute_command(command, key, *value_list)

        else:
            result = await redis._redis_client.execute_command(command, key, value)
    elif key and not value:
        result = await redis._redis_client.execute_command(command, key)
    else:
        result = await redis._redis_client.execute_command(command)

    if result is True:
        result = "***✅ Success!***"
    elif result is False:
        result = "***⛔ Returned False***"

    embed = hikari.Embed(
        title=f"Command `{command}` Results",
        description=f"{result}\n",
        color=14167072,
    ).set_author(name=ctx.author.username, icon=ctx.author.avatar_url)

    await ctx.respond(embed=embed)


from pathlib import Path


def all_plugins() -> Sequence[str]:
    plugins = [f"vegas2.plugins.{p.name[:-3]}" for p in Path(__file__).resolve().parent.glob("*py") if p.name != "__init__.py"]
    return plugins


plugin_choices = all_plugins()


@debug.with_command
@tanjun.with_str_slash_option("module_name", "The module to target.", choices=plugin_choices)
@tanjun.as_slash_command("reload_module", "Reloads a module.")
async def reload_module(
    ctx: tanjun.abc.SlashContext,
    module_name: str,
    client: tanjun.Client = tanjun.injected(type=tanjun.Client),
):
    """Reload a module in Tanjun"""
    start_time = time.time()
    output = "No action taken..."
    try:
        client.reload_modules(module_name)
        logger.info(f"Reloaded {module_name}")
    except ValueError:
        client.load_modules(module_name)
        output = "Loaded!"

    # await client.declare_global_commands()
    end_time = time.time()
    await ctx.respond(output + f" Took {end_time - start_time}")


@debug.with_command
@tanjun.with_str_slash_option("module_name", "The module to target.", choices=plugin_choices)
@tanjun.as_slash_command("unload_module", "Removes a module.")
async def unload_module(
    ctx: tanjun.abc.SlashContext,
    module_name: str,
    client: tanjun.Client = tanjun.injected(type=tanjun.Client),
):
    """Unload a module in Tanjun"""
    try:
        client.unload_modules(module_name)
    except ValueError:
        await ctx.respond("Couldn't unload module...")
        return

    await client.declare_global_commands()
    await ctx.respond("Unloaded!")


@debug.with_command
@tanjun.with_str_slash_option("module_name", "The module to reload.", choices=plugin_choices)
@tanjun.as_slash_command("load_module", "Loads a module.")
async def load_module(
    ctx: tanjun.abc.SlashContext,
    module_name: str,
    client: tanjun.Client = tanjun.injected(type=tanjun.Client),
):
    """Load a module in Tanjun"""
    try:
        client.load_modules(module_name)
    except ValueError:
        await ctx.respond("Can't find that module!")
        return

    await client.declare_global_commands()
    await ctx.respond("Loaded!")


loaders = component.make_loader()
