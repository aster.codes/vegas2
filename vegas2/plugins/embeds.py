"""This plugin provides commands to create and edit hikari.Embeds interactively.

After running one of the commands, an Embed will be displayed and a menu of Buttons
will be attached to the embed. Once the calling Member selects a Button, the bot will
run a function to update the Embed accordingly.

The same Embed will be reused/edited for a single process."""
from __future__ import annotations

import asyncio
import datetime
from typing import Any

import hikari
import tanjun
import yuyo
from hikari.colors import Color
from hikari.embeds import Embed
from hikari.events.message_events import GuildMessageCreateEvent
from hikari.interactions.base_interactions import ResponseType
from hikari.components import ButtonStyle
from tanjun.abc import SlashContext

from ..checks.role_checks import with_any_role_check
from ..util.helpers import (
    collect_response,
    ensure_guild_channel_validator,
    get_vegas_logger,
)
from ..util.helpers import is_int_validator as is_int

__all__ = [
    "embed",
    "interactive_post",
    "interactive_edit",
    "add_field_button",
    "add_ping_button",
    "add_server_logo_button",
    "change_field_button",
    "change_icon_button",
    "change_text_on_publish_button",
    "color_button",
    "description_button",
    "footer_button",
    "image_button",
    "pin_embed_button",
    "publish_to_channel_button",
    "remove_field_button",
    "show_status_button",
    "title_button",
    "loaders",
]

logger = get_vegas_logger(__file__)

CONFIRM_ANSWERS = ["yes", "y", "ye", "t", "true"]
NEGATIVE_ANSWERS = ["no", "n", "false", "f", "❌"]

EMBED_MENU = {
    "📋": {"title": "Title", "style": ButtonStyle.SECONDARY},
    "💬": {"title": "Description", "style": ButtonStyle.SECONDARY},
    "🖼️": {"title": "Change Icon", "style": ButtonStyle.SECONDARY},
    "📦": {"title": "Add Server Logo", "style": ButtonStyle.SECONDARY},
    "🎨": {"title": "Image", "style": ButtonStyle.SECONDARY},
    "👣": {"title": "Footer", "style": ButtonStyle.SECONDARY},
    "➕": {"title": "Add Field", "style": ButtonStyle.SECONDARY},
    "🖊️": {"title": "Change Field", "style": ButtonStyle.SECONDARY},
    "💢": {"title": "Remove Field", "style": ButtonStyle.SECONDARY},
    "🌈": {"title": "Color", "style": ButtonStyle.SECONDARY},
    "📰": {"title": "Change text on Publish", "style": ButtonStyle.SECONDARY},
    "📌": {"title": "Pin Embed", "style": ButtonStyle.SECONDARY},
    "🏓": {"title": "Add Ping", "style": ButtonStyle.SECONDARY},
    "❓": {"title": "Show Status", "style": ButtonStyle.SECONDARY},
}
EMBED_OK = {
    "🆗": {"title": "Publish to Channel", "style": ButtonStyle.PRIMARY},
    "❌": {"title": "Cancel", "style": ButtonStyle.DANGER},
}

EMBED_MENU_FULL = EMBED_MENU | EMBED_OK

component = tanjun.Component()

embed = component.with_slash_command(  # pylint: disable=C0103
    tanjun.slash_command_group("embed", "Work with Embeds! (Requires Can Embed)", default_to_ephemeral=False)
)
"""The `/embed` command group."""


@embed.with_command
@with_any_role_check(
    [
        "Can Embed",
    ]
)
@tanjun.with_str_slash_option("message_id", "The Message Id to edit.")
@tanjun.as_slash_command("interactive-edit", "Edit an Embed!")
async def interactive_edit_command(
    ctx: SlashContext,
    message_id: hikari.Message,
    bot: hikari.GatewayBot = tanjun.injected(type=hikari.GatewayBot),
    component_client: yuyo.ComponentClient = tanjun.injected(type=yuyo.ComponentClient),
):
    await interactive_edit(ctx, message_id, bot, component_client)


async def interactive_edit(
    ctx: SlashContext,
    message_id: hikari.Message,
    bot: hikari.GatewayBot = tanjun.injected(type=hikari.GatewayBot),
    component_client: yuyo.ComponentClient = tanjun.injected(type=yuyo.ComponentClient),
):
    """Edit a hikari.Embed interactively.

    Once started, the Embed will be displayed one time, then updated in place.

    Command: /embed interactive-edit

    Parameters
    ----------
    message_id: hikari.Message
        The Message to target. This can also be a Message id."""
    if not ctx.client.cache:
        return
    message = ctx.client.cache.get_message(message_id) if ctx.client.cache else None
    if not message:
        for channel_id in ctx.client.cache.get_guild_channels_view():
            try:
                message = await ctx.rest.fetch_message(channel_id, message_id)
                break
            except hikari.NotFoundError:
                continue

    if not message:
        await ctx.respond("I couldn't find that message...")
        return

    await embed_builder_loop(
        ctx,
        message.embeds[0],
        bot=bot,
        component_client=component_client,
    )


@embed.with_command
@with_any_role_check(
    [
        "Can Embed",
    ]
)
@tanjun.as_slash_command("interactive-post", "Build an Embed!")
async def interactive_post_command(
    ctx: SlashContext,
    bot: hikari.GatewayBot = tanjun.injected(type=hikari.GatewayBot),
    component_client: yuyo.ComponentClient = tanjun.injected(type=yuyo.ComponentClient),
) -> None:
    await interactive_post(ctx, bot, component_client)


async def interactive_post(
    ctx: SlashContext,
    bot: hikari.GatewayBot = tanjun.injected(type=hikari.GatewayBot),
    component_client: yuyo.ComponentClient = tanjun.injected(type=yuyo.ComponentClient),
) -> None:
    """Create a new hikari.Embed interactively.

    Once started, the Embed will be displayed one time, then updated in place.

    Command: /embed interactive-post"""
    building_embed = Embed(title="New Embed")

    await embed_builder_loop(ctx, building_embed, bot=bot, component_client=component_client)


async def embed_builder_loop(
    ctx: SlashContext,
    building_embed: Embed,
    bot: hikari.GatewayBot,
    component_client: yuyo.ComponentClient,
):
    """
    Helper function for Embed Builder.

    Handles main logic. Updates the embed, provides menu,
    checks interaction, then calls helper function.
    """
    menu = build_menu(ctx)
    ctx.client.metadata["embed"] = building_embed
    ctx.client.metadata["roles"] = []
    ctx.client.metadata["text"] = ""
    ctx.client.metadata["pin"] = False

    message = await ctx.edit_initial_response(
        "Click/Tap your choice below, then watch the embed update!",
        embed=ctx.client.metadata["embed"],
        components=[*menu],
    )
    while True:
        executor = yuyo.components.WaitFor(authors=(ctx.author.id,), timeout=datetime.timedelta(seconds=30))
        component_client.set_executor(message.id, executor)
        try:
            result = await executor.wait_for()
            await result.defer(ResponseType.DEFERRED_MESSAGE_UPDATE)
        except asyncio.TimeoutError:
            await ctx.edit_initial_response("Waited for 60 seconds... Timeout.", embed=None, components=[])
            continue
        key = result.interaction.custom_id
        selected = EMBED_MENU_FULL[key]
        if selected["title"] == "Cancel":
            await ctx.edit_initial_response(content="Exiting!", components=[])
            return

        await globals()[f"{selected['title'].lower().replace(' ', '_')}_button"](ctx, bot, component_client)
        await ctx.edit_initial_response(
            "Click/Tap your choice below, then watch the embed update!",
            embed=ctx.client.metadata["embed"],
            components=[*menu],
        )


def build_menu(ctx: SlashContext) -> list[Any]:
    """
    Internal Helper function for Embed Builder.
    Builds the interactive Embed Menu Component with buttons.

    Requires
    ========
    - EMBED_MENU
    """
    menu = []
    menu_count = 0
    last_menu_item = list(EMBED_MENU)[-1]
    row = ctx.rest.build_message_action_row()
    for emote, options in EMBED_MENU.items():
        (row.add_interactive_button(options["style"], emote).set_label(options["title"]).set_emoji(emote).add_to_container())
        menu_count += 1
        if menu_count == 5 or last_menu_item == emote:
            menu.append(row)
            row = ctx.rest.build_message_action_row()
            menu_count = 0

    confirmation_row = ctx.rest.build_message_action_row()
    for emote, options in EMBED_OK.items():
        (confirmation_row.add_interactive_button(options["style"], emote).set_label(options["title"]).set_emoji(emote).add_to_container())
    menu.append(confirmation_row)

    return menu


async def title_button(
    ctx: SlashContext,
    bot: hikari.GatewayBot,
    component_client: yuyo.ComponentClient,
):
    """Handles button to change the Embed title.

    Prompts the user for a new title. Expects a message of less than 200 characters."""
    embed_dict, *_ = bot.entity_factory.serialize_embed(ctx.client.metadata["embed"])
    await ctx.edit_initial_response(content="Set Title for embed:", components=[])
    event = await collect_response(ctx.client.events, ctx)
    if not event or not event.content:
        return
    embed_dict["title"] = event.content[:200]
    ctx.client.metadata["embed"] = bot.entity_factory.deserialize_embed(embed_dict)
    await ctx.edit_initial_response(content="Title updated!", embed=ctx.client.metadata["embed"], components=[])
    try:
        await event.message.delete()
    except hikari.NotFoundError:
        ...


async def description_button(
    ctx: SlashContext,
    bot: hikari.GatewayBot,
    component_client: yuyo.ComponentClient,
):
    """Handles button to change the Embed description.

    Prompts the user for a new description. Expects a message of less than 4,090 characters."""
    embed_dict, *_ = bot.entity_factory.serialize_embed(ctx.client.metadata["embed"])
    await ctx.edit_initial_response(content="Set Description for embed:", components=[])
    event = await collect_response(ctx.client.events, ctx, validator=lambda _, event: len(event.message.content) < 4096)
    if not event or not event.content:
        return
    embed_dict["description"] = event.content[:4090]
    ctx.client.metadata["embed"] = bot.entity_factory.deserialize_embed(embed_dict)
    await ctx.edit_initial_response(content="Description Updated!", embed=ctx.client.metadata["embed"], components=[])
    try:
        await event.message.delete()
    except hikari.NotFoundError:
        ...


async def change_icon_button(
    ctx: SlashContext,
    _: hikari.GatewayBot,
    component_client: yuyo.ComponentClient,
):
    """Handles button to change the Embed icon.

    Prompts the caller for a URL to an image to set for the Embed Icon."""
    await ctx.edit_initial_response(content="Set Icon for embed:", components=[])
    event = await collect_response(
        ctx.client.events,
        ctx,
        validator=lambda _, event: event.message.content.startswith("http"),
    )
    if not event or not event.content:
        return
    ctx.client.metadata["embed"].set_thumbnail(event.content)
    await ctx.edit_initial_response(content="Thumbnail Updated!", embed=ctx.client.metadata["embed"], components=[])
    try:
        await event.message.delete()
    except hikari.NotFoundError:
        ...


async def add_server_logo_button(
    ctx: SlashContext,
    _: hikari.GatewayBot,
    component_client: yuyo.ComponentClient,
):
    """Handles button to change the Embed Icon to the Guild Icon.

    Adds the calling Guild's Icon/Logo as the Embed icon."""
    ctx.client.metadata["embed"].set_thumbnail((await ctx.fetch_guild()).icon_url)
    await ctx.edit_initial_response(content="Logo Updated!", embed=ctx.client.metadata["embed"], components=[])


async def image_button(
    ctx: SlashContext,
    _: hikari.GatewayBot,
    component_client: yuyo.ComponentClient,
):
    """Handles button to change the Embed image.

    Prompts the user for a new image. Expects a message of less than 2,000 characters and to be a URL."""
    await ctx.edit_initial_response(content="Set Image for embed:", components=[])
    event = await collect_response(ctx.client.events, ctx, validator=lambda _, event: len(event.message.content) < 2000)
    if not event or not event.content:
        return
    ctx.client.metadata["embed"].set_image(event.content)
    await ctx.edit_initial_response(content="Image Updated!", embed=ctx.client.metadata["embed"], components=[])
    try:
        await event.message.delete()
    except hikari.NotFoundError:
        ...


async def footer_button(
    ctx: SlashContext,
    _: hikari.GatewayBot,
    component_client: yuyo.ComponentClient,
):
    """Handles button to change the Embed footer.

    Prompts the user for a new footer text. Expects a message of less than 2,000 characters."""
    await ctx.edit_initial_response(content="Set Footer for embed:", components=[])
    event = await collect_response(ctx.client.events, ctx, validator=lambda _, event: len(event.message.content) < 2000)
    if not event or not event.content:
        return
    guild = ctx.get_guild()
    if not guild:
        guild = await ctx.fetch_guild()
    ctx.client.metadata["embed"].set_footer(text=event.content, icon=guild.icon_url)
    await ctx.edit_initial_response(content="Footer Updated!", embed=ctx.client.metadata["embed"], components=[])
    try:
        await event.message.delete()
    except hikari.NotFoundError:
        ...


async def add_field_button(
    ctx: SlashContext,
    _: hikari.GatewayBot,
    component_client: yuyo.ComponentClient,
):
    """Handles button to add a field to the Embed.

    Prompts the caller for a Field Name. Expects less than 256 characters.

    Prompts the caller for a Field Value/Body. Expects less than 4,096 characters.

    Prompts the caller for a Field Inline Status. Expects less than yes/no answer.
    """
    # Prompt for Name
    await ctx.edit_initial_response("What would you like the Field Title to be?", components=[])
    name_event = await collect_response(ctx.client.events, ctx, validator=lambda _, event: len(event.message.content) < 256)
    if not name_event or not name_event.content or name_event.content in NEGATIVE_ANSWERS[-1]:
        return
    field_name = name_event.content

    # Prompt for Body/Value
    await ctx.edit_initial_response("What would you like the Field Body to be?", components=[])
    body_event = await collect_response(ctx.client.events, ctx, validator=lambda _, event: len(event.message.content) < 4096)
    if not body_event or not body_event.content or body_event.content in NEGATIVE_ANSWERS[-1]:
        return
    field_body = body_event.content

    # Prompt for inline
    await ctx.edit_initial_response("Would you like this to be inline? (y/n)", components=[])
    inline_event = await collect_response(
        ctx.client.events,
        ctx,
        validator=NEGATIVE_ANSWERS + CONFIRM_ANSWERS,
    )
    if not inline_event or not inline_event.content or inline_event.content.lower() in NEGATIVE_ANSWERS[-1]:
        return
    if inline_event.content.lower() in CONFIRM_ANSWERS:
        field_inline = True
    elif inline_event.content.lower() in NEGATIVE_ANSWERS:
        field_inline = False
    else:
        await ctx.edit_initial_response("Didn't understand that answer, assuming `No`.", embed=None, components=[])
        field_inline = False

    try:
        # Clean up messages
        await name_event.message.delete()
        await body_event.message.delete()
        await inline_event.message.delete()
    except hikari.NotFoundError:
        ...

    ctx.client.metadata["embed"].add_field(name=field_name, value=field_body, inline=field_inline)
    await ctx.edit_initial_response(content="Field Added!", embed=ctx.client.metadata["embed"], components=[])


async def change_field_button(
    ctx: SlashContext,
    _: hikari.GatewayBot,
    component_client: yuyo.ComponentClient,
):
    """Handles button to change a field on the Embed.

    Prompts the caller for a Field Name. Expects less than 256 characters.

    Prompts the caller for a Field Value/Body. Expects less than 4,096 characters.

    Prompts the caller for a Field Inline Status. Expects less than yes/no answer.
    """
    fields = ""
    field_name = ""
    field_body = ""

    for k, field in enumerate(ctx.client.metadata["embed"].fields):
        # Create a simple text menu starting at 1.
        k += 1
        fields += f"```{k} - {field.name}```{k}"

    await ctx.edit_initial_response(
        f"What Field would you like to update? Provide just the  Index Number: \n {fields}",
        components=[],
    )

    field_idx_event = await collect_response(ctx.client.events, ctx, validator=is_int)
    if not field_idx_event or not field_idx_event.content or field_idx_event.content.lower() in NEGATIVE_ANSWERS[-1]:
        return
    field_idx = int(field_idx_event.content) - 1

    await ctx.edit_initial_response("What would you like the Field Title to be?", components=[])
    name_event = await collect_response(ctx.client.events, ctx, validator=lambda _, event: len(event.message.content) < 256)
    if not name_event or not name_event.content or name_event.content.lower() in NEGATIVE_ANSWERS[-1]:
        return
    field_name = name_event.content

    await ctx.edit_initial_response("What would you like the Field Body to be?", components=[])
    body_event = await collect_response(ctx.client.events, ctx, validator=lambda _, event: len(event.message.content) < 4096)
    if not body_event or not body_event.content or body_event.content.lower() in NEGATIVE_ANSWERS[-1]:
        return
    field_body = body_event.content

    await ctx.edit_initial_response("Would you like this to be inline? (y/n)", components=[])
    inline_event = await collect_response(
        ctx.client.events,
        ctx,
        validator=CONFIRM_ANSWERS + NEGATIVE_ANSWERS,
    )
    if not inline_event or not inline_event.content:
        return
    if inline_event.content.lower() in CONFIRM_ANSWERS:
        field_inline = True
    elif inline_event.content.lower() in NEGATIVE_ANSWERS:
        field_inline = False
    else:
        await ctx.edit_initial_response("Didn't understand that answer, assuming `No`.", embed=None, components=[])
        field_inline = False

    try:
        await name_event.message.delete()
        await body_event.message.delete()
        await inline_event.message.delete()
    except hikari.NotFoundError:
        ...
    ctx.client.metadata["embed"].edit_field(field_idx, field_name, field_body, inline=field_inline)
    await ctx.edit_initial_response(content="Field Added!", embed=ctx.client.metadata["embed"], components=[])


async def remove_field_button(
    ctx: SlashContext,
    _: hikari.GatewayBot,
    component_client: yuyo.ComponentClient,
):
    """Handles button to change a field on the Embed.

    Displays a text menu, listing all feilds and indexing/numbering each.

    Prompts the caller Field Index (number) to remove.
    """
    fields = ""
    for k, field in enumerate(ctx.client.metadata["embed"].fields):
        k += 1
        fields += f"```{k} - {field.name}```{k}"

    await ctx.edit_initial_response(
        f"What Field would you like to update? Provide just the  Index Number: \n {fields}",
        components=[],
    )
    field_idx_event = await collect_response(ctx.client.events, ctx, validator=is_int)
    if not field_idx_event or not field_idx_event.content or field_idx_event.content.lower() in NEGATIVE_ANSWERS[-1]:
        return
    field_idx = int(field_idx_event.content) - 1
    ctx.client.metadata["embed"].remove_field(field_idx)
    await ctx.edit_initial_response("Removed field!", embed=ctx.client.metadata["embed"])


async def color_button(
    ctx: SlashContext,
    bot: hikari.GatewayBot,
    component_client: yuyo.ComponentClient,
):
    """Handles button to change the color on the Embed.

    Prompts the caller for a hex or decimal color.
    """
    embed_dict, *_ = bot.entity_factory.serialize_embed(ctx.client.metadata["embed"])
    await ctx.edit_initial_response(content="Set Color for embed:", components=[])
    event = await collect_response(ctx.client.events, ctx)
    if not event or not event.content or event.content.lower() in NEGATIVE_ANSWERS[-1]:
        return
    embed_dict["color"] = Color(event.content)
    ctx.client.metadata["embed"] = bot.entity_factory.deserialize_embed(embed_dict)
    await ctx.edit_initial_response(content="Color updated!", embed=ctx.client.metadata["embed"], components=[])
    await event.message.delete()


async def change_text_on_publish_button(
    ctx: SlashContext,
    _: hikari.GatewayBot,
    component_client: yuyo.ComponentClient,
):
    """Handles button to change the text that is displayed when the Embed is published.

    Prompts the caller for text. Expects less than 2,000 characters.
    """
    await ctx.edit_initial_response(content="Set Text for Publish:", components=[])
    event = await collect_response(ctx.client.events, ctx, validator=lambda ctx, event: len(event.content) < 2000)
    if not event or not event.content or event.content.lower() in NEGATIVE_ANSWERS[-1]:
        return
    ctx.client.metadata["text"] = event.content
    await ctx.edit_initial_response(content="Text for Publish updated!")
    await event.message.delete()


async def pin_embed_button(
    ctx: SlashContext,
    _: hikari.GatewayBot,
    component_client: yuyo.ComponentClient,
):
    """Handles button to pin Embed after publish."""
    ctx.client.metadata["pin"] = True
    await ctx.edit_initial_response(content="Set to Pin!", components=[])
    await asyncio.sleep(3)


async def add_ping_button(
    ctx: SlashContext,
    bot: hikari.GatewayBot,
    component_client: yuyo.ComponentClient,
):
    """
    Helper function for Embed Builder.
    Adds a role to ping on publish.
    """
    if not ctx.guild_id:
        return
    roles = ctx.client.cache.get_roles_view_for_guild(ctx.guild_id)
    if not roles:
        roles = {role.id: role for role in (await ctx.rest.fetch_roles(ctx.guild_id))}

    while True:
        await ctx.edit_initial_response(content="Add Ping for Publish. To return to menu, send ❌.", components=[])
        found_role = None
        try:
            with bot.stream(GuildMessageCreateEvent, timeout=60).filter(("author.id", ctx.author.id)) as stream:
                async for event in stream:
                    if event.content == "❌":
                        try:
                            await event.message.delete()
                        except hikari.NotFoundError:
                            ...
                        return

                    for role_id, role in roles.items():
                        if str(role.id) in event.content or role.name in event.content:
                            found_role = role
                            break

                    if not found_role:
                        await ctx.edit_initial_response(content=f"Role `{event.content}` not found! Try again?")
                    else:
                        ctx.client.metadata["roles"].append(found_role)
                        await ctx.edit_initial_response(content=f"Added {found_role.mention} to ping list for Publish!")

                    try:
                        await event.message.delete()
                    except hikari.NotFoundError:
                        ...
                    await asyncio.sleep(5)
                    await ctx.edit_initial_response(
                        content="Add Ping for Publish. To return to menu, send ❌.",
                        components=[],
                    )
        except asyncio.TimeoutError:
            await ctx.edit_initial_response(content="Waited for 60 seconds... Timed out.")
            return None


async def show_status_button(
    ctx: SlashContext,
    _: hikari.GatewayBot,
    component_client: yuyo.ComponentClient,
):
    """
    Helper function for Embed Builder.
    Shows the current metadata (pin/pings/content text) for the current Embed.
    """
    roles = ""
    for role in ctx.client.metadata["roles"]:
        roles += f"{role.mention}\n"
    status_txt = (
        f"Text to show with this embed on publish: ```{ctx.client.metadata['text']}```"
        "\n"
        f"Roles set to ping: {roles}"
        "\n"
        f"Pin: {ctx.client.metadata['pin']}"
    )
    await ctx.edit_initial_response(content=status_txt, components=[])
    await asyncio.sleep(5)


async def publish_to_channel_button(
    ctx: SlashContext,
    _: hikari.GatewayBot,
    component_client: yuyo.ComponentClient,
):
    """
    Helper function for Embed Builder.
    Publishes embeds to a a provided channel.
    """
    content_str = ""
    for role in ctx.client.metadata["roles"]:
        content_str += f"{role.mention}\n"
    content_str += ctx.client.metadata["text"]
    if content_str == "":
        content_str = "-"

    await ctx.edit_initial_response(content="What channel would you like to Publish to:", components=[])

    event = await collect_response(ctx.client.events, ctx, validator=ensure_guild_channel_validator)
    if not event or not event.content or event.content.lower() in NEGATIVE_ANSWERS[-1]:
        return

    found_channel = None
    channels = ctx.client.cache.get_guild_channels_view() if ctx.client.cache else {}

    for channel_id, channel in channels.items():
        if str(channel.id) in event.content or channel.name.lower() == event.content.lower():
            found_channel = channel
            break
    if not found_channel:
        await ctx.respond("I can't find that channel!")
        return

    embed_message = await found_channel.send(
        content=content_str,
        embed=ctx.client.metadata["embed"],
        user_mentions=True,
        role_mentions=True,
    )

    if ctx.client.metadata["pin"]:
        await found_channel.pin_message(embed_message)


loaders = component.make_loader()
