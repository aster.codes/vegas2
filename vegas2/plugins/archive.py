from __future__ import annotations

import time
from io import BytesIO

import emoji
import hikari
import tanjun
from tanjun.abc import SlashContext
from tanjun.checks import with_owner_check

from ..util.dependencies import Jinja2Dep

__all__ = [
    "archive_grp",
    "archive_channel",
    "loaders",
]

component = tanjun.Component()

archive_grp = component.with_slash_command(
    tanjun.slash_command_group("archive", "Archive channels and categories into HTML files.")
)


@archive_grp.with_command
@with_owner_check
@tanjun.with_channel_slash_option("channel", "Channel to archive into an HTML file.")
@tanjun.with_channel_slash_option(
    "asset_channel",
    "A channel that all files, images, or content that requires discord to cache.",
    default=None,
)
@tanjun.with_channel_slash_option(
    "output_channel",
    "The channel the HTML file should be sent to. (Defaults to called channel)",
    default=None,
)
@tanjun.with_bool_slash_option("dry_run", "True if the archive should delete the channel after.", default=True)
@tanjun.as_slash_command("channel", "Archive a channel.")
async def archive_channel_command(
    ctx: SlashContext,
    channel: hikari.InteractionChannel,
    asset_channel: hikari.InteractionChannel | None = None,
    output_channel: hikari.InteractionChannel | None = None,
    dry_run: bool = True,
    client: tanjun.Client = tanjun.injected(type=tanjun.Client),
    jinja2: Jinja2Dep = tanjun.injected(type=Jinja2Dep),
    bot: hikari.GatewayBotAware = tanjun.injected(type=hikari.GatewayBotAware),
):
    await archive_channel(
        ctx=ctx,
        channel=channel,
        asset_channel=asset_channel,
        output_channel=output_channel,
        dry_run=dry_run,
        client=client,
        jinja2=jinja2,
        bot=bot,
    )


async def archive_channel(
    ctx: SlashContext,
    channel: hikari.InteractionChannel,
    client: tanjun.Client,
    jinja2: Jinja2Dep,
    bot: hikari.GatewayBotAware,
    asset_channel: hikari.InteractionChannel | None = None,
    output_channel: hikari.InteractionChannel | None = None,
    dry_run: bool = True,
):
    """Archive a hikari.TextChannel into a self contained downloadable HTML file.

    Parameters
    ----------
    channel: hikari.InteractionChannel
        A Discord channel to archive.
    asset_channel: hikari.InteractionChannel | None = None
        A Discord TextChannel to archive images, files and other non-embedable assets.
        If None is provided, these assets will be lost and not display in the archive.
    output_channel: hikari.InteractionChannel | None = None
        The Discord TextChannel to send your archive file to. If None is provided, the
        archive will be sent to the calling channel.
    dry_run: bool = True
        Whether the provided `channel` should be deleted after archival. If True, the
        channel will not be deleted. Default is True.
    """
    client.metadata["started"] = time.time()
    client.metadata["ended"] = None
    client.metadata["calling-channel"] = ctx.get_channel() if ctx.get_channel() else await ctx.fetch_channel()
    client.metadata["archive-channel"] = await ctx.rest.fetch_channel(channel.id)
    client.metadata["output-channel"] = (
        await ctx.rest.fetch_channel(output_channel.id) if output_channel else client.metadata["calling-channel"]
    )
    client.metadata["asset-channel"] = (
        await ctx.rest.fetch_channel(asset_channel.id) if asset_channel else client.metadata["output-channel"]
    )
    client.metadata["dry-run"] = True

    client.metadata["cached-roles"] = client.cache.get_roles_view_for_guild(ctx.guild_id)
    client.metadata["cached-emoji"] = client.cache.get_emojis_view()
    client.metadata["cached-members"] = client.cache.get_members_view_for_guild(ctx.guild_id)
    client.metadata["local-cached-members"] = {}
    client.metadata["cached-guild"] = ctx.get_guild() or (await ctx.rest.fetch_guild(ctx.guild_id))
    client.metadata["image-saver"] = jinja2.archive_channel_asset(client.metadata["asset-channel"], client)
    await client.metadata["image-saver"].__anext__()

    total_message_count = 0

    await ctx.respond(
        "Dry run! The channel archive document will be created, but the channel will not be touched."
        if dry_run
        else f"Preparing to archive {client.metadata['archive-channel'].mention} {emoji.emojize(':locked:')}"
    )
    async with client.metadata["calling-channel"].trigger_typing():
        messages_output = BytesIO()
        async for message in client.metadata["archive-channel"].fetch_history().reversed():
            try:
                await ctx.rest.fetch_ban(client.metadata["cached-guild"].id, message.author.id)
            except hikari.NotFoundError:
                total_message_count += 1
                (
                    message_content,
                    pinged_mentions,
                    attachments,
                ) = await jinja2.message_content_chain(message, client)
                embeds = await jinja2.render_embeds(message, bot, client)
                member_mentions = filter(
                    lambda mention: isinstance(mention, hikari.Member),
                    pinged_mentions or [],
                )
                role_mentions = filter(
                    lambda mention: isinstance(mention, hikari.Role),
                    pinged_mentions or [],
                )
                reactions = await jinja2.render_reactions(message, client)
                class_color = (
                    jinja2.sanitize_name(message.member.get_top_role().name)
                    if message.member
                    else (await jinja2.render_class_names(message, client))
                )
                messages_output.write(
                    str.encode(
                        jinja2.get_template("channel-archive-message.html").render(
                            cached_roles=client.metadata["cached-roles"],
                            cached_emoji=client.metadata["cached-emoji"],
                            member_mentions=member_mentions,
                            role_mentions=role_mentions,
                            message=message,
                            message_content=message_content,
                            attachments=attachments,
                            embeds=embeds,
                            reactions=reactions,
                            class_name=class_color,
                        )
                    )
                )
                if len(messages_output.getvalue()) >= 7500000:
                    await jinja2.finalize_html_file(ctx, messages_output, client, total_message_count)
                    messages_output = BytesIO()

        client.metadata["dry-run"] = dry_run
        await jinja2.finalize_html_file(ctx, messages_output, client, total_message_count)


loaders = component.make_loader()
