"""Plugin for providing an interface to change a guild's settings."""
from __future__ import annotations

import logging

import hikari
import tanjun
from tanjun.abc import SlashContext
from tanjun.checks import with_check, with_guild_check

from ..checks.guild_owner_check import guild_owner_check
from ..events import GuildSettingUpdated
from ..models.cache.redis import RedisCache
from ..models.database.postgres import PostgresDatabase
from ..models.guild_settings import LocalGuildSettings

__all__ = [
    "edit_settings",
    "list_settings",
    "loaders",
]

logger = logging.getLogger(__name__)

component = tanjun.Component()

guild_settings = component.with_slash_command(tanjun.slash_command_group("settings", "Change your Servers settings!"))


@guild_settings.with_command
@with_guild_check
@with_check(guild_owner_check)
@tanjun.with_bool_slash_option("enable_logging", "Turn on/off logging.", default=None)
@tanjun.with_channel_slash_option(
    "logging_channel",
    "Change the channel Vegas2 logs general information to.",
    default=None,
)
@tanjun.with_channel_slash_option(
    "vc_logging_channel",
    "Change the channel Vegas2 logs VC information to.",
    default=None,
)
@tanjun.with_channel_slash_option(
    "mod_logging_channel",
    "Change the channel Vegas2 logs Moderation information to.",
    default=None,
)
@tanjun.with_bool_slash_option("enable_auto_delete", "Turn on/off Auto Delete functions.", default=None)
@tanjun.with_channel_slash_option(
    "add_auto_delete_channel",
    "Channel for VEGAS2 to automatically delete messages from.",
    default=None,
)
@tanjun.with_channel_slash_option(
    "remove_auto_delete_channel",
    "Channel for VEGAS2 to automatically delete messages from.",
    default=None,
)
@tanjun.as_slash_command("edit", "Edit your Guilds settings.")
async def edit_settings_command(
    ctx: SlashContext,
    enable_logging: bool | None = None,
    logging_channel: hikari.InteractionChannel | None = None,
    vc_logging_channel: hikari.InteractionChannel | None = None,
    mod_logging_channel: hikari.InteractionChannel | None = None,
    enable_auto_delete: bool | None = None,
    add_auto_delete_channel: hikari.InteractionChannel | None = None,
    remove_auto_delete_channel: hikari.InteractionChannel | None = None,
    postgres: PostgresDatabase = tanjun.injected(type=PostgresDatabase),
    vegas_cache: RedisCache = tanjun.injected(type=RedisCache),
    event_manager: hikari.api.EventManager = tanjun.injected(type=hikari.api.EventManager),
    bot: hikari.GatewayBot = tanjun.inject(type=hikari.GatewayBot),
):
    await edit_settings(
        bot=bot,
        event_manager=event_manager,
        postgres=postgres,
        vegas_cache=vegas_cache,
        ctx=ctx,
        enable_logging=enable_logging,
        logging_channel=logging_channel,
        vc_logging_channel=vc_logging_channel,
        mod_logging_channel=mod_logging_channel,
        enable_auto_delete=enable_auto_delete,
        add_auto_delete_channel=add_auto_delete_channel,
        remove_auto_delete_channel=remove_auto_delete_channel,
    )


async def edit_settings(
    bot: hikari.GatewayBot,
    event_manager: hikari.api.EventManager,
    postgres: PostgresDatabase,
    vegas_cache: RedisCache,
    ctx: SlashContext,
    enable_logging: bool | None = None,
    logging_channel: hikari.InteractionChannel | None = None,
    vc_logging_channel: hikari.InteractionChannel | None = None,
    mod_logging_channel: hikari.InteractionChannel | None = None,
    enable_auto_delete: bool | None = None,
    add_auto_delete_channel: hikari.InteractionChannel | None = None,
    remove_auto_delete_channel: hikari.InteractionChannel | None = None,
):
    """Edit the settings for the current Guild/Server.

    Parameters
    ----------
    enable_logging: bool | None = None
        Whether the bot should log events.
    logging_channel: hikari.InteractionChannel | None = None
        The channel most logging will be sent to.
    vc_logging_channel: hikari.InteractionChannel | None = None
        Logging channel specifically for VC changes.
    mod_logging_channel: hikari.InteractionChannel | None = None
        Logging channel for Moderation actions like Banning.
    enable_auto_delete: bool | None = None
        Whether AutoDelete should be enabled.
    add_auto_delete_channel: hikari.InteractionChannel | None = None
        A channel to add to the AutoDelete list.
    remove_auto_delete_channel: hikari.InteractionChannel | None = None,
        A channel to remove from the AutoDelete list."""
    if not ctx.guild_id:
        return
    local_guild_settings = await LocalGuildSettings.setup(ctx.guild_id, ctx.rest, postgres, vegas_cache)
    if not local_guild_settings:
        await ctx.respond("This guild is not registered.")
        return
    changes = False
    if enable_logging is not None and enable_logging != local_guild_settings.logging_enabled:
        changes = True
        local_guild_settings.logging_enabled = enable_logging
    if logging_channel is not None and logging_channel.id != local_guild_settings.logging_channel:
        changes = True
        local_guild_settings.logging_channel = logging_channel.id
    if vc_logging_channel is not None and vc_logging_channel.id != local_guild_settings.vc_logging_channel:
        changes = True
        local_guild_settings.vc_logging_channel = vc_logging_channel.id
    if mod_logging_channel is not None and mod_logging_channel.id != local_guild_settings.mod_logging_channel:
        changes = True
        local_guild_settings.mod_logging_channel = mod_logging_channel.id
    if enable_auto_delete is not None and enable_auto_delete != local_guild_settings.auto_delete_enabled:
        changes = True
        local_guild_settings.auto_delete_enabled = enable_auto_delete
    if add_auto_delete_channel is not None and add_auto_delete_channel.id not in local_guild_settings.auto_delete_channel_ids:
        changes = True
        local_guild_settings.auto_delete_channel_ids.add(add_auto_delete_channel.id)
    if (
        remove_auto_delete_channel is not None
        and str(remove_auto_delete_channel.id) in local_guild_settings.auto_delete_channel_ids
    ):
        changes = True
        local_guild_settings.auto_delete_channel_ids.remove(remove_auto_delete_channel.id)
    if changes:
        event_manager.dispatch(
            GuildSettingUpdated(
                app=bot,
                author_id=ctx.author.id,
                guild_id=ctx.guild_id,
                channel_id=ctx.channel_id,
                logging_enabled=local_guild_settings.logging_enabled,
                logging_channel=local_guild_settings.logging_channel,
                vc_logging_channel=local_guild_settings.vc_logging_channel,
                mod_logging_channel=local_guild_settings.mod_logging_channel,
                auto_delete_enabled=local_guild_settings.auto_delete_enabled,
                auto_delete_channels_ids=local_guild_settings.auto_delete_channel_ids,
            )
        )
        # await local_guild_settings.save(pg_pool)
        # await vegas_cache.set_guild_settings(local_guild_settings)
        # await vegas_cache.set_auto_deleter_channel_ids(local_guild_settings.guild_id, local_guild_settings.auto_delete_channel_ids)
        await ctx.respond(await local_guild_settings.guild_settings_embed(ctx))
        await ctx.respond("Updated!")
    else:
        await ctx.respond("No settings would be changed.")


@guild_settings.with_command
@with_guild_check()
@with_check(guild_owner_check)
@tanjun.as_slash_command("list", "View Vegas2's settings for this Server.")
async def list_settings_command(
    ctx: SlashContext,
    redis: RedisCache = tanjun.injected(type=RedisCache),
    postgres: PostgresDatabase = tanjun.injected(type=PostgresDatabase),
    vegas_cache: RedisCache = tanjun.injected(type=RedisCache),
):
    await list_settings(ctx, redis, postgres, vegas_cache)


async def list_settings(
    ctx: SlashContext,
    redis: RedisCache = tanjun.injected(type=RedisCache),
    postgres: PostgresDatabase = tanjun.injected(type=PostgresDatabase),
    vegas_cache: RedisCache = tanjun.injected(type=RedisCache),
):
    """List all settings for the current guild."""
    if not ctx.guild_id:
        return
    local_guild_settings = await LocalGuildSettings.setup(ctx.guild_id, ctx.rest, postgres, vegas_cache)
    if not local_guild_settings:
        await ctx.respond("This guild is not registered.")
        return
    embed = await local_guild_settings.guild_settings_embed(ctx)
    await ctx.respond(embed)


loaders = component.make_loader()
