"""Logs important and Moderation related events, as well as simple spam & hate-speech filtering.
"""
from __future__ import annotations

import datetime
import logging

import hikari
import tanjun
from hikari.audit_logs import AuditLogEventType

from ..models.cache.redis import RedisCache
from ..models.database.postgres import PostgresDatabase
from ..models.guild_settings import LocalGuildSettings
from ..util.constants import colors

__all__ = [
    "on_guild_member_voice_change",
    "on_user_account_update",
    "on_guild_member_leave",
    "on_guild_member_update",
    "on_guild_member_ban",
    "on_guild_ban_revoke",
    "on_guild_message_edit",
    "on_guild_message_delete",
    "loaders",
]

component = tanjun.Component()

logger = logging.getLogger()


@component.with_listener(hikari.VoiceStateUpdateEvent)
async def on_guild_member_voice_change(  # noqa: C901
    event: hikari.VoiceStateUpdateEvent,
    database: PostgresDatabase = tanjun.injected(type=PostgresDatabase),
    client: tanjun.Client = tanjun.injected(type=tanjun.Client),
    vegas_cache: RedisCache = tanjun.injected(type=RedisCache),
):
    """Log when a members voice state is changed in the guild."""
    local_guild_settings = await LocalGuildSettings.setup(event.guild_id, event.app.rest, database, vegas_cache)
    if not local_guild_settings.logging_enabled:
        return

    embed = hikari.Embed(color=colors.VOICE)
    embed.set_thumbnail(event.state.member.avatar_url)
    embed.set_footer(text=f"Member ID: {event.state.member.id}")
    embed.add_field(
        name="Guild Deafen:",
        value="☑️" if event.state.is_guild_deafened else "❌",
        inline=True,
    )
    embed.add_field(
        name="Guild Mute:",
        value="☑️" if event.state.is_guild_muted else "❌",
        inline=True,
    )
    embed.add_field(
        name="Self Deafen:",
        value="☑️" if event.state.is_self_deafened else "❌",
        inline=True,
    )
    embed.add_field(name="Self Mute:", value="☑️" if event.state.is_self_muted else "❌", inline=True)
    embed.add_field(name="Streaming:", value="☑️" if event.state.is_streaming else "❌", inline=True)
    embed.add_field(
        name="Video On:",
        value="☑️" if event.state.is_guild_deafened else "❌",
        inline=True,
    )
    embed.add_field(
        name="Suppressed:",
        value="☑️" if event.state.is_guild_deafened else "❌",
        inline=True,
    )

    vc_channel = None
    if event.state.channel_id:
        if client.cache:
            vc_channel = client.cache.get_guild_channel(event.state.channel_id)
        if not vc_channel:
            vc_channel = await client.rest.fetch_channel(event.state.channel_id)
    if not vc_channel and not event.state.channel_id and event.old_state and event.old_state.channel_id:
        if client.cache:
            vc_channel = client.cache.get_guild_channel(event.old_state.channel_id)
        if not vc_channel:
            vc_channel = await client.rest.fetch_channel(event.old_state.channel_id)

    if vc_channel:
        if event.state.channel_id and not event.old_state:
            # If joining VC
            embed.set_author(
                name=f"{event.state.member.username}#{event.state.member.discriminator} joined {vc_channel.name}!",
                icon=event.state.member.avatar_url,
            )
        elif not event.state.channel_id and event.old_state and event.old_state.channel_id:
            # If leaving VC
            embed.set_author(
                name=f"{event.state.member.username}#{event.state.member.discriminator} left {vc_channel.name}!",
                icon=event.state.member.avatar_url,
            )
        elif (
            event.state.channel_id
            and event.old_state
            and event.old_state.channel_id
            and event.old_state.channel_id != event.state.channel_id
        ):
            # If changing VC Channels
            embed.set_author(
                name=f"{event.state.member.username}#{event.state.member.discriminator} switched to {vc_channel.name}!",
                icon=event.state.member.avatar_url,
            )
        else:
            # If changing VC permissions
            action = ""
            if event.old_state:
                if event.state.is_guild_deafened != event.old_state.is_guild_deafened:
                    action += " deafen"
                if event.state.is_guild_muted != event.old_state.is_guild_muted:
                    action += " mute"
                if event.state.is_self_deafened != event.old_state.is_self_deafened:
                    action += " self deafen"
                if event.state.is_self_muted != event.old_state.is_self_muted:
                    action += " self mute"
                if event.state.is_streaming != event.old_state.is_streaming:
                    action += " streaming"
                if event.state.is_video_enabled != event.old_state.is_video_enabled:
                    action += " video"
                if event.state.is_suppressed != event.old_state.is_suppressed:
                    action += " suppressed"
            if action == "":
                action = "idk man"
            embed.set_author(
                name=(
                    f"{event.state.member.username}#{event.state.member.discriminator} "
                    "changed {action.strip()} in {vc_channel.name}!"
                ),
                icon=event.state.member.avatar_url,
            )

        embed.add_field(
            "VC Channel:",
            value=f"{vc_channel.name} (id: {vc_channel.id})",
            inline=False,
        )
    else:
        embed.add_field(name="Error:", value="Can't find VC Channel")
    await event.app.rest.create_message(local_guild_settings.vc_logging_channel, embed=embed)


@component.with_listener(hikari.BanDeleteEvent)
async def on_guild_ban_revoke(
    event: hikari.BanDeleteEvent,
    database: PostgresDatabase = tanjun.injected(type=PostgresDatabase),
    vegas_cache: RedisCache = tanjun.injected(type=RedisCache),
):
    """Log when a user account is banned from the guild."""
    local_guild_settings = await LocalGuildSettings.setup(event.guild_id, event.app.rest, database, vegas_cache)
    if not local_guild_settings.logging_enabled:
        return
    embed = hikari.Embed(color=colors.UNBAN)
    embed.set_author(
        name=f"{event.user.username}#{event.user.discriminator} Ban Revoked!",
        icon=event.user.avatar_url,
    )
    embed.set_thumbnail(event.user.avatar_url)
    embed.set_footer(text=f"user ID: {event.user.id}")

    if local_guild_settings.mod_logging_channel != local_guild_settings.logging_channel:
        await event.app.rest.create_message(local_guild_settings.mod_logging_channel, embed=embed)

    await event.app.rest.create_message(local_guild_settings.logging_channel, embed=embed)


@component.with_listener(hikari.BanCreateEvent)
async def on_guild_member_ban(
    event: hikari.BanCreateEvent,
    database: PostgresDatabase = tanjun.injected(type=PostgresDatabase),
    vegas_cache: RedisCache = tanjun.injected(type=RedisCache),
):
    """Log when a user account is banned from the guild."""
    local_guild_settings = await LocalGuildSettings.setup(event.guild_id, event.app.rest, database, vegas_cache)
    if not local_guild_settings.logging_enabled:
        return
    embed = hikari.Embed(color=colors.BAN)
    embed.set_author(
        name=f"{event.user.username}#{event.user.discriminator} Banned!",
        icon=event.user.avatar_url,
    )
    embed.set_thumbnail(event.user.avatar_url)
    embed.set_footer(text=f"User ID: {event.user.id}")

    audit_log, *_ = await event.app.rest.fetch_audit_log(event.guild_id)
    for log_entry in audit_log.entries:
        entry = audit_log.entries.get(log_entry)
        if entry and entry.action_type == AuditLogEventType.MEMBER_BAN_ADD:
            if entry.reason:
                embed.add_field(name="Ban Reason:", value=entry.reason.split(" -- ")[-1])
            break

    await event.app.rest.create_message(local_guild_settings.logging_channel, embed=embed)
    if local_guild_settings.mod_logging_channel != local_guild_settings.logging_channel:
        await event.app.rest.create_message(local_guild_settings.mod_logging_channel, embed=embed)


@component.with_listener(hikari.OwnUserUpdateEvent)
async def on_user_account_update(
    event: hikari.OwnUserUpdateEvent,
    database: PostgresDatabase = tanjun.injected(type=PostgresDatabase),
    vegas_cache: RedisCache = tanjun.injected(type=RedisCache),
):
    """Log when a user account is updated in the guild."""
    if not event.user:
        return
    async for guild in event.app.rest.fetch_my_guilds():
        try:
            await event.app.rest.fetch_member(guild.id, event.user.id)
            local_guild_settings = await LocalGuildSettings.setup(guild.id, event.app.rest, database, vegas_cache)
            if not local_guild_settings.logging_enabled:
                continue
            embed = hikari.Embed(color=colors.USERNAME)
            embed.set_author(
                name=f"User {event.user.username}#{event.user.discriminator} Updated",
                icon=event.user.avatar_url,
            )
            embed.set_thumbnail(event.user.avatar_url)
            embed.set_footer(text=f"user ID: {event.user.id}")

            if event.old_user:
                if event.user.username != event.old_user.username:
                    embed.add_field(name="Old Username:", value=event.user.username)

                if event.user.discriminator != event.old_user.discriminator:
                    embed.add_field(name="Old Discriminator:", value=event.user.discriminator)

                if event.user.avatar_hash != event.old_user.avatar_hash:
                    embed.add_field(name="Old Avatar Url:", value=f"[Link]({event.user.avatar_url})")

            await event.app.rest.create_message(local_guild_settings.logging_channel, embed=embed)
        except hikari.NotFoundError:
            pass


@component.with_listener(hikari.MemberUpdateEvent)
async def on_guild_member_update(
    event: hikari.MemberUpdateEvent,
    database: PostgresDatabase = tanjun.injected(type=PostgresDatabase),
    bot: hikari.GatewayBotAware = tanjun.injected(type=hikari.GatewayBotAware),
    vegas_cache: RedisCache = tanjun.injected(type=RedisCache),
):
    """Log when a member is updated in the guild."""
    if not event.old_member:
        return
    local_guild_settings = await LocalGuildSettings.setup(event.guild_id, event.app.rest, database, vegas_cache)
    if not local_guild_settings.logging_enabled:
        return
    if not event.member:
        return
    embed = hikari.Embed(color=colors.USERNAME)
    embed.set_author(
        name=f"Member {event.member.display_name}#{event.member.discriminator} Updated",
        icon=event.user.avatar_url,
    )
    embed.set_thumbnail(event.member.avatar_url)
    embed.set_footer(text=f"Member ID: {event.member.id}")

    if event.member.username != event.old_member.username:
        if event.old_member.username is not None:
            embed.add_field(name="Old Username:", value=event.old_member.username, inline=True)
        if event.member.username is not None:
            embed.add_field(name="New Username:", value=event.member.username, inline=True)

    if event.member.discriminator != event.old_member.discriminator:
        if event.old_member.discriminator is not None:
            embed.add_field(
                name="Old Discriminator:",
                value=event.old_member.discriminator,
                inline=True,
            )
        if event.member.discriminator is not None:
            embed.add_field(name="New Discriminator:", value=event.member.discriminator, inline=True)

    if event.member.nickname != event.old_member.nickname:
        if event.old_member.nickname is not None:
            embed.add_field(name="Old Nickname:", value=event.old_member.nickname, inline=True)
        if event.member.nickname is not None:
            embed.add_field(name="New Nickname:", value=event.member.nickname, inline=True)

    if event.member.avatar_hash != event.old_member.avatar_hash:
        if event.old_member.avatar_url is not None:
            embed.add_field(
                name="Old Avatar Url:",
                value=f"[Link]({event.old_member.avatar_url})",
                inline=True,
            )
        if event.member.avatar_url is not None:
            embed.add_field(
                name="New Avatar Url:",
                value=f"[Link]({event.member.avatar_url})",
                inline=True,
            )

    old_roles = set(event.old_member.role_ids)
    current_roles = set(event.member.role_ids)
    if changed_ids := old_roles.difference(current_roles):
        changed_roles = [bot.cache.get_role(role_id).mention for role_id in changed_ids if bot.cache.get_role(role_id)]
        embed.add_field(name="Roles Removed:", value="\n".join(changed_roles))
    elif changed_ids := current_roles.difference(old_roles):
        changed_roles = [bot.cache.get_role(role_id).mention for role_id in changed_ids if bot.cache.get_role(role_id)]
        embed.add_field(name="Roles Added:", value="\n".join(changed_roles))
    if len(embed.fields) == 0:
        return

    await event.app.rest.create_message(local_guild_settings.logging_channel, embed=embed)


@component.with_listener(hikari.MemberDeleteEvent)
async def on_guild_member_leave(
    event: hikari.MemberDeleteEvent,
    database: PostgresDatabase = tanjun.injected(type=PostgresDatabase),
    vegas_cache: RedisCache = tanjun.injected(type=RedisCache),
):
    """Log when a member leaves/is kicked from the guild."""
    if event.old_member is None:
        return
    local_guild_settings = await LocalGuildSettings.setup(event.guild_id, event.app.rest, database, vegas_cache)
    if not local_guild_settings.logging_enabled:
        return
    embed = hikari.Embed(color=colors.LEFT)
    embed.set_author(
        name=f"Member {event.old_member.display_name}#{event.old_member.discriminator} Left",
        icon=event.user.avatar_url,
    )
    embed.set_thumbnail(event.user.avatar_url)
    embed.set_footer(text=f"Member ID: {event.user.id}")

    embed.add_field(name="Username:", value=f"{event.user.username}#{event.user.discriminator}")
    if event.old_member and event.old_member.nickname:
        embed.add_field(name="Nickname:", value=event.old_member.nickname)

    if event.old_member and event.old_member.get_roles():
        role_names = [role.name for role in event.old_member.get_roles()[:-1]]
        if len(role_names) > 0:
            embed.add_field(name="[ Roles ]", value="\n".join(role_names))

    await event.app.rest.create_message(local_guild_settings.logging_channel, embed=embed)


@component.with_listener(hikari.MemberCreateEvent)
async def on_member_join_guild(
    event: hikari.MemberCreateEvent,
    database: PostgresDatabase = tanjun.injected(type=PostgresDatabase),
    vegas_cache: RedisCache = tanjun.injected(type=RedisCache),
):
    """Log when a Member joins the guild."""
    local_guild_settings = await LocalGuildSettings.setup(event.guild_id, event.app.rest, database, vegas_cache)
    if not local_guild_settings.logging_enabled:
        return
    if not event.member:
        return
    relative_account_date = datetime.datetime.now().astimezone() - event.member.created_at
    desc = "**Very New Account** - Created less than a week ago."
    if divmod(relative_account_date.days, 365)[0] >= 1:
        desc = "**Old Account** - Created over a Year ago."
    elif divmod(relative_account_date.days, 31)[0] > 1:
        desc = "**Recent Account** - Created over a Month ago."
    elif divmod(relative_account_date.days, 7)[0] > 1:
        desc = "**New Account** - Created over a Week ago."

    embed = hikari.Embed(description=desc, color=colors.JOIN)
    embed.set_author(
        name=f"{event.member.display_name}#{event.member.discriminator} Joined",
        icon=event.member.avatar_url,
    )
    embed.set_thumbnail(event.member.avatar_url)
    embed.set_footer(text=f"Member ID: {event.member.id}")

    embed.add_field(name="Username:", value=f"{event.member.username}#{event.member.discriminator}")
    embed.add_field(name="Mention:", value=f"{event.member.mention}")
    if event.member.nickname:
        embed.add_field(name="Nickname Set:", value=f"{event.member.nickname}")
    embed.add_field(
        name="Account Created:",
        value=event.member.created_at.strftime("%B %-d, %Y @ %-I:%m %p"),
    )

    await event.app.rest.create_message(local_guild_settings.logging_channel, embed=embed)


@component.with_listener(hikari.GuildMessageUpdateEvent)
async def on_guild_message_edit(
    event: hikari.GuildMessageUpdateEvent,
    database: PostgresDatabase = tanjun.injected(type=PostgresDatabase),
    vegas_cache: RedisCache = tanjun.injected(type=RedisCache),
):
    """Log when a message is edited in the guild."""
    logger.info("Edit Fired")
    local_guild_settings = await LocalGuildSettings.setup(event.guild_id, event.app.rest, database, vegas_cache)
    if not local_guild_settings.logging_enabled:
        logger.info(f"No local guild found for {event.guild_id}")
        return
    if not event.member:
        return
    channel = event.get_channel()
    if not channel:
        channel = await event.app.rest.fetch_channel(event.channel_id)
    embed = hikari.Embed(
        description=(
            f"Message edited in: {channel.mention if channel else '```[ Not Found ]```'}\n\n[Jump to Message]"
            f"({event.message.make_link(event.guild_id)})"
        ),
        color=colors.EDIT,
    )
    embed.set_author(
        name=f"{event.member.display_name}#{event.member.discriminator} Message Edited",
        icon=event.member.avatar_url,
    )
    embed.set_thumbnail(event.member.avatar_url)
    embed.set_footer(text=f"Member ID: {event.member.id}")

    new_content = old_content = "``` [ Not Found ]```"
    if event.old_message:
        if isinstance(event.old_message.content, str):
            old_content = event.old_message.content[:1000]
    if hasattr(event, "content") and isinstance(event.content, str):
        new_content = event.content[:1000]

    embed.add_field(name="Before Edit:", value=old_content)
    embed.add_field(name="After Edit:", value=new_content)

    await event.app.rest.create_message(local_guild_settings.logging_channel, embed=embed)


@component.with_listener(hikari.GuildMessageDeleteEvent)
async def on_guild_message_delete(
    event: hikari.GuildMessageDeleteEvent,
    database: PostgresDatabase = tanjun.injected(type=PostgresDatabase),
    vegas_cache: RedisCache = tanjun.injected(type=RedisCache),
):
    """Log when a message is edited in the guild."""
    local_guild_settings = await LocalGuildSettings.setup(event.guild_id, event.app.rest, database, vegas_cache)
    if not local_guild_settings.logging_enabled:
        return
    channel = event.get_channel()
    if not channel:
        channel = await event.app.rest.fetch_channel(event.channel_id)
    embed = hikari.Embed(
        description=(f"Message(s) deleted in: {channel.mention if channel else '```[ Not Found ]```'}"),
        color=colors.DELETE,
    )

    member_id = username = old_content = "``` [ Not Found ]```"
    avatar_url = None
    if event.old_message:
        if isinstance(event.old_message.content, str):
            old_content = event.old_message.content[:1000]
        username = f"{event.old_message.author.username}#{event.old_message.author.discriminator}"
        avatar_url = event.old_message.author.avatar_url
        member_id = event.old_message.author.id

    if event.guild_id == 353694095220408322 and member_id == 227117043143540736:
        return
    embed.set_author(
        name=f"{username}",
        icon=avatar_url,
    )
    embed.set_thumbnail(avatar_url)
    embed.set_footer(text=f"Member ID: {member_id}")
    embed.add_field(name="Before Edit:", value=old_content)

    await event.app.rest.create_message(local_guild_settings.logging_channel, embed=embed)


loaders = component.make_loader()
