"""Welcome to the V.E.G.A.S.2 Plugins Documentation!!!


IMPORTANT
---------

All documentation under this section is not meant as `code documentation` in the sense
that it is not meant to explain the code or how it works. `vegas2.plugins` generally does
not contain any logic, code or interesting features outside of permission management. All
logic, data storage, serialization and other issues are handled in `vegas2.models` or
`vegas2.util.dependencies`.

`vegas2.plugins` is instead meant to act as documentation for how the provided Slash Commands
and Listeners (or automatc features) work for an end user or Discord Mod/Admin. With that in mind
a few notable changes:

- Parameters listed are not what the function takes, but what the slash command expects in Discord.
"""
__all__ = [
    "archive",
    "archive_grp",
    "archive_channel",
    "debug_module",
    "debug",
    "clear",
    "about_bot",
    "redis",
    "execute_redis_command",
    "load_module",
    "unload_module",
    "reload_module",
    "embeds",
    "embed",
    "interactive_post",
    "interactive_edit",
    "event_logger",
    "on_guild_member_voice_change",
    "on_user_account_update",
    "on_guild_member_leave",
    "on_guild_member_update",
    "on_guild_member_ban",
    "on_guild_ban_revoke",
    "on_guild_message_edit",
    "on_guild_message_delete",
    "guild_settings",
    "edit_settings",
    "list_settings",
    "messages",
    "messages_grp",
    "send_message",
    "moderation",
    "watch_for_everyone_ping",
    "watch_for_invites",
    "watch_for_spam",
    "user",
    "strike",
    "remove_strike",
    "warn",
    "remove_warn",
    "kick",
    "ban",
    "question_of_the_day",
    "qotd",
    "watch_for_answer_of_the_day",
    "qotd_post",
    "qotd_list",
    "qotd_add",
    "qotd_remove",
    "roles",
    "add_role_from_role_name_or_emoji",
    "watch_channel_embed_for_emoji",
    "autoroles_list",
    "autoroles_add",
    "autoroles_remove",
    "tickets",
    "startup_tickets",
    "create_new_ticket_button",
    "add_member_to_ticket",
    "add_member_ticket_button",
    "remove_member_from_ticket",
    "remove_member_ticket_button",
    "close_ticket_button",
    "delete_ticket",
    "timeout_ticket",
    "welcome",
    "welcome_member_message",
    "welcome_member_dm",
]
from . import archive
from . import debug as debug_module
from . import (
    embeds,
    event_logger,
    guild_settings,
    messages,
    moderation,
    question_of_the_day,
    roles,
    tickets,
    welcome,
)
from .archive import archive_channel, archive_grp
from .debug import (
    about_bot,
    clear,
    debug,
    execute_redis_command,
    load_module,
    redis,
    reload_module,
    unload_module,
)
from .embeds import embed, interactive_edit, interactive_post
from .event_logger import (
    on_guild_ban_revoke,
    on_guild_member_ban,
    on_guild_member_leave,
    on_guild_member_update,
    on_guild_member_voice_change,
    on_guild_message_delete,
    on_guild_message_edit,
    on_user_account_update,
)
from .guild_settings import edit_settings, list_settings
from .messages import messages_grp, send_message
from .moderation import (
    ban,
    kick,
    remove_strike,
    remove_warn,
    strike,
    user,
    warn,
    watch_for_everyone_ping,
    watch_for_invites,
    watch_for_spam,
)
from .question_of_the_day import (
    qotd,
    qotd_add,
    qotd_list,
    qotd_post,
    qotd_remove,
    watch_for_answer_of_the_day,
)
from .roles import (
    add_role_from_role_name_or_emoji,
    autoroles_add,
    autoroles_list,
    autoroles_remove,
    watch_channel_embed_for_emoji,
)
from .tickets import (
    add_member_ticket_button,
    add_member_to_ticket,
    close_ticket_button,
    create_new_ticket_button,
    delete_ticket,
    remove_member_from_ticket,
    remove_member_ticket_button,
    startup_tickets,
    timeout_ticket,
)
from .welcome import welcome_member_dm, welcome_member_message
