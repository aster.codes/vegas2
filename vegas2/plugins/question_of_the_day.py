"""Provides a simple daily event for Servers!

All listed commands are available under the `/qotd`.

Example: qotd_list is `/qotd list`, add is `/qotd add`, etc.
"""
from __future__ import annotations

import datetime
import math
import random

import hikari
import tanjun
import yuyo
from tanjun.abc import SlashContext

from ..checks.role_checks import with_any_role_check
from ..models.database.postgres import PostgresDatabase
from ..util.constants.moderation import STAFF_ROLES
from ..util.constants.qotd import COLOURS, EMBED_DESC, EMBED_ICON
from ..util.helpers import get_vegas_logger

__all__ = ["qotd", "watch_for_answer_of_the_day", "qotd_post", "qotd_list", "qotd_add", "qotd_remove", "loader"]

logger = get_vegas_logger(__name__)

component = tanjun.Component()

qotd = component.with_slash_command(tanjun.slash_command_group("qotd", "The Question of the Day commands."))
"""The command group for Question of the Day.
All Slash commands in this file are grouped under this CommandGroup. This results in all commands
being prefixed with `/qotd <command>`."""


@component.with_listener(hikari.GuildReactionAddEvent)
async def watch_for_answer_of_the_day(
    event: hikari.GuildReactionAddEvent,
    postgres: PostgresDatabase = tanjun.injected(type=PostgresDatabase),
    cache: hikari.api.Cache = tanjun.injected(type=hikari.api.Cache),
):
    """Automatically adds any message reacted to with ❔, by a staff role member, to the Answer of the Day in #question-of-the-day.

    No commands."""
    channel = cache.get_guild_channel(event.channel_id)
    if not channel:
        channel = await event.app.rest.fetch_channel(event.channel_id)
    message = cache.get_message(event.message_id)
    if not message:
        message = await event.app.rest.fetch_message(event.channel_id, event.message_id)

    if not channel.name.startswith("question-of-the-day") or str(event.emoji_name) != "❔":
        return

    if not any(staff_role.name in STAFF_ROLES for staff_role in event.member.get_roles()):
        return

    question = await postgres.fetch_qotd_for_guild(
        event.guild_id, date_posted=datetime.datetime.today().astimezone(), archived=True
    )
    if not question:
        logger.error(f"No question found for {event.guild_id}")
        return

    await postgres.save_aotd_answer(event.guild_id, question["id"], message)

    await channel.send(f"Added answer from {message.author.mention} for Answer of the Day!")
    pins = await channel.fetch_pins()
    if embeds := pins[0].embeds:
        await pins[0].edit(
            embed=embeds[0].add_field(
                name=f"Answer by {message.author.username}",
                value=message.content,
                inline=False,
            )
        )


@with_any_role_check(STAFF_ROLES)
@qotd.with_command
@tanjun.with_int_slash_option("to_return", "The number of Questions to return.", default=25)
@tanjun.with_bool_slash_option("show_archived", "Show already posted and archived questions.", default=False)
@tanjun.as_slash_command("list", "List open Question's")
async def list_command(
    ctx: SlashContext,
    to_return: int = 250,
    show_archived: bool = False,
    postgres: PostgresDatabase = tanjun.injected(type=PostgresDatabase),
    component_client: yuyo.ComponentClient = tanjun.injected(type=yuyo.ComponentClient),
):
    await qotd_list(ctx, to_return, show_archived, postgres, component_client)


async def qotd_list(
    ctx: SlashContext,
    to_return: int = 250,
    show_archived: bool = False,
    postgres: PostgresDatabase = tanjun.injected(type=PostgresDatabase),
    component_client: yuyo.ComponentClient = tanjun.injected(type=yuyo.ComponentClient),
):
    """Generate an interactive paginated embed of Questions for the calling Guild.

     Command: ```/qotd list```

    Parameters
    ----------
    to_return: int = 250
        The maximum amount of Questions to return. 250 by default.
    show_archived: bool = False
        Show used/archived Questions. False by default."""
    if not ctx.guild_id:
        return
    divider = 25

    questions = await postgres.fetch_qotds_for_guild(ctx.guild_id, show_archived=show_archived, limit=to_return)

    if not questions:
        await ctx.respond("No questions!")
        return

    question_pages = math.ceil(len(questions) / divider)
    embeds = [
        hikari.Embed(
            title="Question's for the Days!",
            description="Below are the currently open Questions!",
        ).set_thumbnail(EMBED_ICON)
        for _ in range(question_pages)
    ]

    questions_embeds = (
        (
            f"Page {page + 1} of {question_pages}",
            [
                embeds[page].add_field(
                    name=f"Question from {question['nickname']} added on {question['date_added'].strftime('%m-%d-%Y')}",
                    value=("*Archived:* ✅\n" if question["archived"] else "")
                    + f"*ID*: {question['id']}\n*To be posted:* {question['date_posted'].strftime('%m-%d-%Y')}\n{question['question'][:190]}...\n"
                    + "-" * 15,
                    inline=False,
                )
                for question in questions[page * divider : (page + 1) * divider]
            ][-1],
        )
        for page in range(question_pages)
    )
    paginator = yuyo.ComponentPaginator(questions_embeds, authors=(ctx.author,))
    if first_response := await paginator.get_next_entry():
        content, embed = first_response
        message = await ctx.respond(content=content, component=paginator, embed=embed, ensure_result=True)
        component_client.set_executor(message, paginator)
        return
    await ctx.respond("No Questions in the system!")


@with_any_role_check(STAFF_ROLES)
@qotd.with_command
@tanjun.with_str_slash_option("question", "The question to post.")
@tanjun.with_member_slash_option("author", "The author of the question.")
@tanjun.with_str_slash_option(
    "date_to_post",
    "The date [in mm-dd-yyyy format] you would like this question posted.",
    converters=lambda date: datetime.datetime.strptime(date, "%m-%d-%Y"),
)
@tanjun.as_slash_command("add", "Add a Question")
async def add_command(
    ctx: SlashContext,
    question: str,
    author: hikari.InteractionMember,
    date_to_post: datetime.datetime,
    postgres: PostgresDatabase = tanjun.inject(type=PostgresDatabase),
):
    await qotd_add(ctx, question, author, date_to_post, postgres)


async def qotd_add(
    ctx: SlashContext,
    question: str,
    author: hikari.InteractionMember,
    date_to_post: datetime.datetime,
    postgres: PostgresDatabase,
):
    """Adds a question to be used for Question of the Day.

    Command: ```/qotd add```

    Parameters
    ----------
    question: str
        The question to be added.
    author: hikari.InteractionMember
        The Guild Member the question is from.
    date_to_post: str
        A preset date to post the question. Optional."""
    if not ctx.guild_id:
        return

    await postgres.save_qotd_question(ctx.guild_id, question, author, date_to_post)
    await ctx.respond("Thanks for the question!")


@with_any_role_check(STAFF_ROLES)
@qotd.with_command
@tanjun.with_int_slash_option("question_id", "The Question's ID you want to Delete")
@tanjun.as_slash_command("remove", "Remove a Question")
async def remove_command(ctx: SlashContext, question_id: int, postgres: PostgresDatabase = tanjun.inject(type=PostgresDatabase)):
    await qotd_remove(ctx, question_id, postgres)


async def qotd_remove(ctx: SlashContext, question_id: int, postgres: PostgresDatabase):
    """Removes a question from the calling Guilds Question of the Day list.

    Command: ```/qotd remove```

    Parameters
    ----------
    question_id: int
        The ID of the question to remove."""
    if not ctx.guild_id:
        return
    await postgres.delete_qotd_question(ctx.guild_id, question_id)
    await ctx.respond(f"Deleted question {question_id}!")


@with_any_role_check(STAFF_ROLES)
@qotd.with_command
@tanjun.with_bool_slash_option("post_todays", "Post the Question marked for today.", default=False)
@tanjun.with_int_slash_option("question_id", "The Question's ID you want to post", default=None)
@tanjun.with_channel_slash_option("post_channel", "The channel to send the Question to.", default=None)
@tanjun.as_slash_command("post", "Post the Question")
async def post_command(
    ctx: SlashContext,
    question_id: int | None,
    post_channel: hikari.InteractionChannel | None = None,
    post_todays: bool = False,
    postgres: PostgresDatabase = tanjun.inject(type=PostgresDatabase),
):
    await qotd_post(postgres, ctx, question_id, post_channel, post_todays)


async def qotd_post(
    postgres: PostgresDatabase,
    ctx: SlashContext,
    question_id: int | None,
    post_channel: hikari.InteractionChannel | None = None,
    post_todays: bool = False,
):
    """Post a Question from the calling Guilds Question of the Day list.

    Command: ```/qotd post```

    Parameters
    ----------
    question_id: int
        A Questions ID to look and post. Optional.
    post_todays: bool = False
        If True, the list will be searched for a question marked for today.
    post_channel: hikari.InteractionChannel | None = None
        The channel to post the question in."""
    if not ctx.cache or not ctx.guild_id:
        return

    date_posted = None
    if post_todays:
        date_posted = datetime.datetime.today().astimezone()

    question = await postgres.fetch_qotd_for_guild(ctx.guild_id, question_id=question_id, date_posted=date_posted)

    if not question:
        await ctx.respond("No questions?")
        return

    embed = hikari.Embed(
        title="Question of the Day!",
        description=EMBED_DESC,
        colour=random.choice(COLOURS),  # noqa: S311
    )
    embed.add_field(name="Question", value=question["question"], inline=False)
    embed.add_field(name="Author", value=question["nickname"], inline=True)
    embed.add_field(name="Added", value=question["date_added"].strftime("%m-%d-%Y"), inline=True)
    embed.set_author(name=question["nickname"], icon=question["avatar_url"])
    embed.set_thumbnail(EMBED_ICON)

    to_channel = ctx.cache.get_guild_channel(post_channel.id) if post_channel else ctx.get_channel()
    to_channel = await ctx.client.rest.fetch_channel(post_channel.id if post_channel else ctx.channel_id)
    qotd_ping = None
    guild_roles = ctx.cache.get_roles_view_for_guild(ctx.guild_id)
    for role in guild_roles:
        role_obj = guild_roles.get(role)
        if role_obj and role_obj.name == "Question of the Day Notification":
            qotd_ping = role_obj
            await ctx.rest.edit_role(ctx.guild_id, qotd_ping.id, mentionable=True)

    qotd_message = await to_channel.send(
        content=qotd_ping.mention if qotd_ping else hikari.UNDEFINED,
        embed=embed,
        role_mentions=True,
    )

    if qotd_ping:
        await ctx.rest.edit_role(ctx.guild_id, qotd_ping.id, mentionable=False)

    await to_channel.pin_message(qotd_message)
    await postgres.archive_qotd_question(ctx.guild_id, question["id"])
    await ctx.respond("Question posted and pinned!")


loader = component.make_loader()
"""Default tanjun loader."""
