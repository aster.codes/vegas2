"""Provides basic moderation and logging functionality.

> TODO
>
> - Nickname check for bad words to moderation.py
> - Nickname check for suspicious words in moderation.py
> - Nickname check for real names in moderation.py
> - Nickname check for a pingable nickname in moderation.py
"""
from __future__ import annotations

import asyncio
import datetime
import typing

import hikari
import pytz
import tanjun
from hikari.components import ButtonStyle
from tanjun.abc import SlashContext

from ..checks.role_checks import with_any_role_check
from ..models.cache.redis import RedisCache
from ..models.database.postgres import PostgresDatabase
from ..util.constants import colors
from ..util.constants.moderation import (
    EVERYONE_PATTERN,
    HERE_PATTERN,
    INVITE_PATTERN,
    REDIS_SPAM_KEY,
    STAFF_ROLES,
)

DATE_FORMAT = "%m/%d/%Y %I:%M %p %Z"

CONFIRM = "☑️"

__all__ = [
    "watch_for_everyone_ping",
    "watch_for_invites",
    "watch_for_spam",
    "user",
    "strike",
    "remove_strike",
    "warn",
    "remove_warn",
    "kick",
    "ban",
    "loader",
]

component = tanjun.Component()


@component.with_listener(hikari.GuildMessageCreateEvent)
async def watch_for_everyone_ping(
    event: hikari.GuildMessageCreateEvent,
    bot: hikari.GatewayBot = tanjun.injected(type=hikari.GatewayBot),
    db_pool: PostgresDatabase = tanjun.injected(type=PostgresDatabase),
    cache: hikari.api.Cache = tanjun.injected(type=hikari.api.Cache),
):
    """Automatically handles when a member tries to ping @everyone or @here.
    The flagged message will be deleted and the author warned. Members with @Mods are ignored.

    Parameters
    ----------
    event: hkari.GuildMessageCreateEvent
        The Message event to check.
    bot: hikari.GatewayBot
        The bot this event is coming through. Provided by DI.
    db_pool: aiopg.Pool
        The database connection to use for recording infractions.
    """
    if event.is_bot or event.content is None or not event.member:
        return
    member_roles = event.member.get_roles()
    if not member_roles:
        member_roles = await event.member.fetch_roles()

    for role in member_roles:
        """Allow Mods"""
        if role.name == "Mods":
            return

    if EVERYONE_PATTERN.search(event.content.lower()) or HERE_PATTERN.search(event.content.lower()):
        await event.message.delete()
        guild = event.get_guild()
        if not guild:
            guild = await event.app.rest.fetch_guild(event.guild_id)
        own_bot: hikari.OwnUser = cache.get_me()
        member_bot = guild.get_member(own_bot)
        if not member_bot:
            member_bot = await guild.app.rest.fetch_member(event.guild_id, own_bot)
        channel = event.get_channel()
        if not channel:
            channel = await event.app.rest.fetch_channel(event.channel_id)

        warn_message = await warn(
            event.member,
            channel,
            "Warned for trying to ping `everyone` or `here`.",
            guild,
            db_pool,
        )
        await channel.send(
            f"{event.author.mention} Only Mods+ can ping `@everyone` or `@here`.",
            user_mentions=True,
        )
        await asyncio.sleep(15)
        await warn_message.delete()


@component.with_listener(hikari.GuildMessageCreateEvent)
async def watch_for_invites(
    event: hikari.GuildMessageCreateEvent,
    bot: hikari.GatewayBot = tanjun.injected(type=hikari.GatewayBot),
    db_pool: PostgresDatabase = tanjun.injected(type=PostgresDatabase),
):
    """Automatically handles regular members sending a Discord Invite (discord.gg).

    The message will be deleted and the Member will be warned. Members with @Mods are ignored.

    Parameters
    ----------
    event: hkari.GuildMessageCreateEvent
        The Message event to check.
    bot: hikari.GatewayBot
        The bot this event is coming through. Provided by DI.
    db_pool: aiopg.Pool
        The database connection to use for recording infractions.
    """
    if event.is_bot or event.content is None or not event.member:
        return
    member_roles = event.member.get_roles()
    if not member_roles:
        member_roles = await event.member.fetch_roles()

    for role in member_roles:
        """Allow Mods"""
        if role.name == "Mods":
            return

    if INVITE_PATTERN.search(event.content.lower()):
        await event.message.delete()
        guild = event.get_guild()
        if not guild:
            guild = await event.app.rest.fetch_guild(event.guild_id)
        own_bot: hikari.OwnUser = bot.get_me()
        member_bot = guild.get_member(own_bot)
        if not member_bot:
            member_bot = await guild.app.rest.fetch_member(event.guild_id, own_bot)
        channel = event.get_channel()
        if not channel:
            channel = await event.app.rest.fetch_channel(event.channel_id)

        warn_message = await warn(
            event.member,
            channel,
            "Warned for posting Discord Invites.",
            guild,
            db_pool,
        )
        await event.get_channel().send(f"{event.author.mention} we do not allow discord invites, sorry.")
        await asyncio.sleep(15)
        await warn_message.delete()


@component.with_listener(hikari.GuildMessageCreateEvent)
async def watch_for_spam(
    event: hikari.GuildMessageCreateEvent,
    redis_client: RedisCache = tanjun.injected(type=RedisCache),
    bot: hikari.GatewayBot = tanjun.injected(type=hikari.GatewayBot),
    db_pool: PostgresDatabase = tanjun.injected(type=PostgresDatabase),
):
    """Attempts to automatically handle spam.

    This function scans every message the bot can see.

    Upon receiving a message the bot records the content in lowercase with the Members ID,
    for 5 minutes.

    Upon receiving a second message from the Member, if the message is identical, the bot
    will delete the message and respond telling the Member not to spam.

    If a third message with identical content is found the member will be kicked.

    TODO:
    - Could modify this to use a HashMap in redis instead. We could store it as:
    ```unique_key = {channel_id:message_id: content, channel_id:message_id: content, ...}```
    This would allow us to lookup messages marked as spam to delete.

    Parameters
    ----------
    event: hkari.GuildMessageCreateEvent
        The Message event to check.
    bot: hikari.GatewayBot
        The bot this event is coming through. Provided by DI.
    db_pool: aiopg.Pool
        The database connection to use for recording infractions.
    """
    own_bot: hikari.OwnUser = bot.get_me()
    if event.is_bot or event.content is None or not own_bot:
        return

    guild = event.get_guild()
    if not guild:
        guild = await event.app.rest.fetch_guild(event.guild_id)
    member_bot = guild.get_member(own_bot)
    if not member_bot:
        member_bot = await guild.app.rest.fetch_member(event.guild_id, own_bot)
    channel = event.get_channel()
    if not channel:
        channel = await event.app.rest.fetch_channel(event.channel_id)

    unique_key = REDIS_SPAM_KEY.format(guild_id=event.guild_id, member_id=event.author.id)
    last_message_content = await redis_client._redis_client.lrange(unique_key, 0, 4)
    if last_message_content and event.content.lower() not in last_message_content[-5:]:
        await redis_client._redis_client.delete(unique_key)
    await redis_client._redis_client.rpush(unique_key, event.content.lower())
    await redis_client._redis_client.expire(unique_key, datetime.timedelta(minutes=10))
    new_spam_messages_count = await redis_client._redis_client.llen(unique_key)
    kick_message = warn_message = None
    if new_spam_messages_count >= 3:
        await event.message.delete()
        warn_message = await warn(
            event.member,
            channel,
            "Warned for spamming.",
            guild,
            db_pool,
        )
    if new_spam_messages_count >= 5:
        kick_message = await kick(
            member_bot,
            event.member,
            guild,
            channel,
            channel,
            "Kicked for spamming.",
            db_pool,
        )
        for _ in range(new_spam_messages_count - 5):
            await redis_client._redis_client.lpop(unique_key)
    await asyncio.sleep(15)
    if warn_message:
        await warn_message.delete()
    if kick_message:
        await kick_message.delete()


@with_any_role_check(STAFF_ROLES)
@component.with_command
@tanjun.with_int_slash_option("ban_number", "The # of bans to display.", default=2)
@tanjun.with_int_slash_option("kick_number", "The # of kicks to display.", default=3)
@tanjun.with_int_slash_option("strike_number", "The # of strikes to display.", default=5)
@tanjun.with_int_slash_option("warn_number", "The # of warns to display.", default=5)
@tanjun.with_member_slash_option("member", "Member to warn.")
@tanjun.as_slash_command("user", "List in server information about a Member")
async def user_command(
    ctx: SlashContext,
    member: hikari.InteractionMember,
    warn_number: int = 5,
    strike_number: int = 5,
    kick_number: int = 3,
    ban_number: int = 2,
    postgres: PostgresDatabase = tanjun.injected(type=PostgresDatabase),
):
    await user(ctx, member, warn_number, strike_number, kick_number, ban_number, postgres)


async def user(
    ctx: SlashContext,
    member: hikari.InteractionMember,
    warn_number: int = 5,
    strike_number: int = 5,
    kick_number: int = 3,
    ban_number: int = 2,
    postgres: PostgresDatabase = tanjun.injected(type=PostgresDatabase),
):
    """Displays an embed about the provided Members warns, strikes, kicks and bans for the Guild called in.

    Displays:

    - Total # of warns/strikes/kicks/bans
    - Displays last # of warns/strikes/kicks/bans
        - # is according to optional arguments
    - Displays member.id and nickname

    Parameters
    ----------
    member: hikari.InteractionMember
        The member view information on.
    warn_number: int = 5
        The maximum number of warns to return.
    strike_number: int = 5
        The maximum number of strikes to return.
    kick_number: int = 3
        The maximum number of kicks to return.
    ban_number: int = 2
        The maximum number of bans to return."""

    now = datetime.datetime.now().astimezone()

    strike_results, warn_results, kick_results, ban_results = await postgres.fetch_user_log(
        ctx.guild_id, member.id, strike_number, warn_number, kick_number, ban_number
    )

    embed = hikari.Embed(
        title=f"{member.username} information",
        timestamp=now,
        color=member.get_top_role().color,
    )
    embed.set_thumbnail(member.avatar_url)

    embed.add_field(name="ID:", value=str(member.id), inline=True)

    if member.nickname:
        embed.add_field(name="Nickname:", value=member.nickname, inline=True)

    embed.add_field(name="Total Warns:", value=str(len(warn_results)), inline=True)
    embed.add_field(name="Total Strikes:", value=str(len(strike_results)), inline=True)
    embed.add_field(name="Total Kicks:", value=str(len(kick_results)), inline=True)
    embed.add_field(name="Total Bans:", value=str(len(ban_results)), inline=True)
    embed = await paginate_to_embed("Warns:", embed, warn_results)
    embed = await paginate_to_embed("Strikes:", embed, strike_results)
    embed = await paginate_to_embed("Kicks:", embed, kick_results)
    embed = await paginate_to_embed("Bans:", embed, ban_results)

    await ctx.respond(embed=embed, user_mentions=True)
    await ctx.respond("✅")


@with_any_role_check(STAFF_ROLES)
@component.with_command
@tanjun.with_channel_slash_option("echo_channel", "Send this warn to an alternative channel.", default=None)
@tanjun.with_str_slash_option("reason", "Reason the user has been warned.")
@tanjun.with_role_slash_option("strike_role", "Role to strike against.")
@tanjun.with_member_slash_option("member", "Member to warn.")
@tanjun.as_slash_command("strike", "Strike a Member")
async def strike_command(
    ctx: SlashContext,
    member: hikari.InteractionMember,
    strike_role: hikari.Role,
    reason: str,
    echo_channel: hikari.InteractionChannel,
    postgres: PostgresDatabase = tanjun.injected(type=PostgresDatabase),
):
    await strike(ctx, member, strike_role, echo_channel, reason, postgres)


async def strike(
    ctx: SlashContext,
    member: hikari.InteractionMember,
    strike_role: hikari.Role,
    echo_channel: hikari.InteractionChannel,
    reason: str,
    postgres: PostgresDatabase = tanjun.injected(type=PostgresDatabase),
):
    """Strikes a Member against a provided Role.

    Strikes a functionally identical to a warn but also accepts a Role to help easily organize.

    Parameters
    ----------
    member: hikari.InteractionMember
        The member to target for this strke.
    strike_role: hikari.Role
        The role to associate with this strike.
    echo_channel: hikari.InteractionChannel
        If proivded, the bot will send this strike message to the echo_channel.
    reason: str
        The reason this strike was applied.
    """
    if not ctx.member or not ctx.guild_id:
        return
    if member.id == ctx.author.id:
        await ctx.respond("You cannot strike yourself.")
        return
    member_top_role = member.get_top_role()
    author_top_role = ctx.member.get_top_role()
    if not member_top_role or not author_top_role:
        return
    if member_top_role.position >= author_top_role.position:
        await ctx.respond("You cannot strike someone with the same or higher role.")
        return
    if not any(
        strike_role.name.lower().startswith(check) or strike_role.name.lower().endswith(check)
        for check in ["no ", "notification"]
    ):
        await ctx.respond("That is not a valid strike role!")
        return

    await ctx.defer()

    output_channel = ctx.get_channel()
    if not output_channel:
        output_channel = await ctx.rest.fetch_channel(ctx.channel_id)
    if echo_channel:
        output_channel = await ctx.client.rest.fetch_channel(echo_channel.id)
    now = datetime.datetime.now().astimezone()

    await postgres.save_strike_member(ctx.guild_id, strike_role, member, reason, now)

    embed = hikari.Embed(
        title=f"{member.username} has been struck for {strike_role.name}",
        timestamp=now,
        color=member_top_role.color,
    )
    embed.add_field(name="Strike Reason", value=reason)
    embed.set_thumbnail(ctx.get_guild().icon_url)

    await output_channel.send(content=f"{member.mention}", embed=embed, user_mentions=True)
    await ctx.respond(CONFIRM)


@with_any_role_check(STAFF_ROLES)
@component.with_command
@tanjun.with_member_slash_option("member", "Member to strike")
@tanjun.as_slash_command("remove_strike", "Remove a strike from a Member")
async def remove_strike_command(
    ctx: SlashContext,
    member: hikari.InteractionMember,
    postgres: PostgresDatabase = tanjun.inject(type=PostgresDatabase),
    bot: hikari.GatewayBot = tanjun.injected(type=hikari.GatewayBot),
):
    await remove_strike(ctx, member, postgres, bot)


async def remove_strike(ctx: SlashContext, member: hikari.InteractionMember, postgres: PostgresDatabase, bot: hikari.GatewayBot):
    """Remove a strike from a member in the calling Guild.

    The bot will display a list of the Members Strikes and ask the caller to choose an option.

    Parameters
    ----------
    member: hikari.InteractionMember
        The member to remove a strike from."""
    est = pytz.timezone("US/Eastern")
    now = datetime.datetime.now().astimezone()
    menu = []
    strike_results, _, _, _ = await postgres.fetch_user_log(ctx.guild_id, 25, 0, 0, 0)
    embed = hikari.Embed(
        title=f"{member.username}#{member.discriminator} last 25 warns.",
        color=member.get_top_role().color,
        timestamp=now,
    )
    embed.set_thumbnail(member.avatar_url)

    action_row = ctx.rest.build_message_action_row()
    for count, strike in enumerate(strike_results):
        if len(strike_results) - 1 == count:
            menu.append(action_row)
        elif count % 5:
            menu.append(action_row)
            action_row = ctx.rest.build_message_action_row()

        (
            action_row.add_interactive_button(ButtonStyle.SECONDARY, f"remove_strike_{strike['id']}")
            .set_label(f"{strike['id']} @ {strike['date_recieved'].astimezone(est).strftime(DATE_FORMAT)}")
            .add_to_container()
        )
        embed.add_field(
            name=f"ID: {strike['id']} @ {strike['date_recieved'].astimezone(est).strftime(DATE_FORMAT)}",
            value=strike["reason"],
            inline=False,
        )

    await ctx.respond(embed=embed, components=menu)

    if len(strike_results) == 0:
        await ctx.edit_initial_response(content="No strikes to remove :o")

    await ctx.edit_initial_response("What strike would you like to remove?")

    try:
        with bot.stream(hikari.InteractionCreateEvent, timeout=60).filter(
            lambda event: event.interaction.user.id == ctx.author.id
            and isinstance(event.interaction, hikari.ComponentInteraction)
            and event.interaction.custom_id.startswith("remove_strike_")
        ) as stream:
            async for event in stream:
                id_to_del = int(event.interaction.custom_id.split("_")[-1])
                await ctx.edit_initial_response(f"Attempting to delete {id_to_del}", components=[])
                await postgres.delete_strike_member(id_to_del)
                await ctx.edit_initial_response(f"Removed strike {id_to_del}")
    except asyncio.TimeoutError:
        await ctx.edit_initial_response("No response :(")


@with_any_role_check(
    [
        "Can Warn",
    ]
)
@component.with_command
@tanjun.with_channel_slash_option("echo_channel", "Send this warn to an alternative channel.", default=None)
@tanjun.with_str_slash_option("reason", "Reason the user has been warned.")
@tanjun.with_member_slash_option("member", "Member to warn")
@tanjun.as_slash_command("warn", "Warn a Member")
async def warn_command(
    ctx: SlashContext,
    member: hikari.InteractionMember,
    reason: str,
    echo_channel: hikari.InteractionChannel,
    postgres: PostgresDatabase = tanjun.injected(type=PostgresDatabase),
):
    if not ctx.member:
        return

    if member.get_top_role().position >= ctx.member.get_top_role().position:
        return await ctx.respond("You cannot warn someone with the same or higher role.")

    if member.id == ctx.author.id:
        return await ctx.respond("You cannot warn yourself.")

    if echo_channel:
        real_echo_channel = await ctx.client.rest.fetch_channel(echo_channel.id)
    else:
        real_echo_channel = ctx.get_channel()
        if not real_echo_channel:
            real_echo_channel = await ctx.fetch_channel()

    guild = ctx.get_guild()
    if not guild:
        guild = await ctx.fetch_guild()
    if not guild:
        return

    _ = await warn(
        member,
        real_echo_channel,
        reason,
        guild,
        postgres,
    )
    await ctx.respond(CONFIRM)


async def warn(
    member: hikari.InteractionMember | hikari.Member,
    echo_channel: hikari.TextableGuildChannel | hikari.PartialChannel,
    reason: str,
    guild: hikari.Guild,
    postgres: PostgresDatabase = tanjun.injected(type=PostgresDatabase),
) -> hikari.Message:
    """Warn a member.

    Stores the provided reason in a database with the associated member and guild.

    If echo_channel is provided, the confirmation will send to that channel.

    Parameters
    ----------
    member: hikari.InteractionMember | hikari.Member
        The member to warn.
    echo_channel: hikari.TextableGuildChannel | hikari.PartialChannel
        If provided, the confirmation embed will be sent to this channel. If None, confirmation sends to called channel.
    """
    now = datetime.datetime.now().astimezone()

    await postgres.save_warn_member(guild.id, member, reason, now)

    embed = hikari.Embed(
        title=f"{member.username} has been warned",
        timestamp=now,
        color=member.get_top_role().color,
    )
    embed.add_field(name="Warn Reason", value=reason)
    embed.set_thumbnail(guild.icon_url)

    return await echo_channel.send(content=f"{member.mention}", embed=embed, user_mentions=True)


@with_any_role_check(
    [
        "Can Warn",
    ]
)
@component.with_command
@tanjun.with_member_slash_option("member", "Member to warn")
@tanjun.as_slash_command("remove_warn", "Remove a warn from a Member")
async def remove_warn_command(
    ctx: SlashContext,
    member: hikari.InteractionMember,
    postgres: PostgresDatabase = tanjun.injected(type=PostgresDatabase),
    bot: hikari.GatewayBot = tanjun.injected(type=hikari.GatewayBot),
):
    await remove_warn(ctx, member, postgres, bot)


async def remove_warn(
    ctx: SlashContext,
    member: hikari.InteractionMember,
    postgres: PostgresDatabase,
    bot: hikari.GatewayBot,
):
    """Interactively removes a warn from a Member.

    The bot will display all warns, in a numbered list, associated with this member in the calling guild.

    The bot will then prompt the caller to select a number which will be removed from the member.

    TODO
    - Remove Warn/Strike could be optimized and combined.

    Parameters
    ----------
    member: hikari.InteractionMember
        The member you want to remove a warn from."""
    est = pytz.timezone("US/Eastern")
    now = datetime.datetime.now().astimezone()
    menu = []

    _, warn_results, _, _ = await postgres.fetch_user_log(ctx.guild_id, 0, 25, 0, 0)

    embed = hikari.Embed(
        title=f"{member.username}#{member.discriminator} last 25 warns.",
        color=member.get_top_role().color,
        timestamp=now,
    )
    embed.set_thumbnail(member.avatar_url)

    action_row = ctx.rest.build_message_action_row()
    for count, warn in enumerate(warn_results):
        if len(warn_results) - 1 == count:
            menu.append(action_row)
        elif count % 5:
            menu.append(action_row)
            action_row = ctx.rest.build_message_action_row()

        (
            action_row.add_interactive_button(ButtonStyle.SECONDARY, f"remove_warn_{warn['id']}")
            .set_label(f"{warn['id']} @ {warn['date_recieved'].astimezone(est).strftime(DATE_FORMAT)}")
            .add_to_container()
        )
        embed.add_field(
            name=f"ID: {warn['id']} @ {warn['date_recieved'].astimezone(est).strftime(DATE_FORMAT)}",
            value=warn["reason"],
            inline=False,
        )

    await ctx.respond(embed=embed, components=menu)

    if len(warn_results) == 0:
        await ctx.edit_initial_response(content="No warns to remove :o")

    await ctx.edit_initial_response("What warn would you like to remove?")

    try:
        with bot.stream(hikari.InteractionCreateEvent, timeout=60).filter(
            lambda event: event.interaction.user.id == ctx.author.id
            and isinstance(event.interaction, hikari.ComponentInteraction)
            and event.interaction.custom_id.startswith("remove_warn_")
        ) as stream:
            async for event in stream:
                id_to_del = int(event.interaction.custom_id.split("_")[-1])
                await ctx.edit_initial_response(f"Attempting to delete {id_to_del}", components=[])
                await postgres.delete_warn_member(id_to_del)
                await ctx.edit_initial_response(f"Removed warn {id_to_del}")
    except asyncio.TimeoutError:
        await ctx.edit_initial_response("No response :(")


@with_any_role_check(
    [
        "Can Kick",
    ]
)
@component.with_command
@tanjun.with_channel_slash_option("echo_channel", "Send this warn to an alternative channel.", default=None)
@tanjun.with_str_slash_option("reason", "Reason the user has been warned.")
@tanjun.with_member_slash_option("member", "Member to warn")
@tanjun.as_slash_command("kick", "Kick a Member")
async def kick_command(
    ctx: SlashContext,
    member: hikari.InteractionMember,
    reason: str,
    echo_channel: hikari.InteractionChannel,
    postgres: PostgresDatabase = tanjun.inject(type=PostgresDatabase),
):
    if not ctx.member:
        return

    if member.get_top_role().position >= ctx.member.get_top_role().position:
        return await ctx.respond("You cannot warn someone with the same or higher role.")

    if member.id == ctx.author.id:
        return await ctx.respond("You cannot warn yourself.")

    await ctx.defer()

    if echo_channel:
        real_echo_channel = await ctx.client.rest.fetch_channel(echo_channel.id)
    else:
        real_echo_channel = ctx.get_channel()
        if not real_echo_channel:
            real_echo_channel = await ctx.fetch_channel()

    guild = ctx.get_guild()
    if not guild:
        guild = await ctx.fetch_guild()
    if not guild:
        return

    _ = await kick(
        member,
        real_echo_channel,
        reason,
        postgres,
        guild,
    )

    await ctx.respond(CONFIRM)


async def kick(
    member: hikari.InteractionMember | hikari.Member,
    echo_channel: hikari.TextableGuildChannel | hikari.PartialChannel,
    reason: str,
    postgres: PostgresDatabase,
    guild: hikari.Guild,
) -> hikari.Message:
    """Kick a member from the guild.

    Parameters
    ----------
    member: hikari.InteractionMember | hikari.members
        The member to kick.
    echo_channel: hikari.TextableGuildChannel | hikari.PartialChannel.
        The channel to send response/confirmation to.
    reason: str
        The reason for this Kick."""
    now = datetime.datetime.now().astimezone()

    await postgres.save_kick_member(guild.id, member, reason, now)

    embed = hikari.Embed(title=f"{member.username} has been Kicked", timestamp=now, color=colors.LEFT)
    embed.add_field(name="Kick Reason", value=reason)
    embed.set_thumbnail(guild.icon_url)

    try:
        await member.send(f"You have been kicked from {guild.name} for {reason}")
    except hikari.ForbiddenError:
        await echo_channel.send("Member was not dm'd :(")

    await member.kick(reason=f"Kicked for {reason}")

    return await echo_channel.send(embed=embed)


@with_any_role_check(
    [
        "Can Ban",
    ]
)
@component.with_command
@tanjun.with_channel_slash_option("echo_channel", "Send this warn to an alternative channel.", default=None)
@tanjun.with_str_slash_option("reason", "Reason the user has been warned.")
@tanjun.with_member_slash_option("member", "Member to warn")
@tanjun.as_slash_command("ban", "Ban a Member")
async def ban_command(
    ctx: SlashContext,
    member: hikari.InteractionMember,
    reason: str,
    echo_channel: hikari.InteractionChannel = None,
    postgres: PostgresDatabase = tanjun.inject(type=PostgresDatabase),
):
    if not ctx.member:
        return

    if member.get_top_role().position >= ctx.member.get_top_role().position:
        return await ctx.respond("You cannot warn someone with the same or higher role.")

    if member.id == ctx.author.id:
        return await ctx.respond("You cannot warn yourself.")

    await ctx.defer()

    if echo_channel:
        real_echo_channel = await ctx.client.rest.fetch_channel(echo_channel.id)
    else:
        real_echo_channel = ctx.get_channel()
        if not real_echo_channel:
            real_echo_channel = await ctx.fetch_channel()

    guild = ctx.get_guild()
    if not guild:
        guild = await ctx.fetch_guild()
    if not guild:
        return

    await ban(member, reason, real_echo_channel, guild, postgres)

    await ctx.respond(CONFIRM)


async def ban(
    member: hikari.InteractionMember,
    reason: str,
    echo_channel: hikari.PartialChannel | hikari.TextableChannel,
    guild: hikari.Guild,
    postgres: PostgresDatabase = tanjun.inject(type=PostgresDatabase),
):
    """Ban a member from the calling guild.

    The member will also receive a DM with the reason.

    Parameters
    ----------
    member: hikari.InteractionMember
        The member to ban.
    reason: str
        The reason the member is being banned.
    echo_channel: hikari.PartialChannel | hikari.TextableChannel
        The channel to send confirmation/responses."""
    now = datetime.datetime.now().astimezone()

    await postgres.save_ban_member(guild.id, member, reason, now)

    embed = hikari.Embed(title=f"{member.username} has been Banned", timestamp=now, color=colors.BAN)
    embed.add_field(name="Ban Reason", value=reason)
    embed.set_thumbnail(guild.icon_url)

    try:
        await member.send(
            f"You have been banned from {guild.name} for {reason}.\n\n"
            "If you believe this was a mistake or would like to appeal the ban,"
            " please fill out this form: https://bit.ly/Ban-Appeal-GTD"
        )
    except hikari.ForbiddenError:
        await echo_channel.respond("Member was not dm'd :(")

    await member.ban(reason=f"Banned for -- {reason}")

    await echo_channel.send(embed=embed)


@with_any_role_check(STAFF_ROLES[:2])
@component.with_command
@tanjun.with_channel_slash_option("channel", "The channel to target.", default=None)
@tanjun.with_int_slash_option("hours", "Hours to set the channel on slowmode.", default=0)
@tanjun.with_int_slash_option("minutes", "Minutes to set the channel on slowmode.", default=0)
@tanjun.with_int_slash_option("seconds", "Seconds to set the channel on slowmode.", default=0)
@tanjun.as_slash_command("slowmode", "Turns on channel slowmode.")
async def slowmode_command(
    ctx: SlashContext,
    seconds: int,
    minutes: int,
    hours: int,
    channel: hikari.GuildTextChannel | hikari.PartialChannel | None,
):
    if not channel:
        """No channel provided"""
        channel = ctx.get_channel()
        if not channel:
            """In case channel not cached"""
            channel = await ctx.rest.fetch_channel(ctx.channel_id)

    offset_time = datetime.timedelta(seconds=seconds, minutes=minutes, hours=hours)

    await slowmode(offset_time, channel)

    output_message = "Slowmode turned off."
    if (seconds, minutes, hours) != (0, 0, 0):
        output_message = f"Slowmode activated for {offset_time}"

    await ctx.respond(output_message)


async def slowmode(
    offset_time: datetime.timedelta,
    channel: hikari.GuildTextChannel | hikari.PartialChannel,
):
    """Turn on slowmode/rate limiting for a channel.

    Parameters
    ----------
    seconds: int
        The number of seconds to delay the channel.
    minutes: int
        The number of minutes to delay the channel.
    hours: int
        The number of hours to delay the channel.
    channel: hikari.GuildTextChannel | hikari.PartialChannel | None
        The channel to target. If not provided the calling channel will be targeted.

    """
    await channel.edit(
        rate_limit_per_user=offset_time,
        reason=f"Activated slowmode to {offset_time}",
    )


async def paginate_to_embed(field_name: str, embed: hikari.Embed, to_loop: typing.Iterable) -> hikari.Embed:
    """Loop through an Iterable (to_loop) and append it's contents onto an embed field.
    For use with the SQL returns in moderation.py"""
    fmt_str = "[None]"
    fmt = "{date} - **{reason}**\n"
    est = pytz.timezone("US/Eastern")

    for entry in to_loop:
        if len(fmt_str) <= 1000:
            date = entry["date_recieved"].astimezone(est).strftime(DATE_FORMAT)

            try:
                fmt_str = fmt.format(date=date, reason=entry["reason"])
            except KeyError:
                fmt_str += fmt.format(date=date, reason=entry[2])
    embed.add_field(name=field_name, value=fmt_str, inline=False)

    return embed


loader = component.make_loader()
"""Default tanjun loader."""
