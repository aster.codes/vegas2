from __future__ import annotations

import datetime

import hikari
import tanjun
import yuyo
from hikari import ButtonStyle

from ..util.helpers import get_vegas_logger

logger = get_vegas_logger(__name__)

component = tanjun.Component()

test_grp = component.with_slash_command(
    tanjun.slash_command_group("test", "Test component.", default_to_ephemeral=False)
)  # pylint: disable=C0103


async def callback_prim(ctx: yuyo.ComponentContext):
    await ctx.respond("Clicked Primary!")


async def callback_sec(ctx: yuyo.ComponentContext):
    await ctx.respond("Clicked Secondary!")


@test_grp.with_command
@tanjun.as_slash_command("multi", "Test the yuyo multi component executor")
async def test_multi_component_exec(
    ctx: tanjun.abc.SlashContext,
    component_client: yuyo.ComponentClient = tanjun.injected(type=yuyo.ComponentClient),
):
    executor = (
        yuyo.MultiComponentExecutor()
        .add_action_row()
        .add_interactive_button(ButtonStyle.PRIMARY, callback_prim)
        .set_emoji("👍")
        .add_to_container()
        .add_interactive_button(ButtonStyle.SECONDARY, callback_sec)
        .set_emoji("💫")
        .add_to_container()
        .add_to_parent()
    )
    message = await ctx.respond("Test!", components=executor.builders, ensure_result=True)
    component_client.set_executor(message, executor)


@test_grp.with_command
@tanjun.as_slash_command("paginator", "Test the yuyo paginator")
async def test_paginator(
    ctx: tanjun.abc.SlashContext,
    component_client: yuyo.ComponentClient = tanjun.injected(type=yuyo.ComponentClient),
):
    entries = (
        "Entry 1",
        "Entry 2",
        "Entry 3",
    )

    iterator = (
        (
            f"`{entry}`",
            hikari.Embed(
                color=16711680,
                title=f"Text selected: `{entry}`",
            ),
        )
        for entry in entries
    )

    paginator = yuyo.ComponentPaginator(iterator, authors=(ctx.author,), timeout=datetime.timedelta(minutes=3))
    if first_response := await paginator.get_next_entry():
        content, embed = first_response
        message = await ctx.respond(content=content, component=paginator, embed=embed, ensure_result=True)
        component_client.add_executor(message, paginator)
        return
    await ctx.respond("Entry not found")


import asyncio


@test_grp.with_command
@tanjun.as_slash_command("wait-for", "Test the yuyo waitfor executor")
async def test_waitfor_exec(
    ctx: tanjun.abc.SlashContext,
    component_client: yuyo.ComponentClient = tanjun.injected(type=yuyo.ComponentClient),
):
    row = (
        ctx.rest.build_message_action_row()
        .add_interactive_button(ButtonStyle.PRIMARY, "primary_button")
        .set_label("Primary!")
        .add_to_container()
        .add_interactive_button(ButtonStyle.SECONDARY, "secondary_button")
        .set_label("Second!")
        .add_to_container()
    )

    message = await ctx.respond(
        "Only works once!",
        component=row,
        ensure_result=True,
    )
    executor = yuyo.components.WaitFor(authors=(ctx.author.id,), timeout=datetime.timedelta(seconds=30))
    component_client.set_executor(message.id, executor)

    try:
        result = await executor.wait_for()
        custom_id = result.interaction.custom_id
    except asyncio.TimeoutError:
        await ctx.respond("timed out")

    else:
        await result.respond(f"The custom id was {custom_id}")


@test_grp.with_command
@tanjun.as_slash_command("flatten", "Test flattening")
async def flattening(
    ctx: tanjun.abc.SlashContext,
):
    await ctx.defer()
    if not ctx.guild_id:
        return
    async for log in ctx.rest.fetch_audit_log(ctx.guild_id):
        logger.info(type(log))
        logger.info(type(log.entries))
        for entry in log.entries.values():
            logger.info(type(entry))

    await ctx.respond()


async def persistent_callback(ctx: yuyo.ComponentContext):
    await ctx.respond("Clicked!")


@component.with_listener(hikari.StartedEvent)
async def on_bot_ready(
    _: hikari.StartedEvent,
    component_client: yuyo.ComponentClient = tanjun.injected(type=yuyo.ComponentClient),
):
    component_client.set_constant_id("click-me-id", persistent_callback)


@test_grp.with_command
@tanjun.as_slash_command("persistent-button", "Test the yuyo waitfor executor")
async def test_persistent_button(
    ctx: tanjun.abc.SlashContext,
):
    row = ctx.rest.build_message_action_row().add_interactive_button(ButtonStyle.PRIMARY, "click-me-id").set_label("Click Me!").add_to_container()

    await ctx.respond(
        "Persistent!",
        component=row,
        ensure_result=True,
    )


loaders = component.make_loader()
