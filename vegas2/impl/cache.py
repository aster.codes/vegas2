"""Standard Cache Implementation for VEGAS2.

This cache provides an API to be inherited by any Cache source for VEGAS2.
All methods must resolve to provide full features.
"""
from __future__ import annotations

import typing
from abc import ABC, abstractclassmethod

if typing.TYPE_CHECKING:
    from ..models.autoroles import AutoRoleList
    from ..models.guild_settings import LocalGuildSettings

DEFAULT_LONG_TIMEOUT = 604_800  # One Week

__all__ = [
    "CacheImpl",
]


class CacheImpl(ABC):
    """Basic implementation for a Cache for VEGAS2."""

    __slots__: typing.Sequence[str] = ("__default_timeout",)

    @abstractclassmethod
    def __init__(self, *, default_expire: int | None = DEFAULT_LONG_TIMEOUT) -> None:
        ...

    @abstractclassmethod
    async def flushall(self) -> bool:
        """Clear all cache from the store.

        Returns
        -------
        bool
            Returns True if the cache was flushed, False on failure."""
        ...

    @abstractclassmethod
    async def get_guild_settings(self, guild_id: int) -> LocalGuildSettings:
        """Retrieve LocalGuildSettings from the store.

        Parameters
        ----------
        guild_id: int
            The Discord Guild/Server ID to lookup.

        Returns
        -------
        LocalGuildSettings
        """
        ...

    @abstractclassmethod
    async def set_guild_settings(self, settings: LocalGuildSettings) -> bool:
        """Set a LocalGuildSettings in the store

        Parameters
        ----------
        settings: LocalGuildSettings
            The item to cache.

        Returns
        -------
        bool
            True if cache succeed, False on failure."""
        ...

    @abstractclassmethod
    async def clear_guild_setting_for_guild(self, guild_id: int) -> bool:
        """Clear LocalGuildSettings for the provided guild_id in the store.

        Parameters
        ----------
        guild_id: int
            The guild id to clear.

        Returns
        -------
        bool
            True if the item is cleared, False if failed."""
        ...

    @abstractclassmethod
    async def clear_guild_settings(self) -> bool:
        """Clear all LocalGuildSettings in the store.

        Returns
        -------
        bool
            True if the store is cleared, False if the flush failed."""
        ...

    @abstractclassmethod
    async def get_auto_deleter_channel_ids(self, guild_id: int) -> set[int]:
        """Returns a typing.Set of all Auto Deleter Channel ID's for this guild.

        Parameters
        ----------
        guild_id: int
            The Guild ID to retrieve channel ids for.

        Returns
        ------
        set[int]
            The Set of channel ids to be cached."""
        ...

    @abstractclassmethod
    async def set_auto_deleter_channel_ids(self, guild_id: int, channel_ids: set[int]) -> bool:
        """Set a channel ids for Auto Deleter for this Guild ID in the store.

        Parameters
        ----------
        guild_id: int
            The Guild ID these channel ids are associated with.
        hannel_ids: set[int]
            The Set of channel ids to be cached.

        Returns
        -------
        bool
            True if cache succeed, False on failure."""
        ...

    @abstractclassmethod
    async def clear_auto_deleter_channel_ids_for_guild(self, guild_id: int):
        """Clear all Auto Deleter channel ids for this Guild id in the store.

        Parameters
        ----------
        guild_id: int
            The Guild ID to clear auto deleter channels associated with.

        Returns
        -------
        bool
            True if the store is cleared, False if the flush failed."""
        ...

    @abstractclassmethod
    async def clear_auto_deleter_channel_ids(self):
        """Clear all Auto Deleter channel ids in the store.

        Returns
        -------
        bool
            True if the store is cleared, False if the flush failed."""
        ...

    @abstractclassmethod
    async def get_autoroles_for_guild(self, guild_id: int):
        """Returns a the AutoRoles for this guild.

        Parameters
        ----------
        auto_roles_list: AutoRoleList
            The AutoRoleList to populate with entries.

        Returns
        ------
        AutoRoleList | None
            The list of AutoRoles from the cache."""
        ...

        """"""
        ...

    @abstractclassmethod
    async def set_autoroles_from_guild(self, auto_roles_list: AutoRoleList) -> bool:
        """Set a AutoRoles in the store.

        Parameters
        ----------
        guild_id: int
            The Guild ID this item is for.
        auto_roles_list: AutoRoleList
            the AutoRoles to cache for this Guild.

        Returns
        -------
        bool
            True if cache succeed, False on failure."""
        ...

    @abstractclassmethod
    async def clear_autoroles_from_guild(self, guild_id: int) -> bool:
        """Clear all AutoRoles for the provided Guild ID in the store.

        Parameters
        ----------
        guild_id: int
            The guild id to clear.

        Returns
        -------
        bool
            True if the store is cleared, False if the flush failed."""
        ...

    @abstractclassmethod
    async def clear_autoroles(self) -> bool:
        """Clear all AutoRoles in the store.

        Returns
        -------
        bool
            True if the store is cleared, False if the flush failed."""
        ...
