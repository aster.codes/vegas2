"""All ABC/AbstractBaseClasses and interfaces that VEGAS2 uses.

These modules should give a good idea of what API is required.
For example, if you would like to write a DB driver for MongoDB,
just inherit from impl.database.DatabaseImpl."""
__all__ = [
    "cache",
    "CacheImpl",
    "database",
    "DatabaseImpl",
]
from . import cache, database
from .cache import CacheImpl
from .database import DatabaseImpl
