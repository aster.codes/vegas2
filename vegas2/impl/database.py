from __future__ import annotations

import typing
from abc import ABC, abstractclassmethod

import tanjun


class DatabaseImpl(ABC):
    """An Absttract Base Class that provides the interface
    that VEGAS2 requires be filled for a database connection."""

    __slots__: typing.Sequence[str] = (
        "__event_manager",
        "__started",
        "_pool",
        "__max_connections",
    )

    @abstractclassmethod
    def __init__(
        self,
        *,
        max_connections: int = 5,
    ):
        """Setup the defaults.

        Parameters
        ----------
        max_connections: int = 5
            The maximum connections this database pool can spawn."""
        ...

    @abstractclassmethod
    def load_into_client(self, client: tanjun.Client) -> None:
        """Helper function to load the Database into the Tanjun Client.

        Only required if the DB setup requires async."""

    @abstractclassmethod
    async def _open(self) -> None:
        """A callback to setup the Database Pool."""

    @abstractclassmethod
    async def _close(self) -> None:
        """A callback to teardown the Database Pool."""
