import typing

from tanjun import abc as tanjun_abc
from tanjun import errors

CommandT = typing.TypeVar("_CommandT", bound="tanjun_abc.ExecutableCommand[typing.Any]")


class _Check:
    __slots__ = ("_error_message", "_halt_execution", "__weakref__")

    def __init__(
        self,
        error_message: typing.Optional[str],
        halt_execution: bool,
    ) -> None:
        self._error_message = error_message
        self._halt_execution = halt_execution

    def _handle_result(self, result: bool) -> bool:
        if not result:
            if self._error_message:
                raise errors.CommandError(self._error_message) from None
            if self._halt_execution:
                raise errors.HaltExecution from None

        return result
