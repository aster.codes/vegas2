"""Models provide a container for logic relating to specific Data.

This "data" can be information contained soloy in a guild through channels,
like with models.tickets. The "data" could also be information soloy about
VEGAS2s internal data, like with models.cache. These classes just serve
as a logical way to group data transformations and mutations.
"""
__all__ = [
    "autoroles",
    "AutoRoleEntry",
    "AutoRoleList",
    "cache",
    "redis",
    "RedisCache",
    "database",
    "postgres",
    "PostgresDatabase",
    "guild_settings",
    "LocalGuildSettings",
    "tickets",
    "Ticket",
    "TICKETS_LOGO",
]
from . import autoroles, cache, database, guild_settings, tickets
from .autoroles import AutoRoleEntry, AutoRoleList
from .cache import redis
from .cache.redis import RedisCache
from .database import postgres
from .database.postgres import PostgresDatabase
from .guild_settings import LocalGuildSettings
from .tickets import TICKETS_LOGO, Ticket
