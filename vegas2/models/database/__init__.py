"""All currently supported Database connections for VEGAS2.

If you would like VEGAS2 to support a different DB, you can
write a simple connector by inheriting from `vegas2.impl.database.DatabaseImpl`:

```py

from vegas2.impl.database import DatabaseImpl

class MongoDB(DatabaseImpl):
    ...
```

Ensure that all of the methods from DatabaseImpl resolve and register it in the client!
"""
__all__ = [
    "postgres",
    "PostgresDatabase",
]
from . import postgres
from .postgres import PostgresDatabase
