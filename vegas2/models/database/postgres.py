"""A PostgresQL connector for VEGAS2 persistent storage.
"""
from __future__ import annotations

import datetime
import typing

import aiopg
import hikari
import tanjun
from psycopg2 import Error as PGError
from psycopg2.extras import RealDictCursor

from ...config import FullConfig
from ...errors import AutoRoleUpsertFailedException
from ...events import (
    AutoRoleEntryDatabaseRemoved,
    AutoRoleEntryDatabaseUpdated,
    AutoRoleEntryRemoved,
    AutoRoleEntryUpdated,
    GuildSettingDatabaseRemoved,
    GuildSettingDatabaseUpdated,
    GuildSettingRemoved,
    GuildSettingUpdated,
)
from ...impl.database import DatabaseImpl
from ...util.constants.qotd import DEFAULT_QOTD_PFP
from ...util.helpers import get_vegas_logger
from ...util.sql.auto_roles import AUTOROLE_ENTRY_DELETE, AUTOROLE_ENTRY_UPSERT
from ...util.sql.guild_settings import GUILD_SETTINGS_DELETE, GUILD_SETTINGS_SAVE
from ...util.sql.moderation_sql import (
    BAN_MEMBER_SQL,
    KICK_MEMBER_SQL,
    MEMBER_EXISTS_SQL,
    MODERATION_USER_CHECK_SQL,
    STRIKE_MEMBER_SQL,
    STRIKE_REMOVE_SQL,
    WARN_MEMBER_SQL,
    WARN_REMOVE_SQL,
)
from ...util.sql.qotd import (
    AOTD_ADD_ANSWER,
    QOTD_ADD_QUESTION,
    QOTD_ARCHIVE_QUESION,
    QOTD_DELETE_QUESTION,
    QOTD_POST,
)

if typing.TYPE_CHECKING:
    from ..autoroles import AutoRoleList
    from ..guild_settings import LocalGuildSettings

logger = get_vegas_logger(__name__)

__all__ = [
    "PostgresDatabase",
]


class PostgresDatabase(DatabaseImpl):
    """A database handler for Postgres."""

    __slots__: typing.Sequence[str] = (
        "__event_manager",
        "__started",
        "__pool",
        "__max_connections",
        "__rest",
        "__hikari_cache",
    )

    def __init__(
        self,
        *,
        max_connections: int = 5,
    ) -> None:
        self.__max_connections = max_connections
        self.__pool = None
        self.__started = False

    def load_into_client(self, client: tanjun.Client) -> None:
        """Load this dependency into a `tanjun.Client`."""
        client.add_client_callback(tanjun.ClientCallbackNames.STARTING, self._open).add_client_callback(
            tanjun.ClientCallbackNames.CLOSED, self._close
        )

    async def _open(
        self,
        client: tanjun.Client = tanjun.injected(type=tanjun.Client),
        config: FullConfig = tanjun.injected(type=FullConfig),
        event_manager: hikari.api.EventManager = tanjun.injected(type=hikari.api.EventManager),
        rest: hikari.api.RESTClient = tanjun.injected(type=hikari.api.RESTClient),
        hikari_cache: hikari.api.Cache = tanjun.injected(type=hikari.api.Cache),
    ) -> None:
        """Helper method to ensure `aiopg.Pool` is acquired."""
        if self.__started:
            raise RuntimeError("Session already running")

        self.__event_manager = event_manager
        self.__rest = rest
        self.__hikari_cache = hikari_cache

        self.__pool = await aiopg.create_pool(
            f"dbname={config.database.name} user={config.database.username} password={config.database.password} port=5432 host={config.database.host}",  # noqa: E501
            maxsize=self.__max_connections,
            timeout=60,
        )
        self.__started = True

        client.set_type_dependency(type(self), self)

        self.__event_manager.subscribe(AutoRoleEntryUpdated, self.save_autoroleentry)
        self.__event_manager.subscribe(AutoRoleEntryRemoved, self.delete_autoroleentry)
        self.__event_manager.subscribe(GuildSettingUpdated, self.save_guildsettings)
        self.__event_manager.subscribe(GuildSettingRemoved, self.delete_guildsettings)
#        self.__event_manager.subscribe(hikari.ExceptionEvent, self._exception_handler)


    async def _close(self, client: tanjun.Client = tanjun.injected(type=tanjun.Client)) -> None:
        """Helper method to ensure the acquired `aiopg.Pool` is closed properly."""
        if not self.__pool:
            raise RuntimeError("Session not running")

        pool: aiopg.Pool = self.__pool
        if isinstance(pool, aiopg.Pool):
            await pool.close()
        self.__pool = None
        client.remove_type_dependency(aiopg.Pool)

    async def __ensure_member_exists(self, guild_id: hikari.Snowflake | int, member_id: hikari.Snowflake | int) -> int | None:
        if not self.__ready():
            raise RuntimeError("Postgres not ready!")

        try:
            with (await self.__pool.cursor(cursor_factory=RealDictCursor)) as cursor:
                logger.debug("Checking postgres to ensure user exists for guild.")
                await cursor.execute(
                    MEMBER_EXISTS_SQL,
                    {"guild_id": guild_id, "member_id": member_id},
                )
                result = await cursor.fetchone()

                return result["id"]
        except PGError as err:
            logger.error(err.pgerror)
            logger.error(err.pgcode)
            return None

    async def __ensure_guild_settings_exist(self, guild_id: hikari.Snowflake | int) -> int | None:
        if not self.__ready():
            raise RuntimeError("Postgres not ready!")

        try:
            with (await self.__pool.cursor(cursor_factory=RealDictCursor)) as cursor:
                logger.debug("Checking postgres for GuildOwnerSettings")
                await cursor.execute(
                    """
                    INSERT INTO guildownersettings_guildownersettings
                        (guild_id, logging_enabled, auto_delete_enabled)
                    VALUES
                        ( %(guild_id)s, %(logging_enabled)s, %(auto_delete_enabled)s )
                    ON CONFLICT (guild_id) DO
                        UPDATE SET
                            logging_enabled=EXCLUDED.logging_enabled
                    RETURNING id
                    """,
                    {"guild_id": guild_id, "logging_enabled": False, "auto_delete_enabled": False},
                )
                result = await cursor.fetchone()

                return result["id"]

        except PGError as err:
            logger.error(err.pgerror)
            logger.error(err.pgcode)
            return None

    def __ready(self) -> bool:
        if not self.__started or not self.__pool or not self.__hikari_cache or not self.__rest:
            raise RuntimeError("Postgres not ready!")
        return True

    async def __aenter__(self):
        logger.info("Entering postgres database context manager")
        if not self.__ready():
            raise RuntimeError("Postgres not ready!")

        logger.info("Try postgres database context manager")
        con = await self.__pool._acquire()
        cursor = await con.cursor(cursor_factory=RealDictCursor)
        logger.info("Returning postgres database context manager")
        return (con, cursor)

    async def __aexit__(self, *args, **kwargs):
        logger.info("Entering postgres database context manager")

    async def save_autoroleentry(self, event: AutoRoleEntryUpdated):
        """Callback to handle all AutoRoleEntryUpdated events.

        Attempts to save the data to Postgres, or captures the error.

        Finally emits a new AutoRoleEntryDatabaseUpdate event."""
        logger.debug(f"Saving AutoRoleEntry for Guild {event.guild_id} - Role ID: {event.role_id}")
        if not self.__ready():
            raise RuntimeError("Postgres not ready!")
        entry = {
            "emoji": event.emoji,
            "custom_emoji_id": event.custom_emoji_id,
            "role_id": event.role_id,
            "author_id": event.author_id,
            "guild_id": event.guild_id,
            "channel_id": event.channel_id,
        }
        try:
            with (await self.__pool.cursor(cursor_factory=RealDictCursor)) as cursor:
                await cursor.execute(AUTOROLE_ENTRY_UPSERT, entry)
        except PGError:
            logger.error(PGError)
            raise AutoRoleUpsertFailedException(event=event)
        self.__event_manager.dispatch(AutoRoleEntryDatabaseUpdated(app=event.app, **entry))

    async def delete_autoroleentry(self, event: AutoRoleEntryRemoved):
        """Callback to handle all AutoRoleEntryRemoved events.

        Attempts to delete the data from Postgres, or captures the error.

        Finally emits a new AutoRoleEntryDatabaseRemoved event."""
        if not self.__ready():
            raise RuntimeError("Postgres not ready!")
        entry = {
            "emoji": event.emoji,
            "custom_emoji_id": event.custom_emoji_id,
            "role_id": event.role_id,
            "author_id": event.author_id,
            "guild_id": event.guild_id,
            "channel_id": event.channel_id,
        }
        try:
            with (await self.__pool.cursor(cursor_factory=RealDictCursor)) as cursor:
                await cursor.execute(AUTOROLE_ENTRY_DELETE, entry)
                logger.debug(
                    "Postgres Deleted AutoRoleEntry: "
                    f"<Guild Id: {event.guild_id}, Emoji: {event.emoji}, "
                    f"CEI: {event.custom_emoji_id}, Role Id: {event.role_id}>"
                )
        except PGError as err:
            logger.error(err.pgerror)
            logger.error(err.pgcode)
            return None
        self.__event_manager.dispatch(AutoRoleEntryDatabaseRemoved(app=event.app, **entry))

    async def fetch_autoroles_for_guild(self, auto_roles: AutoRoleList) -> AutoRoleList | bool:
        """Sets up a `AutoRolesList` for the provided `guild_id`, if one exists in the database or redis.

        Parameters
        ----------
        guild_id: int | hikari.Snowflake
            The guild/server ID to lookup Roles for.

        Returns
        -------
        AutoRoleList
            An AutoRoleList filled with the related data.
        """
        if not self.__ready() or not auto_roles.guild_id:
            raise RuntimeError("Postgres not ready!")

        relation_id = await self.__ensure_guild_settings_exist(auto_roles.guild_id)
        autoroles_db: list[dict[str, str]] | bool = False

        try:
            with (await self.__pool.cursor(cursor_factory=RealDictCursor)) as cursor:
                await cursor.execute(
                    "SELECT * FROM autoroles_autoroles WHERE guild_id = %(relation_id)s;",
                    {"relation_id": relation_id},
                )
                autoroles_db = await cursor.fetchall()
        except PGError as err:
            logger.error(err.pgerror)
            logger.error(err.pgcode)
            return False
        roles = self.__hikari_cache.get_roles_view_for_guild(auto_roles.guild_id)
        known_emoji = self.__hikari_cache.get_emojis_view_for_guild(auto_roles.guild_id)

        for entry in autoroles_db:
            role = roles.get(hikari.Snowflake(entry["role_id"]))

            if entry.get("custom_emoji_id"):
                entry_emoji = known_emoji.get(hikari.Snowflake(entry["custom_emoji_id"]))
            else:
                entry_emoji = hikari.UnicodeEmoji().parse(entry["emoji"])

            if not role or not entry_emoji:
                continue

            auto_roles.append(emoji=entry_emoji, role=role)

        return auto_roles

    async def save_guildsettings(self, event: GuildSettingUpdated):
        """Event handler to manage automatically saving GuildSettings to the database."""
        logger.debug(f"Saving GuildSetting for Guild {event.guild_id}")
        if not self.__ready():
            raise RuntimeError("Postgres not ready!")
        entry = {
            "guild_id": event.guild_id,
            "author_id": event.author_id,
            "channel_id": event.channel_id,
            "logging_enabled": event.logging_enabled,
            "logging_channel": event.logging_channel,
            "vc_logging_channel": event.vc_logging_channel,
            "mod_logging_channel": event.mod_logging_channel,
            "auto_delete_enabled": event.auto_delete_enabled,
            "auto_delete_channels_ids": list(event.auto_delete_channels_ids) if event.auto_delete_channels_ids else [],
        }
        try:
            with (await self.__pool.cursor(cursor_factory=RealDictCursor)) as cursor:
                await cursor.execute(GUILD_SETTINGS_SAVE, entry)
                logger.debug(f"<GuildSettings: {event.guild_id}> saved to Postgres")
        except PGError as err:
            logger.error(err.pgerror)
            logger.error(err.pgcode)
            return None
        self.__event_manager.dispatch(GuildSettingDatabaseUpdated(app=event.app, **entry))

    async def delete_guildsettings(self, event: GuildSettingRemoved):
        """Callback to handle all GuildSettingRemoved events.

        Attempts to delete the data from Postgres, or captures the error.

        Finally emits a new GuildSettingDatabaseRemoved event."""
        if not self.__ready():
            raise RuntimeError("Postgres not ready!")
        entry = {
            "guild_id": event.guild_id,
            "author_id": event.author_id,
            "channel_id": event.channel_id,
            "logging_enabled": event.logging_enabled,
            "logging_channel": event.logging_channel,
            "vc_logging_channel": event.vc_logging_channel,
            "mod_logging_channel": event.mod_logging_channel,
            "auto_delete_enabled": event.auto_delete_enabled,
            "auto_delete_channels_ids": list(event.auto_delete_channels_ids) if event.auto_delete_channels_ids else [],
        }
        try:
            with (await self.__pool.cursor(cursor_factory=RealDictCursor)) as cursor:
                await cursor.execute(GUILD_SETTINGS_DELETE, entry)
                logger.debug("Postgres Deleted GuildSetting: " f"<Guild Id: {event.guild_id}>")
        except PGError as err:
            logger.error(err.pgerror)
            raise AutoRoleUpsertFailedException(event=event)
        self.__event_manager.dispatch(GuildSettingDatabaseRemoved(app=event.app, **entry))

    async def fetch_qotds_for_guild(
        self, guild_id: hikari.Snowflake | int, show_archived: bool = False, limit: int = 250
    ) -> list[dict] | None:
        if not self.__ready():
            raise RuntimeError("Postgres not ready!")

        try:
            with (await self.__pool.cursor(cursor_factory=RealDictCursor)) as cursor:
                await cursor.execute(
                    """SELECT * FROM qotd_questionoftheday WHERE
                    archived = %(show_archived)s ORDER BY date_added ASC
                    LIMIT %(limit)s""",
                    {"show_archived": show_archived, "limit": limit},
                )
                results = await cursor.fetchall()
                return results
        except PGError as err:
            logger.error(err.pgcode)
            logger.error(err.pgerror)

    async def fetch_guild_settings_for_guild(self, guild_settings: LocalGuildSettings) -> LocalGuildSettings:
        if not self.__ready():
            raise RuntimeError("Postgres not ready!")

        guild_db_id = await self.__ensure_guild_settings_exist(int(guild_settings.guild_id))
        try:
            with (await self.__pool.cursor(cursor_factory=RealDictCursor)) as cursor:
                await cursor.execute(
                    """SELECT
                    logging_enabled, logging_channel, vc_logging_channel,
                    mod_logging_channel, auto_delete_enabled, auto_delete_channel_ids
                    FROM guildownersettings_guildownersettings
                    WHERE id=%(id)s;""",
                    {"id": guild_db_id},
                )
                result = await cursor.fetchone()
                if not result.get("guild_id"):
                    return guild_settings
        except PGError as err:
            logger.error(err.pgcode)
            logger.error(err.pgerror)
            return guild_settings

        logger.debug(f"Found LocalGuildSettings {guild_settings.guild_id} in Postgres")
        guild_settings.logging_channel = result["logging_channel"]
        guild_settings.vc_logging_channel = result["vc_logging_channel"]
        guild_settings.mod_logging_channel = result["mod_logging_channel"]
        guild_settings.auto_delete_enabled = result["auto_delete_enabled"]
        guild_settings.auto_delete_channel_ids = result["auto_delete_channel_ids"]
        return guild_settings

    async def fetch_qotd_for_guild(
        self,
        guild_id: hikari.Snowflake | int,
        question_id: int | None = None,
        date_posted: datetime.datetime | None = None,
        archived: bool = False,
    ) -> dict | None:
        """Fetch a Question of the Day from the database by a date.

        Parameters
        ----------
        guild_id: hikari.Snowflake | int
            The guild to use as context.

        Other Parameters
        ----------------
        date_posted: datetime.datetime = datetime.datetime.today().astimezone()
            A to lookup. Defaults to today.

        Returns
        -------
        dict | None
            A dictionary representing the postgres row, or None if failed."""
        try:
            with (await self.__pool.cursor(cursor_factory=RealDictCursor)) as cursor:
                await cursor.execute(
                    QOTD_POST,
                    {
                        "archived": archived,
                        "question_id": question_id,
                        "date_posted": date_posted.strftime("%m-%d-%Y") if date_posted else None,
                        "guild_id": guild_id,
                    },
                )
                return await cursor.fetchone()
        except (PGError, ValueError) as err:
            logger.error(
                f"Failed with context: <Arcivhed={archived}, "
                f"Question Id={question_id}, Date Posted={date_posted}, "
                f"Guild Id={guild_id}>",
                extra={
                    "archived": archived,
                    "question_id": question_id,
                    "date_posted": date_posted.strftime("%m-%d-%Y") if date_posted else None,
                    "guild_id": guild_id,
                },
            )
            if isinstance(err, PGError):
                logger.error(err.pgcode)
                logger.exception(
                    err.pgerror,
                )
            else:
                logger.exception(
                    err.with_traceback(None),
                    extra={
                        "archived": archived,
                        "question_id": question_id,
                        "date_posted": date_posted.strftime("%m-%d-%Y") if date_posted else None,
                        "guild_id": guild_id,
                    },
                )

    async def save_qotd_question(
        self,
        guild_id: hikari.Snowflake | int,
        question: str,
        author: hikari.InteractionMember,
        date_posted: datetime.datetime | None,
    ) -> bool:
        try:
            member_db_id = await self.__ensure_member_exists(guild_id, author.id)
            if not member_db_id:
                return

            with (await self.__pool.cursor(cursor_factory=RealDictCursor)) as cursor:
                await cursor.execute(
                    QOTD_ADD_QUESTION,
                    {
                        "question": question,
                        "date_added": datetime.datetime.now().astimezone(),
                        "nickname": f"{author.nickname}#{author.discriminator}"
                        if author.nickname
                        else f"{author.username}#{author.discriminator}",
                        "avatar_url": str(author.avatar_url) if author.avatar_url else DEFAULT_QOTD_PFP,
                        "date_posted": date_posted if date_posted else None,
                        "author_id": member_db_id,
                    },
                )
                logger.debug(f"Postgres save <{guild_id}>")
                return True
        except PGError as err:
            logger.error(err.pgcode)
            logger.error(err.pgerror)
            return False

    async def archive_qotd_question(
        self,
        guild_id: hikari.Snowflake | int,
        question_id: int,
        date_posted: datetime.datetime | None = None,
    ) -> bool:
        if not date_posted:
            date_posted = datetime.datetime.today().astimezone()
        try:
            with (await self.__pool.cursor(cursor_factory=RealDictCursor)) as cursor:
                await cursor.execute(
                    QOTD_ARCHIVE_QUESION,
                    {"guild_id": guild_id, "question_id": question_id, "date_posted": date_posted},
                )
                logger.debug(f"Postgres archived <{guild_id}, qotd_id={question_id}>")
                return True
        except PGError as err:
            logger.error(err.pgcode)
            logger.error(err.pgerror)
            return False

    async def delete_qotd_question(
        self,
        guild_id: hikari.Snowflake | int,
        question_id: int,
    ) -> bool:
        try:
            with (await self.__pool.cursor(cursor_factory=RealDictCursor)) as cursor:
                await cursor.execute(
                    QOTD_DELETE_QUESTION,
                    {
                        "question_id": question_id,
                        "guild_id": guild_id,
                    },
                )
                logger.debug(f"Postgres deleted <{guild_id}, qotd_id={question_id}>")
                return True
        except PGError as err:
            logger.error(err.pgcode)
            logger.error(err.pgerror)
            return False

    async def save_aotd_answer(self, guild_id: hikari.Snowflake | int, qotd_id: int, message: hikari.Message):
        """Saves an Answer of the Day to the Postgres Database for the provided Quesiont of the Day.

        Parameters
        ----------
        guild_id: hikari.Snowflake | int
            The Guild ID to associate this Answer with.
        qotd_id: int
            A Question of the Day ID to mark this an answer of.
        message: hikari.Message
            The message object to add as an answer."""
        try:
            user_db_id = await self.__ensure_member_exists(guild_id, message.author.id)
            with (await self.__pool.cursor(cursor_factory=RealDictCursor)) as cursor:
                logger.info(f"Trying to save AOTD <Guild:{guild_id}, QuestionId:{qotd_id}>")
                await cursor.execute(
                    AOTD_ADD_ANSWER,
                    {
                        "content": message.content,
                        "avatar_url": str(message.author.avatar_url),
                        "nickname": str(message.author.mention),
                        "author_id": user_db_id,
                        "question_id": qotd_id,
                    },
                )
                logger.debug(f"Postgres AOTD <{guild_id}, qotd_id={qotd_id}, message={message.id}>")
                return True
        except PGError as err:
            logger.error(err.pgcode)
            logger.error(err.pgerror)
            return False

    async def fetch_user_log(
        self,
        guild_id: hikari.Snowflake | int,
        member_id: hikari.Snowflake | int,
        strike_limit: int = 5,
        warn_limit: int = 5,
        kick_limit: int = 3,
        ban_limit: int = 2,
        role_limit: int = 2,
    ) -> list[list[dict]] | bool:
        if not self.__ready():
            raise RuntimeError("Postgres not ready!")

        member_db_id = await self.__ensure_member_exists(guild_id, member_id)
        try:
            with (await self.__pool.cursor(cursor_factory=RealDictCursor)) as cursor:
                await cursor.execute(
                    MODERATION_USER_CHECK_SQL,
                    {
                        "member_id": member_db_id,
                        "strike_limit": strike_limit,
                        "warn_limit": warn_limit,
                        "kick_limit": kick_limit,
                        "ban_limit": ban_limit,
                        "role_limit": role_limit,
                    },
                )
                result = await cursor.fetchall()
        except PGError as err:
            logger.error(err.pgcode)
            logger.error(err.pgerror)
            return False

        returnable = [[], [], [], []]

        for entry in result:
            if entry.get("type", False) == "strike":
                returnable[0].append(entry)
            if entry.get("type", False) == "warn":
                returnable[1].append(entry)
            if entry.get("type", False) == "kick":
                returnable[2].append(entry)
            if entry.get("type", False) == "ban":
                returnable[3].append(entry)

        return returnable

    async def save_strike_member(
        self,
        guild_id: hikari.Snowflake | int,
        strike_role: hikari.Role,
        member: hikari.Member,
        reason: str,
        date_received: datetime.datetime | None = None,
    ) -> bool:
        if not date_received:
            date_received = datetime.datetime.now().astimezone()
        try:
            member_db_id = await self.__ensure_member_exists(guild_id, member.id)
            with (await self.__pool.cursor(cursor_factory=RealDictCursor)) as cursor:
                await cursor.execute(
                    STRIKE_MEMBER_SQL,
                    {
                        "strike_type": strike_role.id,
                        "reason": reason,
                        "discord_user_id": member_db_id,
                        "date_received": date_received,
                    },
                )
                logger.debug(f"Postgres struck Member <{member.id}> for Role <{strike_role.name}> in Guild <{guild_id}>")
                return True
        except PGError as err:
            logger.error(err.pgcode)
            logger.error(err.pgerror)
            return False

    async def delete_strike_member(
        self,
        strike_id: int,
    ) -> bool:
        try:
            with (await self.__pool.cursor(cursor_factory=RealDictCursor)) as cursor:
                await cursor.execute(
                    STRIKE_REMOVE_SQL,
                    {
                        "strike_id": strike_id,
                    },
                )
                logger.debug(f"Postgres deleted strike <{strike_id}>")
                return True
        except PGError as err:
            logger.error(err.pgcode)
            logger.error(err.pgerror)
            return False

    async def save_warn_member(
        self,
        guild_id: hikari.Snowflake | int,
        member: hikari.Member,
        reason: str,
        date_received: datetime.datetime | None = None,
    ) -> bool:
        if not date_received:
            date_received = datetime.datetime.now().astimezone()
        try:
            member_db_id = await self.__ensure_member_exists(guild_id, member.id)
            with (await self.__pool.cursor(cursor_factory=RealDictCursor)) as cursor:
                await cursor.execute(
                    WARN_MEMBER_SQL,
                    {
                        "reason": reason,
                        "discord_user_id": member_db_id,
                        "date_received": date_received,
                    },
                )
                logger.debug(f"Postgres warned Member <{member.id}> in Guild <{guild_id}>")
                return True
        except PGError as err:
            logger.error(err.pgcode)
            logger.error(err.pgerror)
            return False

    async def delete_warn_member(
        self,
        warn_id: int,
    ) -> bool:
        try:
            with (await self.__pool.cursor(cursor_factory=RealDictCursor)) as cursor:
                await cursor.execute(
                    WARN_REMOVE_SQL,
                    {
                        "warn_id": warn_id,
                    },
                )
                logger.debug(f"Postgres removed warn <{warn_id}>")
                return True
        except PGError as err:
            logger.error(err.pgcode)
            logger.error(err.pgerror)
            return False

    async def save_kick_member(
        self,
        guild_id: hikari.Snowflake | int,
        member: hikari.Member,
        reason: str,
        date_received: datetime.datetime | None = None,
    ) -> bool:
        if not date_received:
            date_received = datetime.datetime.now().astimezone()
        try:
            member_db_id = await self.__ensure_member_exists(guild_id, member.id)
            with (await self.__pool.cursor(cursor_factory=RealDictCursor)) as cursor:
                await cursor.execute(
                    KICK_MEMBER_SQL,
                    {
                        "date_received": date_received,
                        "reason": reason,
                        "discord_user_id": member_db_id,
                    },
                )
                logger.debug(f"Postgres kicked Member <{member.id}> in Guild <{guild_id}>")
                return True
        except PGError as err:
            logger.error(err.pgcode)
            logger.error(err.pgerror)
            return False

    async def save_ban_member(
        self,
        guild_id: hikari.Snowflake | int,
        member: hikari.Member,
        reason: str,
        date_received: datetime.datetime | None = None,
    ) -> bool:
        if not date_received:
            date_received = datetime.datetime.now().astimezone()
        try:
            member_db_id = await self.__ensure_member_exists(guild_id, member.id)
            with (await self.__pool.cursor(cursor_factory=RealDictCursor)) as cursor:
                await cursor.execute(
                    BAN_MEMBER_SQL,
                    {
                        "date_received": date_received,
                        "reason": reason,
                        "discord_user_id": member_db_id,
                    },
                )
                logger.debug(f"Postgres banned Member <{member.id}> in Guild <{guild_id}> saved.")
                return True
        except PGError as err:
            logger.error(err.pgcode)
            logger.error(err.pgerror)
            return False
