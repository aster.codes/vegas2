"""All currently supported Cache stores for VEGAS2.

If you would like to extend VEGAS2s support for a different
cache store you can! Start like so:

```py

from veags2.impl.cache import CacheImplmentation

class FastCache(CacheImplementation):
    ...
```

Make sure all of the methods are written and accounted for,
then register this as the Cache in the client!"""
__all__ = [
    "redis",
    "RedisCache",
]
from . import redis
from .redis import RedisCache
