from __future__ import annotations

import os
import typing

import aioredis
import hikari
import tanjun

from ...events import (
    AutoRoleEntryDatabaseRemoved,
    AutoRoleEntryDatabaseUpdated,
    GuildSettingDatabaseRemoved,
    GuildSettingDatabaseUpdated,
)

if typing.TYPE_CHECKING:
    from ..guild_settings import LocalGuildSettings
    from ..autoroles import AutoRoleList

from ...impl.cache import CacheImpl
from ...util.helpers import get_vegas_logger

logger = get_vegas_logger(__name__)

DEFAULT_LONG_TIMEOUT = 604_800  # One Week

LOCAL_GUILD_SETTINGS = "vegas2:{guild_id}:local_guild_settings"
LOCAL_GUILD_IDX = "vegas2:index:local_guild_settings"

AUTO_DELETE_IDS_SET = "vegas2:{guild_id}:auto_delete_ids"
AUTO_DELETE_IDS_IDX = "vegas2:index:auto_delete_ids"

AUTOROLES_KEY = "vegas2:{guild_id}:auto_roles"
AUTOROLES_IDX = "vegas2:index:auto_roles"

DEFAULT_LOGGING_CHANNEL_NAME = "vegas2-logging"


class RedisCache(CacheImpl):
    """Redis Cache Resource for Vegas2."""

    __slots__: typing.Sequence[str] = (
        "_redis_client",
        "__pool",
        "__rest",
        "__hikari_cache",
        "__address",
        "__default_timeout",
        "__max_connections",
        "__started",
        "__event_manager",
    )

    def __init__(
        self,
        *,
        default_expire: int | None = DEFAULT_LONG_TIMEOUT,
        config: typing.MutableMapping[str, typing.Any] | None = None,
        max_connections: int = 5,
        event_manager: hikari.api.EventManager = None,
    ) -> None:
        REDIS_HOST = os.environ.get("REDIS_HOST", "localhost")
        REDIS_PORT = int(os.environ.get("REDIS_PORT", "6379"))

        self._redis_client: aioredis.Redis = None
        self.__pool: aioredis.BlockingConnectionPool = None
        self.__max_connections = max_connections
        self.__default_timeout = default_expire
        self.__address = f"redis://{REDIS_HOST}:{REDIS_PORT}"
        self.__started: bool = False
        self.__event_manager = event_manager
        if self.__event_manager:
            self.__event_manager.subscribe(AutoRoleEntryDatabaseUpdated, self.set_autorolesentry)
            self.__event_manager.subscribe(AutoRoleEntryDatabaseRemoved, self.remove_autorolesentry)
            self.__event_manager.subscribe(GuildSettingDatabaseUpdated, self.set_guildsettings)
            self.__event_manager.subscribe(GuildSettingDatabaseRemoved, self.remove_guildsettings)

    def load_into_redis_client(self, client: tanjun.Client):
        client.add_client_callback(tanjun.ClientCallbackNames.STARTING, self._on_starting_event)
        client.add_client_callback(tanjun.ClientCallbackNames.CLOSING, self._on_stopping_event)

    async def _on_starting_event(
        self,
        client: tanjun.Client = tanjun.injected(type=tanjun.Client),
        rest: hikari.api.RESTClient = tanjun.injected(type=hikari.api.RESTClient),
        hikari_cache: hikari.api.Cache = tanjun.injected(type=hikari.api.Cache),
    ):
        await self.open(client, rest, hikari_cache)

    async def open(self, client: tanjun.Client, rest: hikari.api.RESTClient, hikari_cache: hikari.api.Cache):
        if self.__started:
            return
        try:
            self.__pool = aioredis.BlockingConnectionPool.from_url(
                url=self.__address,
                max_connections=self.__max_connections,
                encoding="utf-8",
                decode_responses=True,
            )
            self._redis_client = await self.spawn_redis_client()
        except aioredis.RedisError:
            redis_client = self._redis_client
            self._redis_client = None
            await redis_client.close()
            raise

        self.__rest = rest
        self.__hikari_cache = hikari_cache
        self.__started = True
        client.set_type_dependency(type(self), self)

    async def _on_stopping_event(self, client: tanjun.Client = tanjun.injected(type=tanjun.Client)):
        await self.close(client)

    async def close(self, client: tanjun.Client):
        was_started = self.__started
        self.__started = False
        if not was_started:
            return

        redis_client = self._redis_client
        redis_pool = self.__pool
        self.__pool = None
        self._redis_client = None
        await redis_client.close()
        await redis_pool.disconnect()
        client.remove_type_dependency(aioredis.Redis)
        client.remove_type_dependency(type(self))

    async def spawn_redis_client(self):
        return await aioredis.Redis(connection_pool=self.__pool)

    async def flushall(self) -> bool:
        """Clear all cache from the store.

        Returns
        -------
        bool
            Returns True if the cache was flushed, False on failure."""
        await self.clear_guild_settings()
        return True

    async def remove_guildsettings(self, event: GuildSettingDatabaseRemoved):
        """Event handler for GuildSetitngsDatabaseRemoved.

        After an GuildSetitngsDatabaseRemoved is handled, this event fires and is resolved here."""
        if not await self._redis_client.exists(LOCAL_GUILD_SETTINGS.format(guild_id=event.guild_id)):
            logger.warning(f"Cached GuildSetitngs <Guild Id: {event.guild_id}> not found")
            return
        await self._redis_client.hdel(LOCAL_GUILD_SETTINGS.format(guild_id=event.guild_id))
        await self._redis_client.sadd(LOCAL_GUILD_IDX, event.guild_id)
        logger.debug(f"Removed Cache of GuildSetitngs <Guild Id: {event.guild_id}> not found")

    async def set_guildsettings(self, event: GuildSettingDatabaseUpdated):
        """Event handler for GuildSetitngsDatabaseUpdated.


        After an GuildSetitngsDatabaseUpdate is updated, this event fires and is resolved here."""
        logger.debug(f"Attempting to save <GuildSettings: {event.guild_id}> to RedisCache.")
        event_cache = {
            "guild_id": event.guild_id,
            "logging_enabled": 1 if event.logging_enabled else 0,
        }
        if event.logging_channel:
            event_cache["logging_channel"] = int(event.logging_channel)
        if event.vc_logging_channel:
            event_cache["vc_logging_channel"] = int(event.vc_logging_channel)
        if event.mod_logging_channel:
            event_cache["mod_logging_channel"] = int(event.mod_logging_channel)

        logger.info(event_cache)
        for k,v in event_cache.items():
            await self._redis_client.hset(
                LOCAL_GUILD_SETTINGS.format(guild_id=event.guild_id), k, v
            )

        logger.info(await self._redis_client.hgetall(LOCAL_GUILD_SETTINGS.format(guild_id=event.guild_id)))
        await self._redis_client.sadd(LOCAL_GUILD_IDX, event.guild_id)
        logger.debug(f"Saved <GuildSettings: {event.guild_id}> to RedisCache.")

    async def get_guild_settings(self, guild_settings: LocalGuildSettings) -> LocalGuildSettings:
        """Retrieve LocalGuildSettings from the store.

        Parameters
        ----------
        guild_id: int
            The Discord Guild/Server ID to lookup.

        Returns
        -------
        LocalGuildSettings
            The guilds settings from cache.
        """
        if not guild_settings.guild_id:
            return guild_settings
        settings = await self._redis_client.hgetall(LOCAL_GUILD_SETTINGS.format(guild_id=guild_settings.guild_id))
        guild_settings.logging_enabled = True if settings.get("logging_enabled") == "1" else False
        guild_settings.auto_delete_enabled = True if settings.get("auto_delete_enabled") == "1" else False
        guild_settings.logging_channel = settings.get("logging_channel", None)
        guild_settings.vc_logging_channel = settings.get("vc_logging_channel", None)
        guild_settings.mod_logging_channel = settings.get("mod_logging_channel", None)
        return guild_settings

    async def set_guild_settings(self, settings: LocalGuildSettings) -> bool:
        """Set a LocalGuildSettings in the store

        Parameters
        ----------
        settings: LocalGuildSettings
            The item to cache.

        Returns
        -------
        bool
            True if cache succeed, False on failure."""
        if not settings.guild_id:
            return False
        settings_cache = {
            "guild_id": settings.guild_id,
            "logging_enabled": 1 if settings.logging_enabled else 0,
        }
        if settings.logging_channel:
            settings_cache["logging_channel"] = int(settings.logging_channel)
        if settings.vc_logging_channel:
            settings_cache["vc_logging_channel"] = int(settings.vc_logging_channel)
        if settings.mod_logging_channel:
            settings_cache["mod_logging_channel"] = int(settings.mod_logging_channel)

        await self._redis_client.hset(
            LOCAL_GUILD_SETTINGS.format(guild_id=settings.guild_id),
            mapping=settings_cache,
        )
        await self._redis_client.sadd(LOCAL_GUILD_IDX, settings.guild_id)
        return True

    async def clear_guild_setting_for_guild(self, guild_id: int) -> bool:
        """Clear LocalGuildSettings for the provided guild_id in the store.


        Parameters
        ----------
        guild_id: int
            The Guild ID to clear settings for.

        Returns
        -------
        bool
            True if the item is cleared, False if failed."""
        if await self._redis_client.exists(LOCAL_GUILD_SETTINGS.format(guild_id=guild_id)):
            await self._redis_client.delete(LOCAL_GUILD_SETTINGS.format(guild_id=guild_id))
            return True
        return False

    async def clear_guild_settings(self) -> bool:
        """Clear all LocalGuildSettings in the store.

        Returns
        -------
        bool
            True if the store is cleared, False if the flush failed."""

        logger.warning("Cache Clear Pipeline: Starting Local Guild Settings....")
        for guild_id in await self._redis_client.smembers(LOCAL_GUILD_IDX):
            async with self._redis_client.pipeline() as pipeline:
                await pipeline.delete(LOCAL_GUILD_SETTINGS.format(guild_id=guild_id))
                logger.warning(f"Cache Clear Pipeline: Added {LOCAL_GUILD_SETTINGS.format(guild_id=guild_id)} for Cache Clear.")
            await pipeline.execute()
        logger.warning("Cache Clear Pipeline: Success for Local Guild Settings!")
        return True

    async def get_auto_deleter_channel_ids(self, guild_id: int) -> set[int]:
        """Returns a typing.Set of all Auto Deleter Channel ID's for this guild.

        Parameters
        ----------
        guild_id: int
            The Guild ID to retrieve channel ids for.

        Returns
        ------
        set[int]
            The Set of channel ids to be cached."""
        return await self._redis_client.smembers(AUTO_DELETE_IDS_SET.format(guild_id=guild_id))

    async def set_auto_deleter_channel_ids(self, guild_id: int, channel_ids: set[int] | set) -> bool:
        """Set a channel ids for Auto Deleter for this Guild ID in the store.

        Parameters
        ----------
        guild_id: int
            The Guild ID these channel ids are associated with.
        channel_ids: set[int] | set
            The Set of channel ids, or an empty set, to be cached.

        Returns
        -------
        bool
            True if cache succeed, False on failure."""
        if not channel_ids:
            return False
        async with self._redis_client.pipeline() as pipeline:
            for channel_id in channel_ids:
                await pipeline.sadd(AUTO_DELETE_IDS_SET.format(guild_id=guild_id), int(channel_id))
            await pipeline.sadd(AUTO_DELETE_IDS_IDX, guild_id)
            await pipeline.execute()
        return True

    async def clear_auto_deleter_channel_ids_for_guild(self, guild_id: int) -> bool:
        """Clear all Auto Deleter channel ids for this Guild id in the store.

        Parameters
        ----------
        guild_id: int
            The id of a Guild to clear.

        Returns
        -------
        bool
            True if the store is cleared, False if the flush failed."""
        await self._redis_client.delete(AUTO_DELETE_IDS_SET.format(guild_id=guild_id))
        await self._redis_client.srem(AUTO_DELETE_IDS_IDX, AUTO_DELETE_IDS_SET.format(guild_id=guild_id))

        return (await self._redis_client.sismember(AUTO_DELETE_IDS_IDX, AUTO_DELETE_IDS_SET.format(guild_id=guild_id))) and (
            await self._redis_client.exists(AUTO_DELETE_IDS_SET.format(guild_id=guild_id))
        )

    async def clear_auto_deleter_channel_ids(self) -> bool:
        """Clear all Auto Deleter channel ids in the store.

        Returns
        -------
        bool
            True if the store is cleared, False if the flush failed."""
        auto_detelte_ids_guilds = await self._redis_client.smembers(AUTO_DELETE_IDS_IDX)
        async with self._redis_client.pipeline() as pipeline:
            for guild_id in auto_detelte_ids_guilds:
                await pipeline.delete(AUTO_DELETE_IDS_SET.format(guild_id=guild_id))
            await pipeline.delete(AUTO_DELETE_IDS_IDX)
            await pipeline.execute()
        return True

    async def get_autoroles_for_guild(self, auto_roles_list: AutoRoleList) -> AutoRoleList | None:
        """Returns a the AutoRoles for this guild.

        Parameters
        ----------
        auto_roles_list: AutoRoleList
            The AutoRoleList to populate with entries.

        Returns
        ------
        AutoRoleList | None
            The list of AutoRoles from the cache."""
        if not auto_roles_list.guild_id:
            return None
        redis_auto_roles = await self._redis_client.hgetall(AUTOROLES_KEY.format(guild_id=auto_roles_list.guild_id))
        if not redis_auto_roles:
            return None

        roles = self.__hikari_cache.get_roles_view_for_guild(auto_roles_list.guild_id)
        if not roles:
            roles = {role.id: role for role in await self.__rest.fetch_roles(auto_roles_list.guild_id)}

        for emote_id, role_id in redis_auto_roles.items():
            role = emote = None
            if roles.get(role_id):
                role = roles.get(role_id)
            if emote_id.isdigit():
                emote = self.__hikari_cache.get_emoji(emote_id)
            else:
                emote = hikari.UnicodeEmoji().parse(emote_id)

            if not emote or not role:
                continue

            auto_roles_list.append(emoji=emote, role=role)

        if len(auto_roles_list.entries) == 0:
            return None
        return auto_roles_list

    async def remove_autorolesentry(self, event: AutoRoleEntryDatabaseRemoved):
        """Event handler for AutoRoleEntryDatabaseRemoved.


        After an AutoRoleEntryDatabaseRemoved is handled, this event fires and is resolved here."""
        if not await self._redis_client.exists(AUTOROLES_KEY.format(guild_id=event.guild_id)):
            logger.warning(
                f"Cached AutoRoleEntry <Guild Id: {event.guild_id}, Emote: {event.emoji}, KEI: {event.custom_emoji_id}, Role Id: {event.role_id} not found"
            )
            return
        await self._redis_client.hdel(AUTOROLES_KEY.format(guild_id=event.guild_id), event.emoji)
        logger.debug(
            f"Removed Cache of AutoRoleEntry <Guild Id: {event.guild_id}, "
            f"Emote: {event.emoji}, KEI: {event.custom_emoji_id}, Role Id: {event.role_id} not found"
        )

    async def set_autorolesentry(self, event: AutoRoleEntryDatabaseUpdated):
        """Event handler for AutoRoleEntryDatabaseUpdated.


        After an AutoRoleEntryDatabaseUpdate is updated, this event fires and is resolved here."""
        await self._redis_client.sadd(AUTOROLES_IDX, event.guild_id)

        if not await self._redis_client.exists(AUTOROLES_KEY.format(guild_id=event.guild_id)):
            logger.debug(f"Creating Cache for <Guild: {event.guild_id}>")

        await self._redis_client.hset(
            AUTOROLES_KEY.format(guild_id=event.guild_id),
            event.emoji if not event.custom_emoji_id else event.custom_emoji_id,
            event.role_id,
        )

        logger.debug(f"Cached <Guild: {event.guild_id}> - <Emoji: {event.emoji}> - <Role: {event.role_id}>")

    async def set_autoroles_from_guild(self, auto_roles_list: AutoRoleList) -> bool:
        """Set a AutoRoles in the store.

        Parameters
        ----------
        guild_id: int
            The Guild ID this item is for.
        auto_roles_list: AutoRoleList
            the AutoRoles to cache for this Guild.

        Returns
        -------
        bool
            True if cache succeed, False on failure."""
        if not auto_roles_list.guild_id:
            return False
        await self._redis_client.sadd(AUTOROLES_IDX, auto_roles_list.guild_id)
        if auto_roles_list.entries:
            if await self._redis_client.exists(AUTOROLES_KEY.format(guild_id=auto_roles_list.guild_id)):
                await self._redis_client.delete(AUTOROLES_KEY.format(guild_id=auto_roles_list.guild_id))
            for entry in auto_roles_list.entries:
                await self._redis_client.hset(
                    AUTOROLES_KEY.format(guild_id=auto_roles_list.guild_id),
                    str(entry.emoji),
                    str(entry.role.id),
                )
                logger.debug(f"Saved {entry.emoji} - {entry.role.name}")
        logger.debug(f"Saved current autoroles for {auto_roles_list.guild_id} to redis!")
        return True

    async def clear_autoroles_from_guild(self, guild_id: int) -> bool:
        """Clear all AutoRoles for the provided Guild ID in the store.

        Parameters
        ----------
        guild_id: int
            The guild id to clear.

        Returns
        -------
        bool
            True if the store is cleared, False if the flush failed."""
        try:
            await self._redis_client.delete(AUTOROLES_KEY.format(guild_id=guild_id))
            await self._redis_client.srem(AUTOROLES_IDX, guild_id)
        except:  # noqa: E722
            logger.exception("Clearing Auto Roles from Redis for guild %s failed", guild_id)
            return False
        return True

    async def clear_autoroles(self) -> bool:
        """Clear all AutoRoles in the store.

        Returns
        -------
        bool
            True if the store is cleared, False if the flush failed."""
        try:
            auto_roles_for_guilds = await self._redis_client.smembers(AUTOROLES_IDX)
            logger.debug(
                "Deleting autoroles from redis for All Guilds ---- %s",
                auto_roles_for_guilds,
            )
            for guild_id in auto_roles_for_guilds:
                guild_key = AUTOROLES_KEY.format(guild_id=guild_id)
                if await self._redis_client.exists(guild_key):
                    await self._redis_client.delete(guild_key)
                    logger.debug("Deleted autoroles from redis for <%s>", guild_id)
        except Exception:
            logger.exception("Clearing all Auto Roles from cache failed")
            return False
        return True
