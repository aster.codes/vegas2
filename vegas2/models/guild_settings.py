"""Provides a Model for interacting with GuildSettings"""
from __future__ import annotations

import logging

import aiopg
import attr
import hikari
import tanjun
from psycopg2.extras import RealDictCursor

from .cache.redis import RedisCache
from .database.postgres import PostgresDatabase

logger = logging.getLogger(__name__)


LOCAL_GUILD_SETTINGS = "vegas2:{guild_id}:local_guild_settings"
AUTO_DELETE_IDS_SET = "vegas2:{guild_id}:auto_delete_ids"
DEFAULT_LOGGING_CHANNEL_NAME = "vegas2-logging"


@attr.s
class LocalGuildSettings:
    guild_id: str = attr.ib()
    logging_enabled: bool = attr.ib(default=False)
    logging_channel: int | None = attr.ib(default=None)
    vc_logging_channel: int | None = attr.ib(default=None)
    mod_logging_channel: int | None = attr.ib(default=None)
    auto_delete_enabled: bool = attr.ib(default=False)
    auto_delete_channel_ids: set[hikari.Snowflake] = attr.ib(factory=set)

    @classmethod
    async def setup(
        cls,
        guild_id: int,
        rest: hikari.api.RESTClient,
        database: PostgresDatabase,
        vegas_cache: RedisCache,
    ) -> LocalGuildSettings:
        guild_key = LOCAL_GUILD_SETTINGS.format(guild_id=guild_id)
        default_channel_id = await cls._get_default_logger_channel_id(guild_id, rest)
        auto_delete_channel_ids: set[int] = set()

        if (await vegas_cache._redis_client.exists(guild_key)) == 1:
            settings = await vegas_cache.get_guild_settings(cls(guild_id=str(guild_id)))
            auto_delete_channel_ids = await vegas_cache.get_auto_deleter_channel_ids(guild_id)
            settings.auto_delete_channel_ids = set(map(int, auto_delete_channel_ids))
            logger.debug(f"Found LocalGuildSettings {guild_key} in Redis")
        else:
            settings = await database.fetch_guild_settings_for_guild(cls(guild_id=str(guild_id)))
            if not settings:
                settings = cls(
                    guild_id=str(guild_id),
                    logging_enabled=False,
                    auto_delete_enabled=False,
                )

            # TODO Remove caching here for event based caching!!
            logger.debug("Trying to cache manually")
            await vegas_cache.set_auto_deleter_channel_ids(guild_id, settings.auto_delete_channel_ids)
            # await vegas_cache.set_guild_settings(settings)
            logger.debug("Cached manually")

        if settings.logging_channel is None:
            settings.logging_channel = default_channel_id
        if settings.vc_logging_channel is None:
            settings.vc_logging_channel = default_channel_id
        if settings.mod_logging_channel is None:
            settings.mod_logging_channel = default_channel_id

        return settings

    @staticmethod
    async def _get_default_logger_channel_id(
        guild_id: int,
        rest: hikari.api.RESTClient,
        ctx: tanjun.abc.SlashContext | None = None,
    ) -> int | None:
        if ctx and ctx.cache:
            cached_channels = ctx.cache.get_guild_channels_view()
            if cached_channels:
                for channel_id in cached_channels:
                    if cached_channels[channel_id].name == DEFAULT_LOGGING_CHANNEL_NAME:
                        return channel_id
        server_channels = await rest.fetch_guild_channels(guild_id)
        for channel in server_channels:
            if channel.name == DEFAULT_LOGGING_CHANNEL_NAME:
                return channel.id
        return None

    async def guild_settings_embed(self, ctx: tanjun.abc.SlashContext):
        guild = ctx.get_guild()
        if not guild:
            return False
        default = logging_channel = vc_logging_channel = mod_logging_channel = auto_delete_channel_ids = "`[ Not Set ]`"
        if self.logging_channel:
            logging_channel = (await ctx.rest.fetch_channel(self.logging_channel)).mention
        if self.vc_logging_channel:
            vc_logging_channel = (await ctx.rest.fetch_channel(self.vc_logging_channel)).mention
        if self.mod_logging_channel:
            mod_logging_channel = (await ctx.rest.fetch_channel(self.mod_logging_channel)).mention
        if self.auto_delete_channel_ids and len(self.auto_delete_channel_ids) > 0:
            auto_delete_channel_ids = ""
            for channel_id in self.auto_delete_channel_ids:
                auto_delete_channel_ids += f"<#{channel_id}>\n"

        embed = (
            hikari.Embed()
            .set_author(name=f"{guild.name} Settings", icon=hikari.Emoji.parse("⚙️"))
            .add_field(
                name="Logging Enabled:",
                value="☑️" if self.logging_enabled else "❌",
                inline=False,
            )
            .add_field(name="Logging Channel:", value=logging_channel or default, inline=True)
            .add_field(
                name="VC Logging Channel:",
                value=vc_logging_channel or default,
                inline=True,
            )
            .add_field(
                "Mod Logging Channel:",
                value=mod_logging_channel or default,
                inline=True,
            )
            .add_field(
                name="Auto Deleter Enabled:",
                value="☑️" if self.auto_delete_enabled else "❌",
                inline=False,
            )
            .add_field(
                "Auto Deleter Channels:",
                value=auto_delete_channel_ids or default,
                inline=True,
            )
            .set_thumbnail(guild.icon_url)
            .set_footer("You can also change settings on the Vegas2's Website!")
        )
        return embed

    async def add_auto_delete_channel(
        self,
        guild_id: int,
        channel_id: int,
        pg_pool: aiopg.Pool,
        vegas_cache: RedisCache,
    ) -> bool:
        if channel_id not in self.auto_delete_channel_ids:
            self.auto_delete_channel_ids.add(channel_id)
            with await pg_pool.cursor(cursor_factory=RealDictCursor) as cursor:
                await cursor.execute(
                    """
                    UPDATE guildownersettings_guildownersettings
                        SET auto_delete_channel_ids = auto_delete_channel_ids || '{ %(channel_id)s }'
                    WHERE guild_id = %(guild_id)s;
                    """,
                    {"guild_id": guild_id, "channel_id": channel_id},
                )
            await vegas_cache.set_auto_deleter_channel_ids(
                guild_id,
                {
                    channel_id,
                },
            )
            return True
        else:
            return False


def _guild_settings_serialize_to_postgres(_, name, value):
    if name == "auto_delete_channel_ids" and value is None:
        return set()
    elif name == "auto_delete_channel_ids":
        return {int(cid) for cid in value}
    if name in [
        "logging_channel",
        "vc_logging_channel",
        "mod_logging_channel",
        "guild_id",
    ]:
        return int(value)
    if name in [
        "logging_enabled",
        "auto_delete_enabled",
    ]:
        return bool(value)

    return value
