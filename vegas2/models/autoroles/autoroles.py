"""Provides 2 models to work with AutoRoles.

RoleEmojiEntry
    Represents a single entry on the AutoRoleList.
    Composed oo an Emoji/hikari.Role.

AutoRoleList
    Represents a full Auto Role List.
    Provides a set of RoleEmojiEntry."""
from __future__ import annotations

import attr
import hikari
import tanjun

from ...models.cache.redis import RedisCache
from ...models.database.postgres import PostgresDatabase
from ...util.helpers import get_vegas_logger

NOTIF_FILTER = "Notif"

AUTO_ROLES_KEY = "vegas2:{guild_id}:auto_roles"

logger = get_vegas_logger(__name__)


@attr.define(frozen=True)
class AutoRoleEntry:
    """Provides a simple model to work with AutoRoles
    as a single Entry."""

    emoji: hikari.UnicodeEmoji | hikari.KnownCustomEmoji | hikari.Emoji = attr.field()
    role: hikari.Role = attr.field()


@attr.define()
class AutoRoleList:
    """Provides a simple model to work with AutoRoles."""

    __event_manager: hikari.api.EventManager | None = attr.field(default=None)
    __app_cache: RedisCache | None = attr.field(default=None)
    __hikari_cache: hikari.api.Cache | None = attr.field(default=None)
    __database: PostgresDatabase = attr.field(default=None)
    __rest: hikari.api.RESTClient = attr.field(default=None)
    guild_id: hikari.Snowflakeish | int | None = attr.field(default=None)
    entries: set[AutoRoleEntry] = attr.field(factory=set)
    auto_channels: set[hikari.GuildTextChannel] = attr.field(factory=set)

    def load_into_client(self, client: tanjun.Client):
        """Helper method to attach the required startup/shutdown callbacks to the `tanjun.Client`.

        Parameters
        ----------
        client: tanjun.Client
            The client to attach the callbacks to.
        """
        client.add_client_callback(tanjun.ClientCallbackNames.STARTED, self._on_starting_event)
        # client.add_client_callback(tanjun.ClientCallbackNames.CLOSED, self._on_stopping_event)

    async def _on_starting_event(  # pylint: disable=R0913
        self,
        app_cache: RedisCache = tanjun.inject(type=RedisCache),
        hikari_cache: hikari.api.Cache = tanjun.inject(type=hikari.api.Cache),
        database: PostgresDatabase = tanjun.inject(type=PostgresDatabase),
        rest: hikari.api.RESTClient = tanjun.inject(type=hikari.api.RESTClient),
        client: tanjun.Client = tanjun.inject(type=tanjun.Client),
    ):
        """Internal method to handle setting up"""
        self.__rest = rest
        self.__app_cache = app_cache
        self.__hikari_cache = hikari_cache
        self.__database = database
        client.set_type_dependency(type(self), self)

    async def load_for_guild(self, guild_id: hikari.Snowflakeish | int):
        self.guild_id = hikari.Snowflake(guild_id)
        self.entries = set()
        if self.__app_cache:
            logger.debug(f"Trying Cached Autoroles for Guild ID: {guild_id}")
            await self.__app_cache.get_autoroles_for_guild(self)
        if not self.entries and self.__database:
            logger.debug(f"Trying Database Autoroles for Guild ID: {guild_id}")
            await self.__database.fetch_autoroles_for_guild(self)

    def append(self, emoji: hikari.UnicodeEmoji | hikari.KnownCustomEmoji | hikari.Emoji, role: hikari.Role) -> bool:
        """Adds an entry from AutoRoleList based on a provided emoji/role."""
        entry = AutoRoleEntry(emoji=emoji, role=role)

        if entry not in self.entries:
            self.entries.add(entry)

            return True
        return False

    def remove(
        self,
        /,
        *,
        emoji: hikari.Emoji | hikari.CustomEmoji | hikari.UnicodeEmoji | None = None,
        role: hikari.Role | None = None,
    ) -> bool:
        """Removes an entry from AutoRoleList based on a provided emoji/role."""
        if self.entries:
            start_count = len(self.entries)
            self.entries = set(
                filter(
                    lambda entry: entry.emoji != emoji and entry.role != role,
                    self.entries,
                )
            )
            end_count = len(self.entries)
            if start_count != end_count:
                return True
        return False

    def generate_embeds(self, ctx: tanjun.abc.Context) -> list[hikari.Embed]:
        """Generates an embed of the current AutoRoleList"""
        guild = ctx.get_guild()
        pronouns_embeds = [
            hikari.Embed(title=f"Pronoun Roles in {guild.name}", color=00000000),
        ]
        notifications_embeds = [
            hikari.Embed(title=f"Notification Roles in {guild.name}", color=00000000),
        ]
        comfort_embeds = [
            hikari.Embed(title=f"Comfort Roles in {guild.name}", color=00000000),
        ]

        if self.entries:
            for entry in self.entries:
                if NOTIF_FILTER.lower() in entry.role.name.lower():
                    notifications_embeds[-1] = append_to_field(notifications_embeds[-1], entry)
                elif entry.role.name.lower() in [
                    "ask to dm",
                    "open to dms",
                    "do not dm",
                    "ask to ping",
                    "open to pings",
                    "do not ping",
                ]:
                    comfort_embeds[-1] = append_to_field(comfort_embeds[-1], entry)
                else:
                    pronouns_embeds[-1] = append_to_field(pronouns_embeds[-1], entry)
        return notifications_embeds + pronouns_embeds + comfort_embeds


def append_to_field(embed: hikari.Embed, entry: AutoRoleEntry):
    """Helper function that appends a value to the last field in
    a provided hikari.Embed or if the field is at it's max, adds
    a new field."""
    entry_str = f"{entry.emoji} - {entry.role.name}\n"

    if not embed.fields or len(embed.fields[-1].value) >= 980:
        embed.add_field(name="Emoji - Role", value=entry_str)
        return embed
    embed_old_val = embed.fields[-1].value
    embed_field_len = len(embed.fields) - 1
    embed.edit_field(embed_field_len, "Emoji - Role", f"{embed_old_val}{entry_str}")
    return embed
