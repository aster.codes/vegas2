"""Contains the AutoRoles model for plugins.roles
"""
__all__ = [
    "AutoRoleList",
    "AutoRoleEntry",
]
from .autoroles import AutoRoleEntry, AutoRoleList
