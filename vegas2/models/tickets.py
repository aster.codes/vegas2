"""Provides a number of help automatic ticketing features!"""
from __future__ import annotations

from typing import TypedDict

import attr
import hikari
import tanjun
import yuyo
from hikari import PermissionOverwrite, PermissionOverwriteType, Permissions
from hikari.api import Cache
from hikari.api.special_endpoints import MessageActionRowBuilder
from hikari.channels import ChannelType
from hikari.interactions.base_interactions import ResponseType
from hikari.messages import  MessageFlag
from hikari.components import ButtonStyle
from tanjun.abc import SlashContext
from yuyo.components import ComponentContext

from ..util.constants.tickets import (
    DEFAULT_TICKET_CATEGORY_NAME,
    MAIN_TICKET_EMBED_DESC,
    MEMBER_NEW_TICKET_PERMISSIONS,
    TICKETS_LOGO,
    USER_TICKET_EMBED_DESC,
)
from ..util.helpers import get_vegas_logger

__all__ = [
    "Ticket",
]

DEFAULT_TIMEOUT = 60


logger = get_vegas_logger(__name__)


class ChannelParticipantsType(TypedDict):
    message_count: int
    member: hikari.User


@attr.s
class Ticket:
    ticket_channel_context: tanjun.abc.SlashContext | yuyo.ComponentContext = attr.ib()

    @staticmethod
    async def start_ticket_embed_components(
        ctx: SlashContext,
        bot: hikari.GatewayBotAware,
        cache: hikari.api.Cache
    ) -> tuple[hikari.Embed, MessageActionRowBuilder]:
        """Provides an Embed and Acton Row for the tickets channel."""
        embed = hikari.Embed(
            title="Submit a Ticket, Get Help!",
            description=MAIN_TICKET_EMBED_DESC,
            color=8454399,
        )
        embed.set_thumbnail(TICKETS_LOGO)
        #if self_bot := cache.get_me():
        #    embed.set_author(name=self_bot.username, icon=self_bot.avatar_url)

        row = (
            ctx.rest.build_message_action_row()
            .add_interactive_button(
                ButtonStyle.PRIMARY,
                "vegas2_new_ticket",
                label="Open Ticket",
                emoji="💬"
            )
        )
        return embed, row

    async def remove_member(self, ctx: tanjun.abc.SlashContext | yuyo.ComponentContext, member: hikari.Member):
        rest, _, channel = await self._normalize_tanjun_yuyo_contexts(ctx)
        if not rest or not channel:
            return
        await self._normalize_tanjun_yuyo_defer(ctx)
        try:
            await channel.remove_overwrite(member.id)
        except hikari.NotFoundError:
            await ctx.create_followup("It doesn't look like {member.mention} is in this channel...")
            return
        await ctx.create_followup(
            hikari.Embed(description=f"Member ID: {member.id}\nMember Mention: {member.mention}").set_author(
                name="Member(s) removed from Ticket:", icon=hikari.Emoji.parse("📤")
            )
        )

    async def add_member(self, ctx: tanjun.abc.SlashContext | yuyo.ComponentContext, member: hikari.Member):
        rest, _, channel = await self._normalize_tanjun_yuyo_contexts(ctx)
        if not rest or not channel:
            return
        await self._normalize_tanjun_yuyo_defer(ctx)
        overwrites = list(channel.permission_overwrites.values())
        if channel.parent_id:
            parent_channel = await rest.fetch_channel(channel.parent_id)
            parent_overwrites = parent_channel.permission_overwrites
            for overwrite_id in parent_overwrites:
                if parent_overwrites[overwrite_id].type == PermissionOverwriteType.ROLE:
                    overwrites.append(parent_overwrites[overwrite_id])
        overwrites.append(
            PermissionOverwrite(
                id=member.id,
                type=PermissionOverwriteType.MEMBER,
                allow=MEMBER_NEW_TICKET_PERMISSIONS,
            )
        )
        await channel.edit(permission_overwrites=overwrites)
        await ctx.create_followup(
            hikari.Embed(description=f"Member ID: {member.id}\nMember Mention: {member.mention}").set_author(
                name="Member(s) added to Ticket:", icon=hikari.Emoji.parse("📩")
            )
        )

    async def open_user_ticket(
        self,
        ctx: ComponentContext | tanjun.abc.SlashContext,
        member: hikari.Member,
        cache: Cache,
        bot: hikari.GatewayBotAware,
        closed: bool = False,
    ):
        rest, guild_id = self._get_rest_and_guild_id(ctx)
        embed, row = await self.open_user_ticket_embed_components(ctx)
        if not embed or not row or not guild_id or not rest:
            return
        if isinstance(ctx, tanjun.abc.SlashContext):
            await ctx.defer(flags=MessageFlag.EPHEMERAL)
        else:
            await ctx.defer(
                defer_type=ResponseType.DEFERRED_MESSAGE_CREATE,
                flags=MessageFlag.EPHEMERAL,
            )

        ticket_channel = None
        tickets_category = None

        async with rest.trigger_typing(ctx.interaction.channel_id):

            guild_channels = None
            if cache:
                guild_channels = cache.get_guild_channels_view_for_guild(guild_id)
            if not guild_channels:
                guild_channels = await rest.fetch_guild_channels(guild_id)

            for channel_id in guild_channels:
                channel = None
                if bot.cache:
                    channel = cache.get_guild_channel(channel_id)
                if not channel:
                    channel = await rest.fetch_channel(channel_id)

                if (
                    channel.name
                    and channel.name.lower() == DEFAULT_TICKET_CATEGORY_NAME.lower()
                    and channel.type == ChannelType.GUILD_CATEGORY
                ):
                    tickets_category = channel
                elif (
                    channel.name
                    and member
                    and str(member.id) in channel.name
                    and channel.name.endswith("-ticket")
                    and channel.type == ChannelType.GUILD_TEXT
                ):
                    ticket_channel = channel
            overwrites = [
                PermissionOverwrite(id=member.id, type=1, allow=MEMBER_NEW_TICKET_PERMISSIONS),
                PermissionOverwrite(id=guild_id, type=0, deny=Permissions.VIEW_CHANNEL),
            ]
            if not ticket_channel:
                if tickets_category:
                    category_overwrites = tickets_category.permission_overwrites
                    for overwrite_id in category_overwrites:
                        if category_overwrites[overwrite_id].type == PermissionOverwriteType.ROLE:
                            overwrites.append(category_overwrites[overwrite_id])

                ticket_channel = await rest.create_guild_text_channel(
                    guild_id,
                    name=f"{member.username}-{member.id}-ticket",
                    category=tickets_category or hikari.UNDEFINED,
                    permission_overwrites=overwrites,
                )
            else:
                await ticket_channel.edit(permission_overwrites=overwrites)

            control_message = await ticket_channel.send(
                f"{member.mention} please use this channel for your ticket!",
                user_mentions=True,
                embed=embed,
                component=row,
            )
            await ticket_channel.pin_message(control_message)
            await ctx.create_followup(
                f"Ticket Created! Go ---> {ticket_channel.mention}",
                flags=MessageFlag.EPHEMERAL,
            )

    async def open_old_ticket(self, ctx: tanjun.abc.SlashContext | yuyo.ComponentContext):
        rest, guild_id, channel = await self._normalize_tanjun_yuyo_contexts(ctx)
        if not rest or not guild_id or not channel:
            return
        await self._normalize_tanjun_yuyo_defer(ctx)

        pins = await channel.fetch_pins()
        if not pins[-1].embeds:
            return
        parts_to_add = list()
        info_embed = hikari.Embed().set_author(name="Adding Members...", icon=hikari.Emoji.parse("📩"))
        for participant_field in pins[0].embeds[0].fields[2:]:
            member = None
            if isinstance(ctx, tanjun.abc.SlashContext) and ctx.cache:
                member = ctx.cache.get_member(guild_id, int(participant_field.name))
            if not member:
                member = await rest.fetch_member(guild_id, int(participant_field.name))
            if member:
                info_embed.add_field(
                    name=f"{member.id}",
                    value=f"{member.mention} -- {member.username}#{member.discriminator}",
                    inline=True,
                )
                parts_to_add.append(member)

        await channel.send(embed=info_embed)

        overwrites = [
            PermissionOverwrite(
                id=member.id,
                type=PermissionOverwriteType.MEMBER,
                allow=MEMBER_NEW_TICKET_PERMISSIONS,
            )
            for member in parts_to_add
        ]
        overwrites.append(
            PermissionOverwrite(
                id=guild_id,
                type=PermissionOverwriteType.ROLE,
                deny=Permissions.VIEW_CHANNEL,
            )
        )

        tickets_category = None

        if channel.parent_id:
            tickets_category = await rest.fetch_channel(channel.parent_id)

        if tickets_category:
            category_overwrites = tickets_category.permission_overwrites
            for overwrite_id in category_overwrites:
                if category_overwrites[overwrite_id].type == PermissionOverwriteType.ROLE:
                    overwrites.append(category_overwrites[overwrite_id])
        await channel.edit(permission_overwrites=overwrites)

    async def open_user_ticket_embed_components(
        self, ctx: ComponentContext | tanjun.abc.SlashContext
    ) -> tuple[hikari.Embed, MessageActionRowBuilder] | tuple[None, None]:
        """Provides an embed for individual ticket channels."""
        rest, _ = self._get_rest_and_guild_id(ctx)
        if not rest:
            return (None, None)
        embed = hikari.Embed(title="", description=USER_TICKET_EMBED_DESC, color=8454399)
        embed.set_thumbnail(TICKETS_LOGO)
        self_bot = await rest.fetch_my_user()
        embed.set_author(name=self_bot.username, icon=self_bot.avatar_url)
        row = (
            ctx.interaction.app.rest.build_message_action_row()
            .add_interactive_button(
                ButtonStyle.SECONDARY,
                "vegas2_add_member_ticket",
                emoji="📩",
                label="Add Member to Ticket"
            )
            .add_interactive_button(
                ButtonStyle.SECONDARY,
                "vegas2_remove_member_ticket",
                emoji="🏴",
                label="Remove Member from Ticket"
            )
            .add_interactive_button(
                ButtonStyle.SECONDARY,
                "vegas2_timeout_mode_ticket",
                emoji="🔒",
                label="Timeout Mode"
            )
            .add_interactive_button(
                ButtonStyle.DANGER,
                "vegas2_close_ticket",
                emoji="🛑",
                label="Close Ticket"
            )
        )

        return (embed, row)

    async def close_user_ticket(self, ctx: tanjun.abc.SlashContext | yuyo.ComponentContext):
        rest, guild_id, channel = await self._normalize_tanjun_yuyo_contexts(ctx)
        if not rest or not guild_id or not channel:
            return
        await self._normalize_tanjun_yuyo_defer(ctx)
        overwrites = [PermissionOverwrite(id=guild_id, type=0, deny=Permissions.VIEW_CHANNEL)]
        old_permissions = channel.permission_overwrites
        tickets_category = None

        if channel.parent_id:
            tickets_category = await rest.fetch_channel(channel.parent_id)

        if tickets_category:
            category_overwrites = tickets_category.permission_overwrites
            for overwrite_id in category_overwrites:
                if category_overwrites[overwrite_id].type == PermissionOverwriteType.ROLE:
                    overwrites.append(category_overwrites[overwrite_id])

        await channel.edit(permission_overwrites=overwrites)

        participants, total_count = await self._get_channel_members(channel)
        embed = hikari.Embed()
        embed.set_author(name="Ticket Closed", icon=hikari.UnicodeEmoji.parse("🛑"))
        embed.set_thumbnail(TICKETS_LOGO)
        embed.add_field(name="Total Messages:", value=f"{total_count}")
        embed.add_field(name="Participants:", value=f"{'-'*14}")
        for member_id, info in participants.items():
            if info is None or member_id is None:
                continue
            embed.add_field(
                name=f"{member_id}",
                value=(
                    f"*Member Ping:* {info['member'].mention}\n"
                    f"*Member Username:* {info['member'].username}\n"
                    f"*Message Count:* {info['message_count']}"
                ),
                inline=True,
            )
        for overwrite in old_permissions:
            if old_permissions[overwrite].type == PermissionOverwriteType.MEMBER:
                if not any(old_permissions[overwrite].id == field.name for field in embed.fields):
                    member = await rest.fetch_member(guild_id, old_permissions[overwrite].id)
                    embed.add_field(
                        name=f"{member.id}",
                        value=(f"*Member Ping:* {member.mention}\n" f"*Member Username:* {member.username}"),
                        inline=True,
                    )

        row = (
            rest.build_message_action_row()
            .add_interactive_button(ButtonStyle.SECONDARY, "vegas2_open_old_ticket")
            .set_emoji("🟢")
            .set_label("ReOpen Ticket")
            .add_interactive_button(ButtonStyle.DANGER, "vegas2_delete_ticket")
            .set_emoji("🚮")
            .set_label("Delete Ticket")
        )
        control_message = await channel.send(embed=embed, component=row)
        await channel.pin_message(control_message)

    async def _get_channel_members(
        self, channel: hikari.GuildTextChannel
    ) -> tuple[dict[None, None] | dict[hikari.Snowflake, ChannelParticipantsType], int]:
        members: dict[hikari.Snowflake, ChannelParticipantsType] = dict()
        total_message_count: int = 0
        async for message in channel.fetch_history():
            total_message_count += 1
            if not message.author or message.author.is_bot:
                continue
            if not members.get(message.author.id, None):
                members[message.author.id] = {
                    "message_count": 0,
                    "member": message.author,
                }
            members[message.author.id]["message_count"] += 1

        return members, total_message_count

    def _get_rest_and_guild_id(
        self, ctx: yuyo.ComponentContext | tanjun.abc.SlashContext
    ) -> tuple[hikari.api.RESTClient, hikari.Snowflake] | tuple[None, None]:
        if isinstance(ctx, tanjun.abc.SlashContext):
            if not ctx.guild_id:
                return (None, None)
            return (ctx.rest, ctx.guild_id)
        else:
            if not ctx.interaction.guild_id:
                return (None, None)
            return (ctx.interaction.app.rest, ctx.interaction.guild_id)

    async def _normalize_tanjun_yuyo_contexts(
        self, ctx: tanjun.abc.SlashContext | yuyo.ComponentContext
    ) -> tuple[None, None, None] | tuple[hikari.api.RESTClient, hikari.Snowflakeish, hikari.GuildTextChannel]:
        if isinstance(ctx, tanjun.abc.SlashContext):
            if not ctx.guild_id:
                return (None, None, None)
            channel = ctx.get_channel()
            if not channel:
                channel = await ctx.fetch_channel()
            return ctx.rest, ctx.guild_id, channel
        else:
            if not ctx.interaction.guild_id:
                return (None, None, None)
            channel = ctx.interaction.get_channel()
            if not channel:
                channel = await ctx.interaction.fetch_channel()
            return ctx.interaction.app.rest, ctx.interaction.guild_id, channel

    async def _normalize_tanjun_yuyo_defer(self, ctx: tanjun.abc.SlashContext | yuyo.ComponentContext):
        if ctx.has_responded or ctx.has_been_deferred:
            return
        if isinstance(ctx, tanjun.abc.SlashContext):
            await ctx.defer(flags=MessageFlag.EPHEMERAL)
        else:
            await ctx.defer(
                defer_type=ResponseType.DEFERRED_MESSAGE_UPDATE,
                flags=MessageFlag.EPHEMERAL,
            )
