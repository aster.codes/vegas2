"""Module provides the basic functions to build the client and bot."""
from __future__ import annotations

import hikari
import httpx
import sentry_sdk
import tanjun
import yuyo
from apscheduler.schedulers.asyncio import AsyncIOScheduler
#from hikari import config as hikari_config

from . import config as config_
from .models.autoroles import AutoRoleList
from .models.cache.redis import RedisCache
from .models.database.postgres import PostgresDatabase
from .util.dependencies import (
    APSchedulerInjector,
    HttpxInjector,
    Jinja2Dep,
    PostgresPoolDependency,
)
from .util.hooks import on_error, on_parse_error


def build_bot() -> hikari.GatewayBot:
    """Builds and returns a GatewayBot. Calls build_client to start tanjun."""
    config = config_.FullConfig.from_environ()
    if config.tokens.sentry:
        sentry_sdk.init(
            config.tokens.sentry,
            # Set traces_sample_rate to 1.0 to capture 100%
            # of transactions for performance monitoring.
            # We recommend adjusting this value in production.
            traces_sample_rate=1.0,
        )

    bot = hikari.GatewayBot(
        config.tokens.discord_bot,
        logs=config.log_level,
        intents=config.intents,
        #cache_settings=hikari_config.CacheSettings(components=config.cache),
        banner="vegas2.util",
    )

    client = build_client(bot, config=config)

    register_client_dependencies_and_callbacks(client, bot, config)

    return bot


def build_client(bot: hikari.GatewayBot, /, *, config: config_.FullConfig | None = None) -> tanjun.Client:
    """Builds and configures the tanjun client from a given GatewayBot."""
    if not config:
        config = config_.FullConfig.from_environ()

    client = tanjun.Client.from_gateway_bot(
        bot,
        mention_prefix=config.mention_prefix,
        declare_global_commands=config.declare_global_commands,
    )

    return client


def register_client_dependencies_and_callbacks(client: tanjun.Client, bot: hikari.GatewayBot, config: config_.FullConfig):
    """Setup Client dependencies and callbacks."""

    # Create component and reaction client for yuyo.
    component_client = yuyo.ComponentClient.from_tanjun(client)
    reaction_client = yuyo.ReactionClient.from_gateway_bot(bot, event_managed=False)

    # Add prefxes to bot, if set
    client.add_prefix(config.prefixes)

    # Add callbacks for client.
    (
        # Yuyo Callbacks for components events
        client.add_client_callback(tanjun.ClientCallbackNames.STARTING, component_client.open)
        .add_client_callback(tanjun.ClientCallbackNames.CLOSING, component_client.close)
        # Yuyo Callbacks for reaction events
        .add_client_callback(tanjun.ClientCallbackNames.STARTING, reaction_client.open)
        .add_client_callback(tanjun.ClientCallbackNames.CLOSING, reaction_client.close)
    )
    """Setup Types and DI"""
    (
        # Yuyo + tokens and config.
        client.set_type_dependency(hikari.traits.GatewayBotAware, bot)
        .set_type_dependency(yuyo.ComponentClient, component_client)
        .set_type_dependency(yuyo.ReactionClient, reaction_client)
        .set_type_dependency(config_.FullConfig, config)
        .set_type_dependency(config_.Tokens, config.tokens)
        # Custom DI
        .set_type_dependency(httpx.AsyncClient, HttpxInjector()())
        .set_type_dependency(AsyncIOScheduler, APSchedulerInjector()())
        .set_type_dependency(Jinja2Dep, Jinja2Dep())
    )

    # Setup Postgres Deps (requires Asnyc so has to be done separately.)
    PostgresPoolDependency().load_into_client(client)
    RedisCache(event_manager=bot.event_manager).load_into_redis_client(client)
    PostgresDatabase().load_into_client(client)
    AutoRoleList().load_into_client(client)

    # Setup global Error hooks for bot.
    client.set_hooks(tanjun.AnyHooks().set_on_parser_error(on_parse_error).set_on_error(on_error))

    # Load potential plugins.
    (
        client.load_modules("vegas2.plugins.debug")
        .load_modules("vegas2.plugins.embeds")
        .load_modules("vegas2.plugins.tickets")
        .load_modules("vegas2.plugins.moderation")
        .load_modules("vegas2.plugins.question_of_the_day")
        .load_modules("vegas2.plugins.event_logger")
        .load_modules("vegas2.plugins.welcome")
        .load_modules("vegas2.plugins.messages")
        .load_modules("vegas2.plugins.archive")
        .load_modules("vegas2.plugins.guild_settings")
        .load_modules("vegas2.plugins.roles")
        # .load_modules("vegas2.plugins.test")
    )

    # If config has owner_only set, lock bot.
    if config.owner_only:
        client.with_check(tanjun.checks.OwnerCheck())

    return client
